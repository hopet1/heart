// import 'package:flutter/material.dart';
//
// class UserCircleAvatar extends StatefulWidget {
//   const UserCircleAvatar(
//       {Key key, @required this.imgUrl, @required this.active})
//       : super(key: key);
//   final String imgUrl;
//   final bool active;
//
//   @override
//   State createState() => UserCircleAvatarState(imgUrl: imgUrl, active: active);
// }
//
// class UserCircleAvatarState extends State<UserCircleAvatar> {
//   UserCircleAvatarState({@required this.imgUrl, @required this.active});
//   final String imgUrl;
//   final bool active;
//
//   final double radius = 20;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return CircleAvatar(
//       radius: radius + 5,
//       backgroundColor: widget.active ? Colors.blue : Colors.transparent,
//       child: CircleAvatar(
//         radius: radius,
//         backgroundImage: NetworkImage(widget.imgUrl),
//       ),
//     );
//   }
// }
