// import 'package:flutter/material.dart';
//
// class Spinner extends StatelessWidget {
//   Spinner({Key key, this.text}) : super(key: key);
//
//   final String text; //Text that's displayed under spinner.
//
//   @override
//   Widget build(BuildContext context) {
//     return Center(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.center,
//         children: [
//           CircularProgressIndicator(
//             strokeWidth: 3.0,
//           ),
//           SizedBox(height: 20),
//           Text(
//             text == null ? 'Loading...' : text,
//             style: TextStyle(fontSize: 15.0, color: Colors.grey),
//           ),
//         ],
//       ),
//     );
//   }
// }
