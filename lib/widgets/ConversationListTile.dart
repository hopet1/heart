// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/blocs/message/MessageBloc.dart';
// import 'package:heart/blocs/message/MessageEvent.dart';
// import 'package:heart/blocs/message/MessagePage.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/ConversationModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:timeago/timeago.dart' as timeago;

// // A list tile that displays information about a user's conversation.
// class ConversationListTile extends StatefulWidget {
//   const ConversationListTile(
//       {Key key, @required this.conversationDoc, @required this.currentUser})
//       : super(key: key);
//   final DocumentSnapshot conversationDoc;
//   final UserModel currentUser;

//   @override
//   State createState() => ConversationListTileState(
//       conversationDoc: conversationDoc, currentUser: currentUser);
// }

// class ConversationListTileState extends State<ConversationListTile> {
//   ConversationListTileState(
//       {@required this.conversationDoc, @required this.currentUser});
//   final DocumentSnapshot conversationDoc;
//   final UserModel currentUser;

//   @override
//   void initState() {
//     super.initState();
//   }

//   Future<ConversationModel> createConversation() async {
//     dynamic userIDs = conversationDoc.data()['users'];

//     //Determine the user you are speaking with.
//     UserModel oppositeUser = currentUser.uid == userIDs[0]
//         ? await locator<UsersDBService>().retrieveUser(uid: userIDs[1])
//         : await locator<UsersDBService>().retrieveUser(uid: userIDs[0]);

//     //Create conversation item.
//     return ConversationModel(
//       title: oppositeUser.username,
//       lastMessage: conversationDoc.data()['lastMessage'],
//       imageUrl: oppositeUser.imgUrl,
//       sender: ChatUser(
//           uid: currentUser.uid,
//           name: currentUser.username,
//           avatar: currentUser.imgUrl,
//           fcmToken: currentUser.fcmToken),
//       sendee: ChatUser(
//           uid: oppositeUser.uid,
//           name: oppositeUser.username,
//           avatar: oppositeUser.imgUrl,
//           fcmToken: oppositeUser.fcmToken),
//       reference: conversationDoc.reference,
//       time: conversationDoc.data()['time'].toDate(),
//       read: conversationDoc.data()['${currentUser.uid}_read'],
//       // oppositeUser: oppositeUser,
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: createConversation(),
//       builder: (BuildContext context, AsyncSnapshot snapshot) {
//         switch (snapshot.connectionState) {
//           case ConnectionState.waiting:
//             return Spinner(
//               text: 'Loading...',
//             );
//             break;
//           default:
//             ConversationModel conversation = snapshot.data;

//             return Column(
//               children: [
//                 ListTile(
//                   onTap: () {
//                     Route route = MaterialPageRoute(
//                       builder: (BuildContext context) => BlocProvider(
//                         create: (BuildContext context) => MessageBloc(
//                           sendee: conversation.sendee,
//                           sender: conversation.sender,
//                           convoDocRef: conversation.reference,
//                         )..add(LoadPageEvent()),
//                         child: MessagePage(),
//                       ),
//                     );

//                     Navigator.push(
//                       context,
//                       route,
//                     );
//                   },
//                   leading: CircleAvatar(
//                     backgroundColor: Colors.purple,
//                     backgroundImage: NetworkImage(conversation.imageUrl),
//                   ),
//                   trailing: Icon(
//                     Icons.chevron_right,
//                     color: Colors.grey,
//                   ),
//                   title: Column(
//                     children: <Widget>[
//                       Row(
//                         children: <Widget>[
//                           conversation.read
//                               ? Container()
//                               : Icon(
//                                   MdiIcons.circle,
//                                   size: 10,
//                                   color: Colors.lightBlue,
//                                 ),
//                           conversation.read
//                               ? Container()
//                               : SizedBox(
//                                   width: 10,
//                                 ),
//                           Text(
//                             conversation.title,
//                             style: TextStyle(
//                                 color: Colors.grey,
//                                 fontWeight: conversation.read
//                                     ? FontWeight.normal
//                                     : FontWeight.bold),
//                           )
//                         ],
//                       ),
//                       SizedBox(
//                         height: 5,
//                       )
//                     ],
//                   ),
//                   subtitle: Column(
//                     crossAxisAlignment: CrossAxisAlignment.start,
//                     mainAxisAlignment: MainAxisAlignment.start,
//                     children: <Widget>[
//                       Text(
//                         '${conversation.lastMessage == '' ? '\"No Last Message\"' : conversation.lastMessage}',
//                         maxLines: 1,
//                         style: TextStyle(color: Colors.black),
//                       ),
//                       SizedBox(
//                         height: 5,
//                       ),
//                       Text(
//                         timeago.format(
//                           conversation.time,
//                         ),
//                         style: TextStyle(color: Colors.grey, fontSize: 12),
//                       )
//                     ],
//                   ),
//                 ),
//                 Divider()
//               ],
//             );
//         }
//       },
//     );
//   }
// }
