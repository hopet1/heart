// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
//
// class TextOfTheDayTile extends StatefulWidget {
//   const TextOfTheDayTile(
//       {Key key,
//       @required this.textOfTheDay,
//       @required this.select,
//       @required this.delete})
//       : super(key: key);
//   final TextOfTheDayModel textOfTheDay;
//   final Function select;
//   final Function delete;
//
//   @override
//   State createState() => TextOfTheDayTileState(
//       textOfTheDay: textOfTheDay, select: select, delete: delete);
// }
//
// class TextOfTheDayTileState extends State<TextOfTheDayTile> {
//   TextOfTheDayTileState(
//       {@required this.textOfTheDay,
//       @required this.select,
//       @required this.delete});
//   final TextOfTheDayModel textOfTheDay;
//   final Function select;
//   final Function delete;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Column(
//       children: <Widget>[
//         ListTile(
//           onTap: widget.select,
//           title: Text(
//             widget.textOfTheDay.text,
//             style: TextStyle(
//                 color: widget.textOfTheDay.isActive ? Colors.green : null),
//           ),
//           trailing: !widget.textOfTheDay.isActive
//               ? IconButton(
//                   icon: Icon(
//                     Icons.delete,
//                     color: Colors.red,
//                   ),
//                   onPressed: widget.delete,
//                 )
//               : SizedBox.shrink(),
//         ),
//         Divider()
//       ],
//     );
//   }
// }
