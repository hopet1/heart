// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/pages/templates/TemplateDetailsForAdmin.dart';
// import 'package:heart/pages/templates/TemplateDetailsPage.dart';
// import 'package:heart/services/UsersDBService.dart';
// import '../ServiceLocator.dart';
//
// //List Tile that attaches user info to a template.
//
// class UserTemplateListTile extends StatefulWidget {
//   const UserTemplateListTile(
//       {Key key,
//       @required this.forAdmin,
//       @required this.template,
//       @required this.category})
//       : super(key: key);
//   final bool forAdmin;
//   final TemplateModel template;
//   final CategoryModel category;
//
//   @override
//   State createState() => UserTemplateListTileState(
//       forAdmin: forAdmin, template: template, category: category);
// }
//
// class UserTemplateListTileState extends State<UserTemplateListTile> {
//   UserTemplateListTileState(
//       {@required this.forAdmin,
//       @required this.template,
//       @required this.category});
//   final bool forAdmin;
//   final TemplateModel template;
//   final CategoryModel category;
//   Future<UserModel> retrieveUserFuture;
//
//   final int messageMax = 100;
//   @override
//   void initState() {
//     super.initState();
//     retrieveUserFuture =
//         locator<UsersDBService>().retrieveUser(uid: template.userID);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: retrieveUserFuture,
//       builder: (BuildContext context, AsyncSnapshot snapshot) {
//         switch (snapshot.connectionState) {
//           case ConnectionState.waiting:
//             return Spinner(
//               text: 'Loading...',
//             );
//             break;
//           default:
//             UserModel user = snapshot.data;
//             return ListTile(
//               leading: CircleAvatar(
//                 backgroundImage: NetworkImage(user.imgUrl),
//               ),
//               title: Text(user.username),
//               subtitle: Text(
//                 template.message.length > messageMax
//                     ? template.message.substring(0, messageMax + 1) + '...'
//                     : template.message,
//                 style:
//                     TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
//               ),
//               trailing: Icon(Icons.chevron_right),
//               onTap: () async {
//                 Navigator.push(
//                   context,
//                   MaterialPageRoute(
//                     builder: (BuildContext context) {
//                       return forAdmin
//                           ? TemplateDetailsForAdminPage(
//                               template: template,
//                               category: category,
//                               user: user,
//                             )
//                           : null;
//                     },
//                   ),
//                 );
//               },
//             );
//         }
//       },
//     );
//   }
// }
