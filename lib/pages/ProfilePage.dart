// import 'dart:io';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/ConversationsDBService.dart';
// import 'package:heart/services/StorageService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/widgets/ConversationListTile.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';
//
// import '../ServiceLocator.dart';
//
// class ProfilePage extends StatefulWidget {
//   const ProfilePage({Key key, @required this.currentUser});
//   final UserModel currentUser;
//   @override
//   State createState() => ProfilePageState(currentUser: currentUser);
// }
//
// class ProfilePageState extends State<ProfilePage> {
//   ProfilePageState({@required this.currentUser});
//   final UserModel currentUser;
//   bool isLoading = true;
//   String loadingText = '';
//   File profileImage;
//   double imageSize = 120;
//   double listViewBuilderPadding = 10;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     locator<ConversationsDBService>().cancelConversationSubscription();
//     super.dispose();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: Stack(
//         children: <Widget>[
//           Container(
//             color: Colors.white,
//             width: screenWidth,
//             height: screenHeight,
//           ),
//           Stack(
//             children: <Widget>[
//               Container(
//                 height: imageSize + 100,
//                 width: screenWidth,
//                 color: Colors.red,
//               ),
//               Padding(
//                 padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
//                 child: Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                   children: <Widget>[
//                     IconButton(
//                       icon: Icon(
//                         Icons.chevron_left,
//                         color: Colors.white,
//                       ),
//                       onPressed: () {
//                         Navigator.of(context).pop();
//                       },
//                     ),
//                     Text(
//                       currentUser.username,
//                       style: TextStyle(
//                           fontSize: 18,
//                           color: Colors.white,
//                           fontWeight: FontWeight.bold),
//                     ),
//                     InkWell(
//                       onTap: showSelectImageDialog,
//                       child: CircleAvatar(
//                         backgroundImage: NetworkImage(currentUser.imgUrl),
//                       ),
//                     )
//                   ],
//                 ),
//               ),
//             ],
//           ),
//           Positioned(
//             top: imageSize,
//             child: Container(
//               height: screenHeight - imageSize,
//               width: screenWidth,
//               decoration: BoxDecoration(
//                 color: Colors.white,
//                 borderRadius: BorderRadius.all(
//                   Radius.circular(20),
//                 ),
//               ),
//               child: Container(
//                   height: screenHeight - imageSize + listViewBuilderPadding,
//                   width: screenWidth,
//                   child: StreamBuilder<QuerySnapshot>(
//                     stream: locator<ConversationsDBService>()
//                         .streamConversations(userID: currentUser.uid),
//                     builder: (BuildContext context, AsyncSnapshot snapshot) {
//                       switch (snapshot.connectionState) {
//                         case ConnectionState.waiting:
//                           return Spinner(text: 'Loading...');
//                         default:
//                           print(snapshot.data);
//
//                           return Column(
//                             children: <Widget>[
//                               Container(
//                                 width: screenWidth,
//                                 padding: EdgeInsets.all(20),
//                                 child: Text(
//                                   'Messages',
//                                   style: TextStyle(
//                                       fontSize: 18,
//                                       fontWeight: FontWeight.bold),
//                                   textAlign: TextAlign.center,
//                                 ),
//                               ),
//                               Expanded(
//                                 child: ListView.builder(
//                                   itemCount: snapshot.data.documents.length,
//                                   itemBuilder:
//                                       (BuildContext context, int index) {
//                                     DocumentSnapshot conversationDoc =
//                                         snapshot.data.documents[index];
//
//                                     // return ConversationListTile(
//                                     //   conversationDoc: conversationDoc,
//                                     //   currentUser: currentUser,
//                                     // );
//                                   },
//                                 ),
//                               )
//                             ],
//                           );
//                       }
//                     },
//                   )),
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
//   showSelectImageDialog() {
//     return Platform.isIOS ? iOSBottomSheet() : androidDialog();
//   }
//
//   iOSBottomSheet() {
//     showCupertinoModalPopup(
//         context: context,
//         builder: (BuildContext context) {
//           return CupertinoActionSheet(
//             title: Text('Add Photo'),
//             actions: <Widget>[
//               CupertinoActionSheetAction(
//                 child: Text('Take Photo'),
//                 onPressed: () => handleImage(source: ImageSource.camera),
//               ),
//               CupertinoActionSheetAction(
//                 child: Text('Choose From Gallery'),
//                 onPressed: () => handleImage(source: ImageSource.gallery),
//               )
//             ],
//             cancelButton: CupertinoActionSheetAction(
//               child: Text(
//                 'Cancel',
//                 style: TextStyle(color: Colors.redAccent),
//               ),
//               onPressed: () => Navigator.pop(context),
//             ),
//           );
//         });
//   }
//
//   androidDialog() {
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return SimpleDialog(
//             title: Text('Add Photo'),
//             children: <Widget>[
//               SimpleDialogOption(
//                 child: Text('Take Photo'),
//                 onPressed: () => handleImage(source: ImageSource.camera),
//               ),
//               SimpleDialogOption(
//                 child: Text('Choose From Gallery'),
//                 onPressed: () => handleImage(source: ImageSource.gallery),
//               ),
//               SimpleDialogOption(
//                 child: Text(
//                   'Cancel',
//                   style: TextStyle(color: Colors.redAccent),
//                 ),
//                 onPressed: () => Navigator.pop(context),
//               )
//             ],
//           );
//         });
//   }
//
//   handleImage({@required ImageSource source}) async {
//     //Remove previous pop up.
//     Navigator.pop(context);
//
//     //Pick an image.
//     PickedFile file = await ImagePicker().getImage(source: source);
//
//     //Check that user picked an image.
//     if (file == null) return;
//
//     //Crop an image.
//     File image = await ImageCropper.cropImage(sourcePath: file.path);
//
//     //Check that user cropped the image.
//     if (image == null) return;
//
//     //Update loading text.
//     loadingText = 'Saving image...';
//
//     //Start loading indicator.
//     setState(() {
//       isLoading = true;
//     });
//
//     //Update image variables.
//     profileImage = image;
//
//     //Get image upload url.
//     final String newImgUrl = await locator<StorageService>().uploadImage(
//         file: profileImage, path: 'Images/Users/${currentUser.uid}/Profile');
//
//     //Save image upload url.
//     await locator<UsersDBService>()
//         .updateUser(uid: currentUser.uid, data: {'imgUrl': newImgUrl});
//
//     //Update image url on user.
//     currentUser.imgUrl = newImgUrl;
//
//     //Update loading text, probably don't need it here again...
//     loadingText = null;
//
//     //Stop loading indicator.
//     setState(() {
//       isLoading = false;
//     });
//   }
// }
