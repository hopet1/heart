// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
//
// import '../../ServiceLocator.dart';
//
// class TemplateDetailsForAdminPage extends StatefulWidget {
//   TemplateDetailsForAdminPage(
//       {@required this.template, @required this.category, @required this.user});
//   final TemplateModel template;
//   final CategoryModel category;
//   final UserModel user;
//
//   @override
//   State createState() => TemplateDetailsForAdminPageState(
//       template: template, category: category, user: user);
// }
//
// class TemplateDetailsForAdminPageState
//     extends State<TemplateDetailsForAdminPage> {
//   TemplateDetailsForAdminPageState(
//       {@required this.template, @required this.category, @required this.user});
//   final TemplateModel template;
//   final CategoryModel category;
//   final UserModel user;
//   final TextEditingController messageController = TextEditingController();
//   double imageSize = 200;
//   double listViewBuilderPadding = 10;
//   Future<UserModel> getCurrentUserFuture =
//       locator<AuthService>().getCurrentUser();
//   @override
//   void initState() {
//     super.initState();
//
//     messageController.text = template.message;
//   }
//
//   void approve() async {
//     bool confirm = await locator<ModalService>().showConfirmation(
//         context: context,
//         title: 'Approve Message?',
//         message: 'This message will be viewable by everyone.');
//     if (confirm) {
//       await locator<TemplatesService>()
//           .updateTemplate(templateID: template.id, data: {'safe': true});
//       Navigator.of(context).pop();
//     }
//   }
//
//   void delete() async {
//     bool confirm = await locator<ModalService>().showConfirmation(
//         context: context,
//         title: 'Delete Message?',
//         message: 'This message will be delete permanatly.');
//     if (confirm) {
//       await locator<TemplatesService>().deleteTemplate(templateID: template.id);
//       Navigator.of(context).pop();
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: FutureBuilder(
//         future: getCurrentUserFuture,
//         builder: (BuildContext context, AsyncSnapshot snapshot) {
//           switch (snapshot.connectionState) {
//             case ConnectionState.waiting:
//               return Spinner(
//                 text: 'Loading...',
//               );
//               break;
//             default:
//               return Stack(
//                 children: <Widget>[
//                   Container(
//                     color: Colors.white,
//                     width: screenWidth,
//                     height: screenHeight,
//                   ),
//                   Stack(
//                     children: <Widget>[
//                       Container(
//                         height: 300.0,
//                         width: screenWidth,
//                         decoration: BoxDecoration(
//                           image: DecorationImage(
//                             colorFilter: ColorFilter.mode(
//                                 Colors.black.withOpacity(0.3),
//                                 BlendMode.darken),
//                             image: category.image.image,
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                       Padding(
//                         padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
//                         child: Row(
//                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                           children: <Widget>[
//                             IconButton(
//                               icon: Icon(
//                                 Icons.chevron_left,
//                                 color: Colors.white,
//                               ),
//                               onPressed: () {
//                                 Navigator.of(context).pop();
//                               },
//                             ),
//                             Text(
//                               category.title,
//                               style: TextStyle(
//                                   fontSize: 18,
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold),
//                             ),
//                             IconButton(
//                               icon: Icon(
//                                 Icons.refresh,
//                                 color: Colors.white.withOpacity(0),
//                               ),
//                               onPressed: () {
//                                 //Navigator.of(context).pop();
//                               },
//                             ),
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                   Positioned(
//                     top: imageSize,
//                     child: Container(
//                       height: screenHeight - imageSize,
//                       width: screenWidth,
//                       decoration: BoxDecoration(
//                         color: Colors.white,
//                         borderRadius: BorderRadius.all(
//                           Radius.circular(40),
//                         ),
//                       ),
//                       child: Padding(
//                         padding: EdgeInsets.only(top: listViewBuilderPadding),
//                         child: Container(
//                             height: screenHeight -
//                                 imageSize +
//                                 listViewBuilderPadding,
//                             width: screenWidth,
//                             child: Padding(
//                               padding: EdgeInsets.fromLTRB(40, 20, 40, 20),
//                               child: Column(
//                                 children: <Widget>[
//                                   CircleAvatar(
//                                     radius: 30,
//                                     backgroundImage: NetworkImage(
//                                       user.imgUrl,
//                                     ),
//                                   ),
//                                   SizedBox(
//                                     height: 20,
//                                   ),
//                                   Text(
//                                     user.username,
//                                     style: TextStyle(
//                                         color: Colors.grey, fontSize: 16),
//                                   ),
//                                   SizedBox(
//                                     height: 20,
//                                   ),
//                                   Container(
//                                     color: Colors.red,
//                                     width: screenWidth * 0.65,
//                                     height: 2,
//                                   ),
//                                   SizedBox(
//                                     height: 20,
//                                   ),
//                                   Text(
//                                     template.message,
//                                     style: TextStyle(fontSize: 16),
//                                     textAlign: TextAlign.center,
//                                   ),
//                                   Spacer(),
//                                   Padding(
//                                     padding: EdgeInsets.only(bottom: 20),
//                                     child: Text(
//                                       'Approve or Delete this template?',
//                                       style: TextStyle(
//                                         fontSize: 16,
//                                         fontWeight: FontWeight.bold,
//                                       ),
//                                     ),
//                                   ),
//                                   Row(
//                                     children: <Widget>[
//                                       Expanded(
//                                         child: FloatingActionButton(
//                                           heroTag: 'ApproveFAB',
//                                           backgroundColor: Colors.red,
//                                           child: Icon(MdiIcons.check),
//                                           onPressed: approve,
//                                         ),
//                                       ),
//                                       Expanded(
//                                         child: FloatingActionButton(
//                                             heroTag: 'DeleteFAB',
//                                             backgroundColor: Colors.red,
//                                             child: Icon(MdiIcons.delete),
//                                             onPressed: delete),
//                                       ),
//                                     ],
//                                   )
//                                 ],
//                               ),
//                             )),
//                       ),
//                     ),
//                   )
//                 ],
//               );
//           }
//         },
//       ),
//     );
//   }
// }
