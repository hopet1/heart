// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/services/InAppReviewService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/services/ValidatorService.dart';
//
// import '../../ServiceLocator.dart';
//
// class CopyToClipboardPage extends StatefulWidget {
//   CopyToClipboardPage({@required this.template});
//   final TemplateModel template;
//
//   @override
//   State createState() => CopyToClipboardPageState(template: template);
// }
//
// class CopyToClipboardPageState extends State<CopyToClipboardPage> {
//   CopyToClipboardPageState({@required this.template});
//   final TemplateModel template;
//   TextEditingController messageController = TextEditingController();
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
//   bool autoValidate = false;
//
//   @override
//   void initState() {
//     super.initState();
//
//     messageController.text = template.message;
//   }
//
//   void save() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//
//       await locator<TemplatesService>().updateTemplate(
//           templateID: template.id, data: {'copies': template.copies + 1});
//       Clipboard.setData(ClipboardData(text: messageController.text));
//       locator<ModalService>()
//           .showInSnackBar(context: context, message: 'Message copied.');
//     } else {
//       autoValidate = true;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           backgroundColor: Colors.black,
//           title: Text(
//             'Any Changes First?',
//             style: TextStyle(
//               fontWeight: FontWeight.bold,
//             ),
//           ),
//         ),
//         key: scaffoldKey,
//         backgroundColor: Colors.white,
//         body: SafeArea(
//           child: Form(
//             autovalidateMode: AutovalidateMode.onUserInteraction,
//             key: formKey,
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Padding(
//                   padding: EdgeInsets.all(30),
//                   child: TextFormField(
//                     keyboardType: TextInputType.multiline,
//                     maxLines: null,
//                     textCapitalization: TextCapitalization.sentences,
//                     controller: messageController,
//                     textInputAction: TextInputAction.done,
//                     onFieldSubmitted: (term) {},
//                     validator: locator<ValidatorService>().password,
//                     onSaved: (value) {},
//                   ),
//                 ),
//                 Spacer(),
//                 Row(
//                   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                   children: <Widget>[
//                     RaisedButton(
//                       color: Colors.red,
//                       textColor: Colors.white,
//                       child: Text('Copy To Clipboard'),
//                       onPressed: () async {
//                         save();
//
//                         locator<InAppReviewService>().requestReview();
//                       },
//                     )
//                   ],
//                 ),
//               ],
//             ),
//           ),
//         ));
//   }
// }
