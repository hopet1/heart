// import 'package:flutter/material.dart';
// import 'package:heart/data.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import '../../ServiceLocator.dart';
//
// class EditTemplatePage extends StatefulWidget {
//   EditTemplatePage({@required this.template});
//   final TemplateModel template;
//
//   @override
//   State createState() => EditTemplatePageState(template: template);
// }
//
// class EditTemplatePageState extends State<EditTemplatePage> {
//   EditTemplatePageState({@required this.template});
//   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final TemplateModel template;
//   final TextEditingController messageController = TextEditingController();
//   bool autoValidate = false;
//
//   @override
//   void initState() {
//     super.initState();
//     messageController.text = template.message;
//   }
//
//   delete() async {
//     bool confirm = await locator<ModalService>().showConfirmation(
//         context: context, title: 'Delete Message', message: 'Are you sure?');
//
//     if (confirm) {
//       await locator<TemplatesService>().deleteTemplate(templateID: template.id);
//       Navigator.of(context).pop();
//     }
//   }
//
//   submit() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//
//       bool confirm = await locator<ModalService>().showConfirmation(
//           context: context,
//           title: 'Update Template',
//           message:
//               'This template will be viewed by the HEART team for approval.');
//
//       if (confirm) {
//         try {
//           locator<ModalService>().showInSnackBar(
//               context: context, message: 'Submitting template...');
//
//           template.message = messageController.text;
//           template.safe = false;
//           template.time = DateTime.now();
//
//           await locator<TemplatesService>()
//               .updateTemplate(templateID: template.id, data: template.toMap());
//
//           locator<ModalService>().showInSnackBar(
//               context: context, message: 'Template waiting for approval.');
//         } catch (e) {
//           locator<ModalService>().showAlert(
//             context: context,
//             title: 'Error',
//             message: e.toString(),
//           );
//         }
//       }
//     } else {
//       autoValidate = true;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Edit Message',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SingleChildScrollView(
//         child: Container(
//           height: screenHeight,
//           width: screenWidth,
//           child: Padding(
//             padding: SCAFFOLD_PADDING,
//             child: Form(
//               autovalidateMode: AutovalidateMode.onUserInteraction,
//               key: formKey,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     margin: EdgeInsets.only(top: 60, left: 10, right: 10),
//                     child: TextFormField(
//                       keyboardType: TextInputType.multiline,
//                       maxLines: null,
//                       textCapitalization: TextCapitalization.sentences,
//                       controller: messageController,
//                       textInputAction: TextInputAction.done,
//                       onFieldSubmitted: (term) {},
//                       validator: locator<ValidatorService>().isEmpty,
//                       onSaved: (value) {},
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     children: <Widget>[
//                       Expanded(
//                         child: Padding(
//                           padding: EdgeInsets.fromLTRB(0, 0, 5, 0),
//                           child: MaterialButton(
//                             onPressed: submit,
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                               children: <Widget>[
//                                 Icon(Icons.save),
//                                 Text(
//                                   'SAVE',
//                                   style: TextStyle(
//                                     letterSpacing: 2.0,
//                                     fontSize: 18,
//                                     fontFamily: 'SFUIDisplay',
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 )
//                               ],
//                             ),
//                             color: Theme.of(context).buttonColor,
//                             elevation: 0,
//                             minWidth: screenWidth * 0.75,
//                             height: 50,
//                             textColor: Colors.white,
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(10),
//                             ),
//                           ),
//                         ),
//                       ),
//                       Expanded(
//                         child: Padding(
//                           padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
//                           child: MaterialButton(
//                             onPressed: delete,
//                             child: Row(
//                               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                               children: <Widget>[
//                                 Icon(MdiIcons.delete),
//                                 Text(
//                                   'DELETE',
//                                   style: TextStyle(
//                                     letterSpacing: 2.0,
//                                     fontSize: 18,
//                                     fontFamily: 'SFUIDisplay',
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 )
//                               ],
//                             ),
//                             color: Colors.white,
//                             elevation: 0,
//                             minWidth: screenWidth * 0.75,
//                             height: 50,
//                             textColor: Colors.red,
//                             shape: RoundedRectangleBorder(
//                               borderRadius: BorderRadius.circular(10),
//                             ),
//                           ),
//                         ),
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }
