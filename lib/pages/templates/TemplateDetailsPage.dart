// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/LikeModel.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/pages/templates/CopyToClipboardPage.dart';
// import 'package:heart/pages/templates/EditTemplatePage.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/LikesDBService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:async/async.dart';
// import 'package:simple_gesture_detector/simple_gesture_detector.dart';
// import 'package:page_transition/page_transition.dart';
// import '../../ServiceLocator.dart';

// class TemplateDetailsPage extends StatefulWidget {
//   TemplateDetailsPage(
//       {@required this.templateID,
//       @required this.category,
//       @required this.userOfTemplate});
//   final String templateID;
//   final CategoryModel category;
//   final UserModel userOfTemplate;

//   @override
//   State createState() => TemplateDetailsPageState(
//       templateID: templateID,
//       category: category,
//       userOfTemplate: userOfTemplate);
// }

// class TemplateDetailsPageState extends State<TemplateDetailsPage> {
//   TemplateDetailsPageState(
//       {@required this.templateID,
//       @required this.category,
//       @required this.userOfTemplate});

//   final String templateID;
//   final CategoryModel category;
//   final UserModel userOfTemplate;
//   final double imageSize = 200;
//   final double listViewBuilderPadding = 10;

//   Stream<DocumentSnapshot> templateStream;
//   Stream<QuerySnapshot> likesStream;
//   DocumentSnapshot templateSnapshot;

//   Future<UserModel> getCurrentUserFuture =
//       locator<AuthService>().getCurrentUser();

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     templateStream.listen((x) {}).cancel();
//     likesStream.listen((x) {}).cancel();
//   }

// //Returns the adjacent template, (next or previous).
//   void getNextTemplate({@required bool forward}) async {
//     DocumentSnapshot nextTemplateSnapshot = await locator<TemplatesService>()
//         .retrieveNextTemplate(
//             templateSnapshot: templateSnapshot,
//             category: category.title,
//             forward: forward);

//     if (nextTemplateSnapshot == null) {
//       locator<ModalService>().showAlert(
//           context: context, title: 'Sorry', message: 'End of messages');
//       return;
//     }

//     // TemplateModel nextTemplate =
//     //     TemplateModel.extractDocument(nextTemplateSnapshot);
//     // UserModel nextUser =
//     //     await locator<UsersDBService>().retrieveUser(uid: nextTemplate.userID);

//     // Navigator.pushReplacement(
//     //   context,
//     //   PageTransition(
//     //     type: forward
//     //         ? PageTransitionType.leftToRightWithFade
//     //         : PageTransitionType.rightToLeftWithFade,
//     //     child: TemplateDetailsPage(
//     //       templateID: nextTemplate.id,
//     //       category: category,
//     //       userOfTemplate: nextUser,
//     //     ),
//     //   ),
//     // );
//   }

//   @override
//   Widget build(BuildContext context) {
//     //Get screen width.
//     double screenWidth = MediaQuery.of(context).size.width;
//     //Get screen height.
//     double screenHeight = MediaQuery.of(context).size.height;
//     //Create stream for watching template info.
//     templateStream =
//         locator<TemplatesService>().streamTemplate(templateID: templateID);
//     //Create stream for likes array.
//     likesStream = locator<LikesDBService>().streamLikes(templateID: templateID);
//     //Create stream zip to hold both streams.
//     StreamZip streams = StreamZip([templateStream, likesStream]);

//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: FutureBuilder(
//         future: getCurrentUserFuture,
//         builder: (BuildContext context, AsyncSnapshot futureSnapshot) {
//           switch (futureSnapshot.connectionState) {
//             case ConnectionState.waiting:
//               return Spinner(
//                 text: 'Loading...',
//               );
//               break;
//             default:
//               //Set current user of app.
//               UserModel currentUser = futureSnapshot.data;
//               return StreamBuilder(
//                 stream: streams,
//                 builder: (BuildContext context, AsyncSnapshot streamSnapshots) {
//                   switch (streamSnapshots.connectionState) {
//                     case ConnectionState.waiting:
//                       return loadingView(
//                           screenHeight: screenHeight, screenWidth: screenWidth);
//                       break;
//                     default:
//                       //Get snapshot of template.
//                       templateSnapshot = streamSnapshots.data[0];
//                       //Get snapshots of likes for this template.
//                       QuerySnapshot likesQuerySnapshot =
//                           streamSnapshots.data[1];
//                       //If the template document no longer exists, pop the user back to the previous screen.
//                       if (templateSnapshot == null) {
//                         Navigator.of(context).pop();
//                         return null;
//                       } else {
//                         //Set template.
//                         TemplateModel template =
//                             TemplateModel.extractDocument(templateSnapshot);
//                         //Set likes.
//                         List<DocumentSnapshot> likesDocumentSnapshots =
//                             likesQuerySnapshot.docs;
//                         List<LikeModel> likes = [];
//                         for (var i = 0;
//                             i < likesDocumentSnapshots.length;
//                             i++) {
//                           DocumentSnapshot documentSnapshot =
//                               likesDocumentSnapshots[i];
//                           LikeModel like =
//                               LikeModel.extractDocument(documentSnapshot);
//                           likes.add(like);
//                         }
//                         return SimpleGestureDetector(
//                           onHorizontalSwipe: (SwipeDirection direction) {
//                             if (currentUser.activeSubscription != null) {
//                               //If user swipes left, retrieve previous template.
//                               if (direction == SwipeDirection.left)
//                                 getNextTemplate(forward: false);
//                               //If user swipes right, retrieve next template.
//                               else
//                                 getNextTemplate(forward: true);
//                             }
//                           },
//                           swipeConfig: SimpleSwipeConfig(
//                             verticalThreshold: 40.0,
//                             horizontalThreshold: 40.0,
//                             swipeDetectionBehavior:
//                                 SwipeDetectionBehavior.continuousDistinct,
//                           ),
//                           child: Stack(
//                             children: <Widget>[
//                               Container(
//                                 color: Colors.white,
//                                 width: screenWidth,
//                                 height: screenHeight,
//                               ),
//                               Stack(
//                                 children: <Widget>[
//                                   Container(
//                                     child: Center(
//                                       child: Text(
//                                         '${template.count} ${template.count == 1 ? 'person likes' : 'people like'} this message.',
//                                         style: TextStyle(
//                                             color: Colors.white,
//                                             fontSize: 18,
//                                             fontWeight: FontWeight.bold),
//                                       ),
//                                     ),
//                                     height: 300.0,
//                                     width: screenWidth,
//                                     decoration: BoxDecoration(
//                                       image: DecorationImage(
//                                         colorFilter: ColorFilter.mode(
//                                             Colors.black.withOpacity(0.3),
//                                             BlendMode.darken),
//                                         image: category.image.image,
//                                         fit: BoxFit.cover,
//                                       ),
//                                     ),
//                                   ),
//                                   Padding(
//                                     padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
//                                     child: Row(
//                                       mainAxisAlignment:
//                                           MainAxisAlignment.spaceBetween,
//                                       children: <Widget>[
//                                         IconButton(
//                                           icon: Icon(
//                                             Icons.chevron_left,
//                                             color: Colors.white,
//                                           ),
//                                           onPressed: () {
//                                             Navigator.of(context).pop();
//                                           },
//                                         ),
//                                         Text(
//                                           category.title,
//                                           style: TextStyle(
//                                               fontSize: 18,
//                                               color: Colors.white,
//                                               fontWeight: FontWeight.bold),
//                                         ),
//                                         IconButton(
//                                           icon: Icon(
//                                             Icons.refresh,
//                                             color: Colors.white.withOpacity(0),
//                                           ),
//                                           onPressed: () {
//                                             //Navigator.of(context).pop();
//                                           },
//                                         ),
//                                       ],
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                               Positioned(
//                                 top: imageSize,
//                                 child: Container(
//                                   height: screenHeight - imageSize,
//                                   width: screenWidth,
//                                   decoration: BoxDecoration(
//                                     color: Colors.white,
//                                     borderRadius: BorderRadius.all(
//                                       Radius.circular(40),
//                                     ),
//                                   ),
//                                   child: Padding(
//                                     padding: EdgeInsets.only(
//                                         top: listViewBuilderPadding),
//                                     child: Container(
//                                       height: screenHeight -
//                                           imageSize +
//                                           listViewBuilderPadding,
//                                       width: screenWidth,
//                                       child: Padding(
//                                         padding:
//                                             EdgeInsets.fromLTRB(40, 20, 40, 20),
//                                         child: Column(
//                                           children: <Widget>[
//                                             CircleAvatar(
//                                               radius: 30,
//                                               backgroundImage: NetworkImage(
//                                                 userOfTemplate.imgUrl,
//                                               ),
//                                             ),
//                                             SizedBox(
//                                               height: 20,
//                                             ),
//                                             Text(
//                                               userOfTemplate.username,
//                                               style: TextStyle(
//                                                   color: Colors.grey,
//                                                   fontSize: 16),
//                                             ),
//                                             SizedBox(
//                                               height: 20,
//                                             ),
//                                             Container(
//                                               color: Colors.red,
//                                               width: screenWidth * 0.65,
//                                               height: 2,
//                                             ),
//                                             SizedBox(
//                                               height: 20,
//                                             ),
//                                             Text(
//                                               template.message,
//                                               style: TextStyle(fontSize: 16),
//                                               textAlign: TextAlign.center,
//                                             ),
//                                             Spacer(),
//                                             Row(
//                                               children: <Widget>[
//                                                 Expanded(
//                                                   child: FloatingActionButton(
//                                                     heroTag:
//                                                         'FloatingActionButton1',
//                                                     backgroundColor: Colors.red,
//                                                     child: likes
//                                                             .where((like) =>
//                                                                 (like.userID ==
//                                                                     currentUser
//                                                                         .uid))
//                                                             .isNotEmpty
//                                                         ? Icon(MdiIcons.heart)
//                                                         : Icon(MdiIcons
//                                                             .heartOutline),
//                                                     onPressed: () {
//                                                       if (likes
//                                                           .where((like) => (like
//                                                                   .userID ==
//                                                               currentUser.uid))
//                                                           .isNotEmpty) {
//                                                         locator<LikesDBService>()
//                                                             .deleteLike(
//                                                                 templateID:
//                                                                     template.id,
//                                                                 userID:
//                                                                     currentUser
//                                                                         .uid);
//                                                       } else {
//                                                         LikeModel newLike =
//                                                             LikeModel(
//                                                                 userID:
//                                                                     currentUser
//                                                                         .uid);
//                                                         locator<LikesDBService>()
//                                                             .createLike(
//                                                           templateID:
//                                                               template.id,
//                                                           like: newLike,
//                                                         );
//                                                       }
//                                                     },
//                                                   ),
//                                                 ),
//                                                 Expanded(
//                                                   child: FloatingActionButton(
//                                                     heroTag:
//                                                         'FloatingActionButton2',
//                                                     backgroundColor: Colors.red,
//                                                     child:
//                                                         Icon(MdiIcons.download),
//                                                     onPressed: () {
//                                                       Navigator.push(
//                                                         context,
//                                                         MaterialPageRoute(
//                                                           builder: (context) =>
//                                                               CopyToClipboardPage(
//                                                             template: template,
//                                                           ),
//                                                         ),
//                                                       );
//                                                     },
//                                                   ),
//                                                 ),
//                                                 (currentUser.uid ==
//                                                             userOfTemplate
//                                                                 .uid ||
//                                                         currentUser.isAdmin)
//                                                     ? Expanded(
//                                                         child:
//                                                             FloatingActionButton(
//                                                           heroTag:
//                                                               'FloatingActionButton3',
//                                                           backgroundColor:
//                                                               Colors.red,
//                                                           child:
//                                                               Icon(Icons.edit),
//                                                           onPressed: () {
//                                                             Navigator.push(
//                                                               context,
//                                                               MaterialPageRoute(
//                                                                 builder:
//                                                                     (context) =>
//                                                                         EditTemplatePage(
//                                                                   template:
//                                                                       template,
//                                                                 ),
//                                                               ),
//                                                             );
//                                                           },
//                                                         ),
//                                                       )
//                                                     : SizedBox.shrink()
//                                               ],
//                                             ),
//                                             SizedBox(
//                                               height: 40,
//                                             )
//                                           ],
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 ),
//                               )
//                             ],
//                           ),
//                         );
//                       }
//                   }
//                 },
//               );
//           }
//         },
//       ),
//     );
//   }

//   //View of the page before the data is loaded.
//   Widget loadingView(
//       {@required double screenWidth, @required double screenHeight}) {
//     return Stack(
//       children: <Widget>[
//         Container(
//           color: Colors.white,
//           width: screenWidth,
//           height: screenHeight,
//         ),
//         Stack(
//           children: <Widget>[
//             Container(
//               child: Center(
//                 child: Text(
//                   'Loading...',
//                   style: TextStyle(
//                       color: Colors.white,
//                       fontSize: 18,
//                       fontWeight: FontWeight.bold),
//                 ),
//               ),
//               height: 300.0,
//               width: screenWidth,
//               decoration: BoxDecoration(
//                 image: DecorationImage(
//                   colorFilter: ColorFilter.mode(
//                       Colors.black.withOpacity(0.3), BlendMode.darken),
//                   image: category.image.image,
//                   fit: BoxFit.cover,
//                 ),
//               ),
//             ),
//             Padding(
//               padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   IconButton(
//                     icon: Icon(
//                       Icons.chevron_left,
//                       color: Colors.white,
//                     ),
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                   ),
//                   Text(
//                     category.title,
//                     style: TextStyle(
//                         fontSize: 18,
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold),
//                   ),
//                   IconButton(
//                     icon: Icon(
//                       Icons.refresh,
//                       color: Colors.white.withOpacity(0),
//                     ),
//                     onPressed: () {
//                       //Navigator.of(context).pop();
//                     },
//                   ),
//                 ],
//               ),
//             ),
//           ],
//         ),
//         Positioned(
//           top: imageSize,
//           child: Container(
//             height: screenHeight - imageSize,
//             width: screenWidth,
//             decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.all(
//                 Radius.circular(40),
//               ),
//             ),
//             child: Padding(
//               padding: EdgeInsets.only(top: listViewBuilderPadding),
//               child: Container(
//                 height: screenHeight - imageSize + listViewBuilderPadding,
//                 width: screenWidth,
//                 child: Spinner(
//                   text: 'Loading...',
//                 ),
//               ),
//             ),
//           ),
//         )
//       ],
//     );
//   }
// }
