// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:getwidget/getwidget.dart';
// import 'package:heart/blocs/login/Bloc.dart' as LOGIN_BP;
// import 'package:heart/blocs/signup/Bloc.dart' as SIGN_UP_BP;
//
// class LandingPage extends StatefulWidget {
//   @override
//   State createState() => LandingPageState();
// }
//
// class LandingPageState extends State<LandingPage> {
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     final double height = MediaQuery.of(context).size.height;
//     final double width = MediaQuery.of(context).size.width;
//     return Scaffold(
//         backgroundColor: Colors.white,
//         body: SafeArea(
//           child: Stack(
//             children: <Widget>[
//               // Column(
//               //   crossAxisAlignment: CrossAxisAlignment.start,
//               //   children: <Widget>[
//               //     Padding(
//               //       padding: EdgeInsets.fromLTRB(12, 30, 12, 8),
//               //       child: Row(
//               //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               //         children: <Widget>[
//               //           InkWell(
//               //             child: Text(
//               //               'Login',
//               //               style: TextStyle(fontSize: 15),
//               //             ),
//               //             onTap: () {
//               //               Route route = MaterialPageRoute(
//               //                 builder: (BuildContext context) => BlocProvider(
//               //                   create: (BuildContext context) => LoginBloc()
//               //                     ..add(
//               //                       LoadPageEvent(),
//               //                     ),
//               //                   child: LoginPage(),
//               //                 ),
//               //               );
//               //               Navigator.push(
//               //                 context,
//               //                 route,
//               //               );
//               //             },
//               //           ),
//               //           InkWell(
//               //             child: Text(
//               //               'Sign Up',
//               //               style: TextStyle(
//               //                   color: Colors.red,
//               //                   fontWeight: FontWeight.bold,
//               //                   fontSize: 16),
//               //             ),
//               //             onTap: () {
//               //               Route route = MaterialPageRoute(
//               //                 builder: (BuildContext context) => SignUpPage(),
//               //               );
//               //               Navigator.push(
//               //                 context,
//               //                 route,
//               //               );
//               //             },
//               //           )
//               //         ],
//               //       ),
//               //     ),
//               //     Container(
//               //       margin: EdgeInsets.only(top: 30),
//               //       child: Text(
//               //         '',
//               //         textAlign: TextAlign.left,
//               //         style: TextStyle(
//               //           color: Colors.black,
//               //           fontFamily: "Avenir",
//               //           fontWeight: FontWeight.w800,
//               //           fontSize: 34,
//               //         ),
//               //       ),
//               //     ),
//               //   ],
//               // ),
//               // SimpleNavbar(
//               //   leftWidget: Text(
//               //     'Login',
//               //     style: TextStyle(fontSize: 15),
//               //   ),
//               //   leftTap: () {
//               //     Route route = MaterialPageRoute(
//               //       builder: (BuildContext context) => BlocProvider(
//               //         create: (BuildContext context) => LoginBloc()
//               //           ..add(
//               //             LoadPageEvent(),
//               //           ),
//               //         child: LoginPage(),
//               //       ),
//               //     );
//               //     Navigator.push(
//               //       context,
//               //       route,
//               //     );
//               //   },
//               //   rightWidget: Text(
//               //     'Sign Up',
//               //     style: TextStyle(fontSize: 15),
//               //   ),
//               //   rightTap: () {
//               //     Route route = MaterialPageRoute(
//               //       builder: (BuildContext context) => SignUpPage(),
//               //     );
//               //     Navigator.push(
//               //       context,
//               //       route,
//               //     );
//               //   },
//               // ),
//               Center(
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     Spacer(),
//                     Text('Feelings Unleashed',
//                         style: TextStyle(
//                             color: Color(0xff757575),
//                             fontStyle: FontStyle.normal,
//                             fontSize: 25.0),
//                         textAlign: TextAlign.center),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     GFButton(
//                       onPressed: () {
//                         Route route = MaterialPageRoute(
//                           builder: (BuildContext context) => BlocProvider(
//                             create: (BuildContext context) =>
//                                 LOGIN_BP.LoginBloc()
//                                   ..add(
//                                     LOGIN_BP.LoadPageEvent(),
//                                   ),
//                             child: LOGIN_BP.LoginPage(),
//                           ),
//                         );
//                         Navigator.push(
//                           context,
//                           route,
//                         );
//                       },
//                       color: Colors.red,
//                       text: 'Login',
//                       textColor: Colors.white,
//                       type: GFButtonType.solid,
//                     ),
//                     GFButton(
//                       onPressed: () {
//                         Route route = MaterialPageRoute(
//                           builder: (BuildContext context) => BlocProvider(
//                             create: (BuildContext context) =>
//                                 SIGN_UP_BP.SignUpBloc()
//                             // ..add(
//                             //   SIGN_UP_BP.LoadPageEvent(),
//                             // ),
//                             ,
//                             child: SIGN_UP_BP.SignUpPage(),
//                           ),
//                         );
//                         Navigator.push(
//                           context,
//                           route,
//                         );
//
//                         // Route route = MaterialPageRoute(
//                         //   builder: (BuildContext context) => SignUpPage(),
//                         // );
//                         // Navigator.push(
//                         //   context,
//                         //   route,
//                         // );
//                       },
//                       color: Colors.red,
//                       text: 'Sign Up',
//                       textColor: Colors.red,
//                       type: GFButtonType.outline,
//                     ),
//                   ],
//                 ),
//               ),
//               Center(
//                 child: Image.asset(
//                   'assets/images/heartbeat.png',
//                   width: width * .7,
//                   height: height * .7,
//                 ),
//               ),
//               // Positioned(
//               //   bottom: 100,
//               //   left: 0,
//               //   right: 0,
//               //   child: Text('Feelings Unleashed',
//               //       style: TextStyle(
//               //           color: Color(0xff757575),
//               //           fontStyle: FontStyle.normal,
//               //           fontSize: 25.0),
//               //       textAlign: TextAlign.center),
//               // ),
//             ],
//           ),
//         ));
//   }
// }
