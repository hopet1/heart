// import 'package:flutter/material.dart';
// import 'package:heart/data.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import 'package:heart/services/WordOfTheDayDBService.dart';
//
// import '../../ServiceLocator.dart';
//
// class AdminAddWordPage extends StatefulWidget {
//   @override
//   State createState() => AdminAddWordPageState();
// }
//
// class AdminAddWordPageState extends State<AdminAddWordPage> {
//   bool isLoading = true;
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final TextEditingController wordController = TextEditingController();
//   final formKey = GlobalKey<FormState>();
//   bool autoValidate = false;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Create Word of The Day',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SingleChildScrollView(
//         child: Container(
//           height: screenHeight,
//           width: screenWidth,
//           child: Padding(
//             padding: SCAFFOLD_PADDING,
//             child: Form(
//               autovalidateMode: AutovalidateMode.onUserInteraction,
//               key: formKey,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     margin: EdgeInsets.only(top: 60, left: 10, right: 10),
//                     child: TextFormField(
//                       keyboardType: TextInputType.multiline,
//                       maxLines: null,
//                       textCapitalization: TextCapitalization.sentences,
//                       controller: wordController,
//                       textInputAction: TextInputAction.done,
//                       onFieldSubmitted: (term) {},
//                       validator: locator<ValidatorService>().isEmpty,
//                       onSaved: (value) {},
//                       decoration: InputDecoration(hintText: 'Enter word'),
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: <Widget>[
//                       RaisedButton(
//                         textColor: Colors.white,
//                         color: Colors.red,
//                         child: Text('Save'),
//                         onPressed: () {
//                           submit();
//                         },
//                       )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   void submit() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//       bool confirm = await locator<ModalService>().showConfirmation(
//           context: context, title: 'Submit', message: 'Are you sure?');
//
//       if (confirm) {
//         //Save new quote of the day to database.
//         locator<WordOfTheDayDBService>().createWOTD(text: wordController.text);
//
//         //Clear text field.
//         wordController.clear();
//
//         Navigator.of(context).pop();
//       }
//     } else {
//       autoValidate = true;
//     }
//   }
// }
