// import 'package:flutter/material.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/data.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/widgets/UserTemplateListTile.dart';
//
// class AdminTemplatesPage extends StatefulWidget {
//   @override
//   State createState() => AdminTemplatesPageState();
// }
//
// class AdminTemplatesPageState extends State<AdminTemplatesPage> {
//   bool viewingApproved = true;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Templates',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       backgroundColor: Colors.white,
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Padding(
//             padding: EdgeInsets.fromLTRB(40, 0, 40, 0),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: <Widget>[
//                 MaterialButton(
//                   color: viewingApproved ? Colors.red : Colors.white,
//                   child: Text(
//                     'Approved',
//                     style: TextStyle(
//                         color: viewingApproved ? Colors.white : Colors.black),
//                   ),
//                   onPressed: () {
//                     setState(() {
//                       viewingApproved = true;
//                     });
//                   },
//                 ),
//                 MaterialButton(
//                   color: viewingApproved ? Colors.white : Colors.red,
//                   child: Text(
//                     'Unapproved',
//                     style: TextStyle(
//                         color: viewingApproved ? Colors.black : Colors.white),
//                   ),
//                   onPressed: () {
//                     setState(() {
//                       viewingApproved = false;
//                     });
//                   },
//                 )
//               ],
//             ),
//           ),
//           Expanded(
//             child: FutureBuilder(
//               future: locator<TemplatesService>()
//                   .retrieveTemplates(safe: viewingApproved),
//               builder: (BuildContext context,
//                   AsyncSnapshot<List<TemplateModel>> snapshot) {
//                 switch (snapshot.connectionState) {
//                   case ConnectionState.waiting:
//                     return Spinner(
//                       text: 'Loading...',
//                     );
//                   default:
//                     if (snapshot.hasError) {
//                       return Center(
//                         child:
//                             Text('Could not fetch templates at this time...'),
//                       );
//                     } else if (snapshot.data.isEmpty) {
//                       return Center(
//                         child: Text(
//                           'No templates at this time',
//                           style: TextStyle(color: Colors.grey),
//                         ),
//                       );
//                     } else {
//                       List<TemplateModel> templates = snapshot.data;
//
//                       return ListView.builder(
//                           itemCount: templates.length,
//                           itemBuilder: (BuildContext context, int index) {
//                             TemplateModel template = templates[index];
//
//                             return UserTemplateListTile(
//                               forAdmin: !viewingApproved,
//                               template: template,
//                               category: categories.firstWhere(
//                                   (c) => c.title == template.category),
//                             );
//                           });
//                     }
//                 }
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
