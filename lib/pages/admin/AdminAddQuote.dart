// import 'package:flutter/material.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/data.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/QuoteOfTheDayDBService.dart';
// import 'package:heart/services/ValidatorService.dart';
//
// class AdminAddQuotePage extends StatefulWidget {
//   @override
//   State createState() => AdminAddQuotePageState();
// }
//
// class AdminAddQuotePageState extends State<AdminAddQuotePage> {
//   bool isLoading = true;
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final TextEditingController quoteController = TextEditingController();
//   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
//   bool autoValidate = false;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Create Quote of The Day',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SingleChildScrollView(
//         child: Container(
//           height: screenHeight,
//           width: screenWidth,
//           child: Padding(
//             padding: SCAFFOLD_PADDING,
//             child: Form(
//               autovalidateMode: AutovalidateMode.onUserInteraction,
//               key: formKey,
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: [
//                   Container(
//                     margin: EdgeInsets.only(top: 60, left: 10, right: 10),
//                     child: TextFormField(
//                       keyboardType: TextInputType.multiline,
//                       maxLines: null,
//                       textCapitalization: TextCapitalization.sentences,
//                       controller: quoteController,
//                       textInputAction: TextInputAction.done,
//                       onFieldSubmitted: (term) {},
//                       validator: locator<ValidatorService>().isEmpty,
//                       onSaved: (value) {},
//                       decoration: InputDecoration(hintText: 'Enter quote'),
//                     ),
//                   ),
//                   Spacer(),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                     children: <Widget>[
//                       RaisedButton(
//                         color: Colors.red,
//                         textColor: Colors.white,
//                         child: Text('Save'),
//                         onPressed: () {
//                           submit();
//                         },
//                       )
//                       // SilentButton(
//                       //   title: 'SAVE',
//                       //   onTap: () => {submit()},
//                       // )
//                     ],
//                   ),
//                 ],
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   void submit() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//
//       bool confirm = await locator<ModalService>().showConfirmation(
//           context: context, title: 'Submit', message: 'Are you sure?');
//
//       if (confirm) {
//         //Save new quote of the day to database.
//         locator<QuoteOfTheDayDBService>()
//             .createQOTD(text: quoteController.text);
//
//         //Clear text field.
//         quoteController.clear();
//
//         Navigator.of(context).pop();
//       }
//     } else {
//       autoValidate = true;
//     }
//   }
// }
