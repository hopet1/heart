// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
// import 'package:heart/pages/admin/AdminAddWord.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/WordOfTheDayDBService.dart';
// import 'package:heart/widgets/TextOfTheDayTile.dart';
//
// class AdminWordsListPage extends StatefulWidget {
//   @override
//   State createState() => AdminWordsListPageState();
// }
//
// class AdminWordsListPageState extends State<AdminWordsListPage> {
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Words of The Day',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.add),
//             onPressed: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => AdminAddWordPage(),
//                 ),
//               );
//             },
//           )
//         ],
//       ),
//       backgroundColor: Colors.white,
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Expanded(
//             child: StreamBuilder<QuerySnapshot>(
//               stream: locator<WordOfTheDayDBService>().streamWordsOfTheDay(),
//               builder: (BuildContext context,
//                   AsyncSnapshot<QuerySnapshot> snapshot) {
//                 switch (snapshot.connectionState) {
//                   case ConnectionState.waiting:
//                     return Spinner(
//                       text: 'Loading...',
//                     );
//                     break;
//                   default:
//                     return ListView.builder(
//                       shrinkWrap: true,
//                       itemCount: snapshot.data.docs.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         // TextOfTheDayModel textOfTheDay =
//                         //     TextOfTheDayModel.extractDocument(
//                         //         snapshot.data.docs[index]);
//
//                         // return TextOfTheDayTile(
//                         //   textOfTheDay: textOfTheDay,
//                         //   select: () async {
//                         //     if (textOfTheDay.isActive == false) {
//                         //       //Prompt user for updated active word.
//                         //       bool confirm = await locator<ModalService>()
//                         //           .showConfirmation(
//                         //               context: context,
//                         //               title:
//                         //                   'Make this the active word of the day?',
//                         //               message: textOfTheDay.text);
//                         //       if (confirm) {
//                         //         //Update the selected quote to be active.
//                         //         await locator<WordOfTheDayDBService>()
//                         //             .updateActiveWOTD(wotdID: textOfTheDay.id);
//                         //         //Refresh page.
//                         //         //_refresh();
//                         //       }
//                         //     }
//                         //   },
//                         //   delete: () async {
//                         //     //Prompt user for deleting word of the day.
//                         //     bool confirm = await locator<ModalService>()
//                         //         .showConfirmation(
//                         //             context: context,
//                         //             title: 'Delete word of the day?',
//                         //             message: textOfTheDay.text);
//                         //     if (confirm) {
//                         //       //Remove quote of the day from database.
//                         //       await locator<WordOfTheDayDBService>()
//                         //           .deleteWOTD(wotdID: textOfTheDay.id);
//                         //     }
//                         //   },
//                         // );
//                       },
//                     );
//                 }
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
