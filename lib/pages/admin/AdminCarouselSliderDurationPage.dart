// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/data.dart';
// import 'package:heart/services/MetaDataDBService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
//
// import '../../ServiceLocator.dart';
//
// class AdminCarouselSliderDurationPage extends StatefulWidget {
//   @override
//   State createState() => AdminCarouselSliderDurationPageState();
// }
//
// class AdminCarouselSliderDurationPageState
//     extends State<AdminCarouselSliderDurationPage>
//     with SingleTickerProviderStateMixin {
//   final TextEditingController carouselSliderDurationController =
//       TextEditingController();
//   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
//   bool autoValidate = false;
//   Future<int> getCarouselSliderDurationFuture =
//       locator<MetaDataDBService>().getCarouselSliderDuration();
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   void save() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//
//       bool confirm = await locator<ModalService>().showConfirmation(
//           context: context, title: 'Update', message: 'Are you sure?');
//       if (confirm) {
//         int carouselSliderDuration =
//             int.parse(carouselSliderDurationController.text);
//         await locator<MetaDataDBService>().setCarouselSliderDuration(
//             carouselSliderDuration: carouselSliderDuration);
//         Navigator.of(context).pop();
//       }
//     } else {
//       autoValidate = true;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Carousel Slider Duration',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       body: FutureBuilder(
//         future: getCarouselSliderDurationFuture,
//         builder: (BuildContext context, AsyncSnapshot futureSnapshot) {
//           switch (futureSnapshot.connectionState) {
//             case ConnectionState.waiting:
//               return Spinner(
//                 text: 'Loading...',
//               );
//               break;
//             default:
//               //Set text controller to current duration.
//               int carouselSliderDuration = futureSnapshot.data;
//               carouselSliderDurationController.text = '$carouselSliderDuration';
//
//               return SingleChildScrollView(
//                 child: Container(
//                   width: screenWidth,
//                   height: screenHeight,
//                   child: Padding(
//                     padding: SCAFFOLD_PADDING,
//                     child: Form(
//                       autovalidateMode: AutovalidateMode.onUserInteraction,
//                       key: formKey,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           Container(
//                             margin:
//                                 EdgeInsets.only(top: 60, left: 10, right: 10),
//                             child: TextFormField(
//                               controller: carouselSliderDurationController,
//                               keyboardType: TextInputType.number,
//                               textInputAction: TextInputAction.done,
//                               validator: locator<ValidatorService>().isEmpty,
//                               onFieldSubmitted: (term) {},
//                               onSaved: (value) {},
//                               decoration: InputDecoration(
//                                 focusedBorder: UnderlineInputBorder(
//                                   borderSide: BorderSide(color: Colors.red),
//                                 ),
//                                 hintText: 'Enter number of seconds.',
//                                 // icon: Icon(Icons.email),
//                                 fillColor: Colors.white,
//                               ),
//                             ),
//                           ),
//                           Spacer(),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                             children: <Widget>[
//                               RaisedButton(
//                                 textColor: Colors.white,
//                                 color: Colors.red,
//                                 child: Text('Save'),
//                                 onPressed: () {
//                                   save();
//                                 },
//                               )
//                             ],
//                           ),
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               );
//           }
//         },
//       ),
//     );
//   }
// }
