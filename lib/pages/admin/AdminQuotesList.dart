// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
// import 'package:heart/pages/admin/AdminAddQuote.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/QuoteOfTheDayDBService.dart';
// import 'package:heart/widgets/TextOfTheDayTile.dart';
//
// import '../../ServiceLocator.dart';
//
// class AdminQuotesListPage extends StatefulWidget {
//   @override
//   State createState() => AdminQuotesListPageState();
// }
//
// class AdminQuotesListPageState extends State<AdminQuotesListPage> {
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Quotes of The Day',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.add),
//             onPressed: () {
//               Navigator.push(
//                 context,
//                 MaterialPageRoute(
//                   builder: (context) => AdminAddQuotePage(),
//                 ),
//               );
//             },
//           )
//         ],
//       ),
//       backgroundColor: Colors.white,
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Expanded(
//             child: StreamBuilder<QuerySnapshot>(
//               stream: locator<QuoteOfTheDayDBService>().streamQuotesOfTheDay(),
//               builder: (BuildContext context,
//                   AsyncSnapshot<QuerySnapshot> snapshot) {
//                 switch (snapshot.connectionState) {
//                   case ConnectionState.waiting:
//                     return Spinner(
//                       text: 'Loading...',
//                     );
//                     break;
//                   default:
//                     return ListView.builder(
//                       shrinkWrap: true,
//                       itemCount: snapshot.data.docs.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         // TextOfTheDayModel textOfTheDay =
//                         //     TextOfTheDayModel.extractDocument(
//                         //         snapshot.data.docs[index]);
//
//                         // return TextOfTheDayTile(
//                         //   textOfTheDay: textOfTheDay,
//                         //   select: () async {
//                         //     if (textOfTheDay.isActive == false) {
//                         //       //Prompt user for updated active quote.
//                         //       bool confirm = await locator<ModalService>()
//                         //           .showConfirmation(
//                         //               context: context,
//                         //               title:
//                         //                   'Make this the active quote of the day?',
//                         //               message: textOfTheDay.text);
//                         //       if (confirm) {
//                         //         //Update the selected quote to be active.
//                         //         await locator<QuoteOfTheDayDBService>()
//                         //             .updateActiveQOTD(qotdID: textOfTheDay.id);
//                         //         //Refresh page.
//                         //         //_refresh();
//                         //       }
//                         //     }
//                         //   },
//                         //   delete: () async {
//                         //     //Prompt user for deletingn quote of the day.
//                         //     bool confirm = await locator<ModalService>()
//                         //         .showConfirmation(
//                         //             context: context,
//                         //             title: 'Delete quote of the day?',
//                         //             message: textOfTheDay.text);
//                         //     if (confirm) {
//                         //       //Remove quote of the day fro database.
//                         //       await locator<QuoteOfTheDayDBService>()
//                         //           .deleteQOTD(qotdID: textOfTheDay.id);
//                         //       //Refresh the page.
//                         //       //_refresh();
//                         //     }
//                         //   },
//                         // );
//                       },
//                     );
//                 }
//               },
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }
