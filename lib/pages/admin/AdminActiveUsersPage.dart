// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/widgets/UserCircleAvatar.dart';
// import 'package:pagination/pagination.dart';
//
// import '../../ServiceLocator.dart';
//
// class ActiveUsersPage extends StatefulWidget {
//   @override
//   State createState() => ActiveUsersPageState();
// }
//
// class ActiveUsersPageState extends State<ActiveUsersPage> {
//   DocumentSnapshot startAfterDocument;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   Future<List<UserModel>> pageFetch(int offset) async {
//     final List<DocumentSnapshot> userDocs = await locator<UsersDBService>()
//         .retrieveUsersPagination(
//             limit: 10,
//             startAfterDocument: startAfterDocument,
//             orderBy: 'email');
//
//     if (userDocs.isEmpty) {
//       return [];
//     }
//
//     startAfterDocument = userDocs[userDocs.length - 1];
//
//     // final List<UserModel> users = userDocs
//     //     .map((userDoc) => UserModel.fromDocumentSnapshot(ds: userDoc))
//     //     .toList();
//
//     // return users;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Active Users',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       backgroundColor: Colors.white,
//       body: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Expanded(
//             child: PaginationList<UserModel>(
//               onLoading: Spinner(
//                 text: 'Loading...',
//               ),
//               onPageLoading: Spinner(
//                 text: 'Loading...',
//               ),
//               itemBuilder: (BuildContext context, UserModel user) {
//                 return buildUser(user: user);
//               },
//               pageFetch: pageFetch,
//               onError: (dynamic error) => Center(
//                 child: Text('Something Went Wrong'),
//               ),
//               onEmpty: Center(
//                 child: Text('Empty List'),
//               ),
//             ),
//           )
//         ],
//       ),
//     );
//   }
//
//   Widget buildUser({@required UserModel user}) {
//     return Column(
//       children: <Widget>[
//         ListTile(
//           leading: UserCircleAvatar(
//             imgUrl: user.imgUrl,
//             active: user.isOnline,
//           ),
//           title: Text(user.username),
//           subtitle: Text(user.email),
//         ),
//         Divider()
//       ],
//     );
//   }
// }
