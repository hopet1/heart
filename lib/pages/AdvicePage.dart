// import 'dart:math';
//
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/blocs/profile/Bloc.dart' as PROFILE_BP;
// import 'package:heart/blocs/message/Bloc.dart' as MESSAGE_BP;
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/widgets/UserCircleAvatar.dart';
//
// class AdvicePage extends StatefulWidget {
//   @override
//   State createState() => AdvicePageState();
// }
//
// class AdvicePageState extends State<AdvicePage> {
//   UserModel currentUser;
//   Future<UserModel> getCurrentUserFuture =
//       locator<AuthService>().getCurrentUser();
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Get Advice',
//           style: TextStyle(fontWeight: FontWeight.bold),
//         ),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.person),
//             onPressed: () {
//               Route route = MaterialPageRoute(
//                 builder: (BuildContext context) => BlocProvider(
//                   create: (BuildContext context) => PROFILE_BP.ProfileBloc()
//                     ..add(
//                       PROFILE_BP.LoadPageEvent(),
//                     ),
//                   child: PROFILE_BP.ProfilePage(),
//                 ),
//               );
//
//               Navigator.push(
//                 context,
//                 route,
//               );
//             },
//           )
//         ],
//       ),
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: FutureBuilder(
//           future: getCurrentUserFuture,
//           builder: (BuildContext context, AsyncSnapshot snapshot) {
//             switch (snapshot.connectionState) {
//               case ConnectionState.waiting:
//                 return Spinner(
//                   text: 'Loading...',
//                 );
//                 break;
//               default:
//                 //Set current user variable.
//                 currentUser = snapshot.data;
//
//                 return Column(
//                   children: [
//                     Expanded(
//                       child: StreamBuilder<QuerySnapshot>(
//                         stream: locator<UsersDBService>()
//                             .streamUsers(canGiveAdvice: true, isAdmin: false),
//                         builder: (BuildContext context,
//                             AsyncSnapshot<QuerySnapshot> snapshot) {
//                           switch (snapshot.connectionState) {
//                             case ConnectionState.waiting:
//                               return Spinner(
//                                 text: 'Loading...',
//                               );
//                               break;
//                             // default:
//                             //   return ListView.builder(
//                             //     shrinkWrap: true,
//                             //     itemCount: snapshot.data.docs.length,
//                             //     itemBuilder: (BuildContext context, int index) {
//                             //       UserModel adviceUser =
//                             //           UserModel.fromDocumentSnapshot(
//                             //               ds: snapshot.data.docs[index]);
//                             //       return buildAdviceUser(
//                             //           adviceUser: adviceUser);
//                             //     },
//                             //   );
//                           }
//                         },
//                       ),
//                     ),
//                     RaisedButton(
//                       child: Text(
//                         'Get Advice From HEART Team Member',
//                         style: TextStyle(
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       onPressed: () async {
//                         List<UserModel> heartTeamMembers =
//                             await locator<UsersDBService>()
//                                 .retrieveUsers(isAdmin: true);
//
//                         final int randomIndex =
//                             Random().nextInt(heartTeamMembers.length);
//
//                         UserModel chosenHeartTeamMember =
//                             heartTeamMembers[randomIndex];
//
//                         openMessage(adviceUser: chosenHeartTeamMember);
//                       },
//                       color: Colors.red,
//                       textColor: Colors.white,
//                     )
//                   ],
//                 );
//             }
//           },
//         ),
//       ),
//     );
//   }
//
//   Widget buildAdviceUser({@required UserModel adviceUser}) {
//     return Column(
//       children: <Widget>[
//         ListTile(
//           leading: UserCircleAvatar(
//             imgUrl: adviceUser.imgUrl,
//             active: adviceUser.isOnline,
//           ),
//           title: Text(adviceUser.username),
//           onTap: () => openMessage(adviceUser: adviceUser),
//           trailing: Icon(
//             Icons.chevron_right,
//             color: Colors.grey,
//           ),
//         ),
//         Divider()
//       ],
//     );
//   }
//
//   void openMessage({@required UserModel adviceUser}) async {
//     try {
//       if (adviceUser.uid == currentUser.uid) {
//         locator<ModalService>().showAlert(
//             context: context,
//             title: 'Error',
//             message: 'Cannot message yourself...');
//         return;
//       }
//
//       // final ChatUser sendee = ChatUser(
//       //     name: adviceUser.username,
//       //     uid: adviceUser.uid,
//       //     avatar: adviceUser.imgUrl,
//       //     fcmToken: adviceUser.fcmToken);
//
//       // final ChatUser sender = ChatUser(
//       //     name: currentUser.username,
//       //     uid: currentUser.uid,
//       //     avatar: currentUser.imgUrl,
//       //     fcmToken: currentUser.fcmToken);
//
//       Query query = FirebaseFirestore.instance.collection('Conversations');
//
//       //Find conversation by filtering on both user IDs being true.
//       query = query.where(currentUser.uid, isEqualTo: true);
//       query = query.where(adviceUser.uid, isEqualTo: true);
//
//       //Extract document snapshots from query object.
//       List<DocumentSnapshot> docs = (await query.get()).docs;
//
//       DocumentReference convoDocRef;
//       //Creat a new conversation document if one does not already exist.
//       if (docs.isEmpty) {
//         convoDocRef =
//             FirebaseFirestore.instance.collection('Conversations').doc();
//         convoDocRef.set({
//           'id': convoDocRef.id,
//           // sender.uid: true,
//           // sendee.uid: true,
//           // 'users': [sender.uid, sendee.uid],
//           'time': DateTime.now(),
//           'lastMessage': ''
//         });
//       } else {
//         convoDocRef = docs.first.reference;
//       }
//
//       //Proceed to message thread with the user who you just recieved a message from.
//       // Route route = MaterialPageRoute(
//       //   builder: (BuildContext context) => BlocProvider(
//       //     create: (BuildContext context) => MESSAGE_BP.MessageBloc(
//       //       sendee: sendee,
//       //       sender: sender,
//       //       convoDocRef: convoDocRef,
//       //     )..add(
//       //         MESSAGE_BP.LoadPageEvent(),
//       //       ),
//       //     child: MESSAGE_BP.MessagePage(),
//       //   ),
//       // );
//
//       // Navigator.push(
//       //   context,
//       //   route,
//       // );
//     } catch (e) {
//       locator<ModalService>().showAlert(
//         context: context,
//         title: 'Error',
//         message: e.toString(),
//       );
//     }
//   }
// }
