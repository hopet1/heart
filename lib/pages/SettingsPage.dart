// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/blocs/changeUsername/Bloc.dart' as CHANGE_USERNAME_BP;
// import 'package:heart/blocs/changePassword/Bloc.dart' as CHANGE_PASSWORD_BP;
// import 'package:heart/blocs/subscription/Bloc.dart' as SUBSCRIPTION_BP;
// import 'package:heart/pages/PrivacyPolicyPage.dart';
// import 'package:heart/pages/admin/AdminActiveUsersPage.dart';
// import 'package:heart/pages/admin/AdminCarouselSliderDurationPage.dart';
// import 'package:heart/pages/admin/AdminQuotesList.dart';
// import 'package:heart/pages/admin/AdminTemplatesPage.dart';
// import 'package:heart/pages/admin/AdminWordsList.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/InAppReviewService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
// import 'package:settings_ui/settings_ui.dart';
// import 'package:url_launcher/url_launcher.dart';
//
// class SettingsPage extends StatelessWidget {
//   SettingsPage({Key key, @required this.isAdmin}) : super(key: key);
//
//   final bool isAdmin;
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//
//   final Color _iconChevronColor = Colors.grey;
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       key: _scaffoldKey,
//       backgroundColor: Colors.grey.shade200,
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Settings',
//           style: TextStyle(fontWeight: FontWeight.bold),
//         ),
//       ),
//       body: SettingsList(
//         sections: [
//           SettingsSection(
//             title: 'Personal',
//             tiles: [
//               SettingsTile(
//                 title: 'Change Username',
//                 leading: Icon(Icons.person),
//                 onPressed: (BuildContext context) {
//                   Route route = MaterialPageRoute(
//                     builder: (BuildContext context) => BlocProvider(
//                       create: (BuildContext context) =>
//                           CHANGE_USERNAME_BP.ChangeUsernameBloc()
//                             ..add(
//                               CHANGE_USERNAME_BP.LoadPageEvent(),
//                             ),
//                       child: CHANGE_USERNAME_BP.ChangeUsernamePage(),
//                     ),
//                   );
//
//                   Navigator.push(
//                     context,
//                     route,
//                   );
//                 },
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//               ),
//               SettingsTile(
//                 title: 'Change Password',
//                 leading: Icon(Icons.lock),
//                 onPressed: (BuildContext context) {
//                   Route route = MaterialPageRoute(
//                     builder: (BuildContext context) => BlocProvider(
//                       create: (BuildContext context) =>
//                           CHANGE_PASSWORD_BP.ChangePasswordBloc()
//                             ..add(
//                               CHANGE_PASSWORD_BP.LoadPageEvent(),
//                             ),
//                       child: CHANGE_PASSWORD_BP.ChangePasswordPage(),
//                     ),
//                   );
//
//                   Navigator.push(
//                     context,
//                     route,
//                   );
//                 },
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//               ),
//             ],
//           ),
//           SettingsSection(
//             title: 'Payments',
//             tiles: [
//               SettingsTile(
//                 title: 'Subscription',
//                 leading: Icon(Icons.book),
//                 onPressed: (BuildContext context) {
//                   // Route route = MaterialPageRoute(
//                   //   builder: (BuildContext context) => BlocProvider(
//                   //     create: (BuildContext context) =>
//                   //         SUBSCRIPTION_BP.SubscriptionBloc()
//                   //           ..add(SUBSCRIPTION_BP.LoadPageEvent()),
//                   //     child: SUBSCRIPTION_BP.SubscriptionPage(),
//                   //   ),
//                   // );
//
//                   // Navigator.push(context, route);
//                 },
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//               )
//             ],
//           ),
//           SettingsSection(
//             title: 'Other',
//             tiles: [
//               SettingsTile(
//                 leading: Icon(MdiIcons.fileAccount),
//                 title: 'Terms of Use',
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//                 onPressed: (BuildContext context) async {
//                   const url =
//                       'https://www.apple.com/legal/internet-services/itunes/dev/stdeula/';
//                   if (await canLaunch(url)) {
//                     await launch(url);
//                   } else {
//                     locator<ModalService>().showAlert(
//                       context: context,
//                       title: 'Error',
//                       message: 'Terms of User cannot be open.',
//                     );
//                   }
//                 },
//               ),
//               SettingsTile(
//                 leading: Icon(MdiIcons.fileDocument),
//                 title: 'Privacy Policy',
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//                 onPressed: (BuildContext context) {
//                   Navigator.push(
//                     context,
//                     MaterialPageRoute(
//                       builder: (context) => PrivacyPolicyPage(),
//                     ),
//                   );
//                 },
//               ),
//               SettingsTile(
//                 leading: Icon(Icons.rate_review),
//                 title: 'Leave A Review',
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//                 onPressed: (BuildContext context) {
//                   locator<InAppReviewService>().openStoreListing();
//                 },
//               ),
//               SettingsTile(
//                 title: 'Delete Account',
//                 leading: Icon(Icons.delete),
//                 onPressed: (BuildContext context) {
//                   locator<ModalService>().showAlert(
//                       context: context,
//                       title: 'Contact Admin',
//                       message:
//                           'This requires extra steps from the admin team.');
//                 },
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//               ),
//             ],
//           ),
//           isAdmin
//               ? SettingsSection(
//                   title: 'Admin',
//                   tiles: [
//                     SettingsTile(
//                       title: 'Quote of The Day',
//                       leading: Icon(MdiIcons.accountTieVoice),
//                       trailing: Icon(Icons.chevron_right),
//                       onPressed: (BuildContext context) {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => AdminQuotesListPage(),
//                           ),
//                         );
//                       },
//                     ),
//                     SettingsTile(
//                       leading: Icon(
//                         MdiIcons.accountVoice,
//                       ),
//                       title: 'Word of The Day',
//                       trailing: Icon(Icons.chevron_right),
//                       onPressed: (BuildContext context) {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => AdminWordsListPage(),
//                           ),
//                         );
//                       },
//                     ),
//                     SettingsTile(
//                       leading: Icon(
//                         MdiIcons.thumbUp,
//                       ),
//                       title: 'Templates',
//                       trailing: Icon(Icons.chevron_right),
//                       onPressed: (BuildContext context) {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => AdminTemplatesPage(),
//                           ),
//                         );
//                       },
//                     ),
//                     SettingsTile(
//                       leading: Icon(
//                         MdiIcons.face,
//                       ),
//                       title: 'Active Users',
//                       trailing: Icon(Icons.chevron_right),
//                       onPressed: (BuildContext context) {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) => ActiveUsersPage(),
//                           ),
//                         );
//                       },
//                     ),
//                     SettingsTile(
//                       leading: Icon(
//                         MdiIcons.timer,
//                       ),
//                       title: 'Carousel Slider Duration',
//                       trailing: Icon(Icons.chevron_right),
//                       onPressed: (BuildContext context) {
//                         Navigator.push(
//                           context,
//                           MaterialPageRoute(
//                             builder: (context) =>
//                                 AdminCarouselSliderDurationPage(),
//                           ),
//                         );
//                       },
//                     )
//                   ],
//                 )
//               : SettingsSection(
//                   tiles: [],
//                 ),
//           SettingsSection(
//             tiles: [
//               SettingsTile(
//                 title: 'Logout',
//                 trailing: Icon(
//                   Icons.chevron_right,
//                   color: _iconChevronColor,
//                 ),
//                 onPressed: (BuildContext context) async {
//                   bool confirm = await locator<ModalService>().showConfirmation(
//                       context: context,
//                       title: 'Logout',
//                       message: 'Are you sure?');
//                   if (!confirm) return;
//
//                   locator<AuthService>().signOut();
//                   Navigator.of(context).popUntil((route) => route.isFirst);
//                 },
//               )
//             ],
//           )
//         ],
//       ),
//     );
//   }
// }
