// import 'package:flutter/material.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/data.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
//
// class ForgotPasswordPage extends StatefulWidget {
//   @override
//   State createState() => ForgotPasswordPageState();
// }
//
// class ForgotPasswordPageState extends State<ForgotPasswordPage>
//     with SingleTickerProviderStateMixin {
//   final TextEditingController emailController = TextEditingController();
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   final GlobalKey<FormState> formKey = GlobalKey<FormState>();
//   bool autoValidate = false;
//   bool isLoading = false;
//   final Padding verticalPadding = Padding(
//     padding: EdgeInsets.symmetric(vertical: 15),
//   );
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   submit() async {
//     if (formKey.currentState.validate()) {
//       formKey.currentState.save();
//
//       final bool confirm = await locator<ModalService>().showConfirmation(
//           context: context,
//           title: 'Send Password Reset Link?',
//           message: 'The link will go to the email you provided.');
//
//       if (!confirm) return;
//
//       try {
//         setState(
//           () {
//             isLoading = true;
//           },
//         );
//         await locator<AuthService>().resetPassword(email: emailController.text);
//         setState(
//           () {
//             isLoading = false;
//           },
//         );
//
//         locator<ModalService>().showAlert(
//           context: context,
//           title: 'Password Reset Link Sent',
//           message: 'Check your email to follow steps.',
//         );
//       } catch (e) {
//         setState(
//           () {
//             isLoading = false;
//             locator<ModalService>().showAlert(
//               context: context,
//               title: 'Error',
//               message: e.message,
//             );
//           },
//         );
//       }
//     } else {
//       autoValidate = true;
//     }
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Forgot Password',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       body: Padding(
//         padding: SCAFFOLD_PADDING,
//         child: Form(
//           key: formKey,
//           autovalidateMode: AutovalidateMode.onUserInteraction,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               verticalPadding,
//               Text(
//                 'Don\'t worry! It happens.',
//                 style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
//               ),
//               verticalPadding,
//               Text(
//                 'Please enter your email address that is assosiated with us. A password reset link will be sent to this email.',
//                 style: TextStyle(fontSize: 18),
//               ),
//               Container(
//                 margin: EdgeInsets.only(top: 60),
//                 child: TextFormField(
//                   controller: emailController,
//                   keyboardType: TextInputType.emailAddress,
//                   textInputAction: TextInputAction.done,
//                   onFieldSubmitted: (term) {},
//                   validator: locator<ValidatorService>().email,
//                   onSaved: (value) {},
//                   decoration: InputDecoration(
//                     focusedBorder: UnderlineInputBorder(
//                       borderSide: BorderSide(color: Colors.red),
//                     ),
//                     hintText: 'Email',
//                     // icon: Icon(Icons.email),
//                     fillColor: Colors.white,
//                   ),
//                 ),
//               ),
//               Spacer(),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                 children: <Widget>[
//                   RaisedButton(
//                     color: Colors.red,
//                     textColor: Colors.white,
//                     child: Text('Send Reset Link'),
//                     onPressed: () {
//                       submit();
//                     },
//                   )
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
