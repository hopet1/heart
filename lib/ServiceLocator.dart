// import 'package:get_it/get_it.dart';
// // import 'package:heart/services/AnalyticsService.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/ConversationsDBService.dart';
// import 'package:heart/services/FCMNotificationService.dart';
// import 'package:heart/services/FormatterService.dart';
// import 'package:heart/services/InAppReviewService.dart';
// import 'package:heart/services/LikesDBService.dart';
// import 'package:heart/services/MetaDataDBService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/PerformanceService.dart';
// import 'package:heart/services/QuoteOfTheDayDBService.dart';
// // import 'package:heart/services/RevenueCatService.dart';
// import 'package:heart/services/StorageService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import 'package:heart/services/WordOfTheDayDBService.dart';
//
// GetIt locator = GetIt.I;
//
// void setUpLocater() {
//   // locator.registerLazySingleton(() => AnalyticsService());
//   locator.registerLazySingleton(() => AuthService());
//   locator.registerLazySingleton(() => ConversationsDBService());
//   locator.registerLazySingleton(() => FCMNotificationService());
//   locator.registerLazySingleton(() => FormatterService());
//   locator.registerLazySingleton(() => InAppReviewService());
//   locator.registerLazySingleton(() => LikesDBService());
//   locator.registerLazySingleton(() => MetaDataDBService());
//   locator.registerLazySingleton(() => ModalService());
//   locator.registerLazySingleton(() => PerformanceService());
//   locator.registerLazySingleton(() => QuoteOfTheDayDBService());
//   // locator.registerLazySingleton(() => RevenueCatService());
//   locator.registerLazySingleton(() => StorageService());
//   locator.registerLazySingleton(() => TemplatesService());
//   locator.registerLazySingleton(() => UsersDBService());
//   locator.registerLazySingleton(() => ValidatorService());
//   locator.registerLazySingleton(() => WordOfTheDayDBService());
// }
