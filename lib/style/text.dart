import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle serviceButtonStyle =
    TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold);

TextStyle headingOneTextStyle = TextStyle(
  fontSize: 20,
  color: Colors.black,
  fontWeight: FontWeight.bold,
);
