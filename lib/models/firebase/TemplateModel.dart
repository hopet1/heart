// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
//
// class TemplateModel {
//   final String category;
//   final int copies;
//   int count;
//   final String id;
//   String message;
//   bool safe;
//   DateTime time;
//   final String userID;
//
//   TemplateModel(
//       {@required this.category,
//       @required this.copies,
//       @required this.count,
//       @required this.id,
//       @required this.message,
//       @required this.safe,
//       @required this.time,
//       @required this.userID});
//
//   // static TemplateModel extractDocument(DocumentSnapshot ds) {
//   //   return TemplateModel(
//   //     category: ds.data()['category'],
//   //     copies: ds.data()['copies'],
//   //     count: ds.data()['count'],
//   //     id: ds.data()['id'],
//   //     message: ds.data()['message'],
//   //     safe: ds.data()['safe'],
//   //     time: ds.data()['time'].toDate(),
//   //     userID: ds.data()['userID'],
//   //   );
//   // }
//
//   Map<String, dynamic> toMap() {
//     return {
//       'category': category,
//       'copies': copies,
//       'count': count,
//       'id': id,
//       'message': message,
//       'safe': safe,
//       'time': time,
//       'userID': userID,
//     };
//   }
// }
