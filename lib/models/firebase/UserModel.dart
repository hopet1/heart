// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
//
// class UserModel {
//   String email;
//   String imgUrl;
//   bool isAdmin;
//   bool isOnline;
//   String fcmToken;
//   DateTime time;
//   String uid;
//   String username;
//   bool canGiveAdvice;
//   String activeSubscription;
//   int freeMessageCount;
//
//   UserModel({
//     @required this.email,
//     @required this.imgUrl,
//     @required this.isAdmin,
//     @required this.isOnline,
//     @required this.fcmToken,
//     @required this.time,
//     @required this.uid,
//     @required this.username,
//     @required this.canGiveAdvice,
//     @required this.activeSubscription,
//     @required this.freeMessageCount,
//   });
//
//   // factory UserModel.fromDocumentSnapshot({@required DocumentSnapshot ds}) {
//   //   return UserModel(
//   //     email: ds.data()['email'],
//   //     imgUrl: ds.data()['imgUrl'],
//   //     fcmToken: ds.data()['fcmToken'],
//   //     isAdmin: ds.data()['isAdmin'],
//   //     isOnline: ds.data()['isOnline'],
//   //     time: ds.data()['time'].toDate(),
//   //     uid: ds.data()['uid'],
//   //     username: ds.data()['username'],
//   //     canGiveAdvice: ds.data()['canGiveAdvice'],
//   //     activeSubscription: ds.data()['activeSubscription'],
//   //     freeMessageCount: ds.data()['freeMessageCount'],
//   //   );
//   // }
//
//   Map<String, dynamic> toMap() {
//     return {
//       'email': email,
//       'imgUrl': imgUrl,
//       'fcmToken': fcmToken,
//       'isAdmin': isAdmin,
//       'isOnline': isOnline,
//       'time': time,
//       'uid': uid,
//       'username': username,
//       'canGiveAdvice': canGiveAdvice,
//       'activeSubscription': activeSubscription,
//       'freeMessageCount': freeMessageCount
//     };
//   }
// }
