// import 'package:firebase_performance/firebase_performance.dart';
// import 'package:flutter/material.dart';
//
// abstract class IPerformanceService {
//   void startTrace({@required String traceName});
//   void stopTrace();
// }
//
// class PerformanceService extends IPerformanceService {
//   Trace trace;
//
//   @override
//   void startTrace({@required String traceName}) async {
//     trace = FirebasePerformance.instance.newTrace(traceName);
//     trace.start();
//   }
//
//   @override
//   void stopTrace() {
//     trace.stop();
//   }
// }
