// import 'package:in_app_review/in_app_review.dart';
// import 'dart:io';
//
// //https://pub.dev/packages/in_app_review
//
// abstract class IInAppReviewService {
//   void requestReview();
//   void openStoreListing();
// }
//
// class InAppReviewService {
//   final InAppReview _inAppReview = InAppReview.instance;
//   final String _appStoreId = '1481385455';
//
//   void requestReview() async {
//     bool isAvailable = await _inAppReview.isAvailable();
//
//     if (isAvailable) {
//       _inAppReview.requestReview();
//     } else {
//       openStoreListing();
//     }
//   }
//
//   void openStoreListing() {
//     if (Platform.isIOS) {
//       _inAppReview.openStoreListing(appStoreId: _appStoreId);
//     } else {
//       _inAppReview.openStoreListing();
//     }
//   }
// }
