// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/LikeModel.dart';
//
// abstract class ILikesDBService {
//   Future<void> createLike(
//       {@required String templateID, @required LikeModel like});
//   Future<void> deleteLike(
//       {@required String templateID, @required String userID});
//   // Future<List<LikeModel>> retrieveLikes({@required String templateID});
//   Stream<QuerySnapshot> streamLikes({@required String templateID});
// }
//
// class LikesDBService extends ILikesDBService {
//   final CollectionReference conversationsDB =
//       FirebaseFirestore.instance.collection('Conversations');
//   final CollectionReference templatesDB =
//       FirebaseFirestore.instance.collection('Templates');
//
//   @override
//   Future<void> createLike(
//       {@required String templateID, @required LikeModel like}) async {
//     try {
//       //Create new batch object.
//       final WriteBatch batch = FirebaseFirestore.instance.batch();
//       //Create document reference for the template of this like.
//       final DocumentReference templateDocRef = templatesDB.doc(templateID);
//       //Create document reference for new like.
//       final DocumentReference likeDocRef =
//           templateDocRef.collection('Likes').doc();
//       //Set data for new reference.
//       like.id = likeDocRef.id;
//       batch.set(likeDocRef, like.toMap());
//       //Increase count value for total likes on this template.
//       batch.update(templateDocRef, {'count': FieldValue.increment(1)});
//       //Commit batch.
//       batch.commit();
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> deleteLike(
//       {@required String templateID, @required String userID}) async {
//     try {
//       //Create new batch object.
//       final WriteBatch batch = FirebaseFirestore.instance.batch();
//       //Create document reference for the template of this like.
//       final DocumentReference templateDocRef = templatesDB.doc(templateID);
//       //Create document reference for new like.
//       final DocumentReference likeDocRef = (await templateDocRef
//               .collection('Likes')
//               .where('userID', isEqualTo: userID)
//               .get())
//           .docs
//           .first
//           .reference;
//
//       //Delete like document.
//       batch.delete(likeDocRef);
//       //Decrease count value for total likes on this template.
//       batch.update(templateDocRef, {'count': FieldValue.increment(-1)});
//       //Commit batch.
//       batch.commit();
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   // @override
//   // Future<List<LikeModel>> retrieveLikes({String templateID}) async {
//   //   try {
//   //     CollectionReference likesColRef =
//   //         templatesDB.document(templateID).collection('Likes');
//   //     QuerySnapshot querySnapshot = await likesColRef.getDocuments();
//   //     List<DocumentSnapshot> docs = querySnapshot.documents;
//   //     List<LikeModel> likes = List<LikeModel>();
//   //     for (int i = 0; i < docs.length; i++) {
//   //       likes.add(
//   //         LikeModel.extractDocument(docs[i]),
//   //       );
//   //     }
//   //     return likes;
//   //   } catch (e) {
//   //     throw Exception(
//   //       e.toString(),
//   //     );
//   //   }
//   // }
//
//   @override
//   Stream<QuerySnapshot> streamLikes({@required String templateID}) {
//     Query query = templatesDB.doc(templateID).collection('Likes');
//     return query.snapshots();
//   }
// }
