// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
//
// abstract class IMetaDataDBService {
//   //Meta Data
//   Future<int> getCarouselSliderDuration();
//   Future<void> setCarouselSliderDuration(
//       {@required int carouselSliderDuration});
//
//   //Miscellanious
//   Future<void> addPropertyToDocuments(
//       {@required String collection,
//       @required String property,
//       @required dynamic value});
// }
//
// class MetaDataDBService extends IMetaDataDBService {
//   final CollectionReference templatesDB =
//       FirebaseFirestore.instance.collection('Templates');
//   final CollectionReference metaDataDB =
//       FirebaseFirestore.instance.collection('Meta Data');
//
//   @override
//   Future<void> addPropertyToDocuments(
//       {String collection, String property, dynamic value}) async {
//     try {
//       final CollectionReference colRef =
//           FirebaseFirestore.instance.collection(collection);
//       QuerySnapshot querySnapshot = await colRef.get();
//       List<DocumentSnapshot> docs = querySnapshot.docs;
//       for (int i = 0; i < docs.length; i++) {
//         await colRef.doc(docs[i].id).update(
//           {property: value},
//         );
//       }
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<int> getCarouselSliderDuration() async {
//     try {
//       DocumentSnapshot doc = (await metaDataDB.doc('Basic').get());
//       // return doc.data()['carouselSliderDuration'];
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> setCarouselSliderDuration({int carouselSliderDuration}) async {
//     try {
//       metaDataDB
//           .doc('Basic')
//           .update({'carouselSliderDuration': carouselSliderDuration});
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
// }
