// import 'package:flutter/material.dart';
// import 'package:heart/data.dart';
// import 'package:purchases_flutter/purchases_flutter.dart';

// enum SubscriptionStatus {
//   PREMIUM_PLAN_SUBSCRIBED,
//   NO_PLAN_SUBSCRIBED,
// }

// abstract class IRevenueCatService {
//   Future<SubscriptionStatus> getActiveSubscriptionStatus();
//   Future<List<Product>> getProducts();
//   void reset();
//   Future<PurchaserInfo> purchase({@required String sku});
// }

// class RevenueCatService extends IRevenueCatService {
//   @override
//   Future<SubscriptionStatus> getActiveSubscriptionStatus() async {
//     try {
//       final PurchaserInfo purchaserInfo = await Purchases.getPurchaserInfo();
//       final List<String> activeSubscriptions =
//           purchaserInfo.activeSubscriptions;

//       if (activeSubscriptions.contains(revenueCatPremiumPlan)) {
//         return SubscriptionStatus.PREMIUM_PLAN_SUBSCRIBED;
//       }

//       return SubscriptionStatus.NO_PLAN_SUBSCRIBED;
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }

//   @override
//   Future<List<Product>> getProducts() async {
//     try {
//       final List<Product> products = await Purchases.getProducts(
//         [
//           revenueCatPremiumPlan,
//         ],
//       );
//       return products;
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }

//   @override
//   void reset() async {
//     try {
//       await Purchases.reset();
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }

//   @override
//   Future<PurchaserInfo> purchase({@required String sku}) async {
//     try {
//       PurchaserInfo purchaseInfo = await Purchases.purchaseProduct(sku);
//       return purchaseInfo;
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }
// }
