// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
//
// abstract class ITemplatesService {
//   Future<List<TemplateModel>> retrieveTemplates(
//       {String category,
//       bool safe,
//       int limit,
//       DocumentSnapshot startAfterDocument,
//       String orderBy});
//   Future<void> createTemplate({@required TemplateModel template});
//
//   Future<List<DocumentSnapshot>> retrieveTemplatesPaginated(
//       {String category,
//       bool safe,
//       int limit,
//       DocumentSnapshot startAfterDocument,
//       String orderBy});
//   Future<DocumentSnapshot> retrieveNextTemplate(
//       {@required DocumentSnapshot templateSnapshot,
//       @required String category,
//       @required bool forward});
//   Future<void> updateTemplate(
//       {@required String templateID, @required Map<String, dynamic> data});
//   Future<void> deleteTemplate({@required String templateID});
//   Stream<DocumentSnapshot> streamTemplate({@required String templateID});
// }
//
// class TemplatesService extends ITemplatesService {
//   final CollectionReference templatesDB =
//       FirebaseFirestore.instance.collection('Templates');
//
//   @override
//   Future<List<DocumentSnapshot>> retrieveTemplatesPaginated(
//       {String category,
//       bool safe,
//       int limit,
//       DocumentSnapshot startAfterDocument,
//       String orderBy}) async {
//     try {
//       Query query = templatesDB;
//
//       if (category != null) {
//         query = query.where('category', isEqualTo: category);
//       }
//
//       if (safe != null) {
//         query = query.where('safe', isEqualTo: safe);
//       }
//
//       if (limit != null) {
//         query = query.limit(limit);
//       }
//
//       if (orderBy != null) {
//         query = query.orderBy(orderBy);
//       }
//
//       if (startAfterDocument != null) {
//         query = query.startAfterDocument(startAfterDocument);
//       }
//
//       QuerySnapshot querySnapshot = await query.get();
//
//       List<DocumentSnapshot> documentSnapshots = querySnapshot.docs;
//
//       return documentSnapshots;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<List<TemplateModel>> retrieveTemplates(
//       {String category,
//       bool safe,
//       int limit,
//       DocumentSnapshot startAfterDocument,
//       String orderBy}) async {
//     try {
//       Query query = templatesDB;
//
//       if (category != null) {
//         query = query.where('category', isEqualTo: category);
//       }
//
//       if (safe != null) {
//         query = query.where('safe', isEqualTo: safe);
//       }
//
//       if (limit != null) {
//         query = query.limit(limit);
//       }
//
//       if (orderBy != null) {
//         query = query.orderBy(orderBy);
//       }
//
//       if (startAfterDocument != null) {
//         query = query.startAfterDocument(startAfterDocument);
//       }
//
//       List<DocumentSnapshot> docs = (await query.get()).docs;
//
//       // List<TemplateModel> templates = docs
//       //     .map(
//       //       (doc) => TemplateModel.extractDocument(
//       //         doc,
//       //       ),
//       //     )
//       //     .toList();
//
//       // //Order templates by most recent, (firebase requires an index to add this extra param).
//       // templates.sort(
//       //   (a, b) => b.time.compareTo(a.time),
//       // );
//
//       return [];
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> updateTemplate(
//       {String templateID, Map<String, dynamic> data}) async {
//     try {
//       await templatesDB.doc(templateID).update(data);
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> createTemplate({TemplateModel template}) async {
//     try {
//       DocumentReference docRef = await templatesDB.add(
//         template.toMap(),
//       );
//       await templatesDB.doc(docRef.id).update(
//         {'id': docRef.id},
//       );
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> deleteTemplate({String templateID}) async {
//     try {
//       await templatesDB.doc(templateID).delete();
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Stream<DocumentSnapshot> streamTemplate({@required String templateID}) {
//     Stream<DocumentSnapshot> stream = templatesDB.doc(templateID).snapshots();
//     return stream;
//   }
//
//   @override
//   Future<DocumentSnapshot> retrieveNextTemplate(
//       {@required DocumentSnapshot templateSnapshot,
//       @required String category,
//       @required bool forward}) async {
//     try {
//       Query query = templatesDB;
//
//       query = query.startAfterDocument(templateSnapshot);
//
//       query = query.limit(1);
//
//       query = query.orderBy('time', descending: forward);
//
//       query = query.where('safe', isEqualTo: true);
//
//       query = query.where('category', isEqualTo: category);
//
//       QuerySnapshot querySnapshot = await query.get();
//
//       if (querySnapshot.docs.isEmpty) return null;
//
//       DocumentSnapshot documentSnapshot = querySnapshot.docs.first;
//
//       return documentSnapshot;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
// }
