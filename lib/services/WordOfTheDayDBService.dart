// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
//
// abstract class IWordOfTheDayDBService {
//   //Word of The Day
//   Future<TextOfTheDayModel> retrieveWordOfTheDay();
//   Future<List<TextOfTheDayModel>> retrieveWordsOfTheDay();
//   Stream<QuerySnapshot> streamWordsOfTheDay();
//   Future<void> updateActiveWOTD({@required String wotdID});
//   Future<void> createWOTD({@required String text});
//   Future<void> deleteWOTD({@required String wotdID});
// }
//
// class WordOfTheDayDBService extends IWordOfTheDayDBService {
//   final CollectionReference conversationsDB =
//       FirebaseFirestore.instance.collection('Conversations');
//   final CollectionReference metaDataDB =
//       FirebaseFirestore.instance.collection('Meta Data');
//   @override
//   Future<TextOfTheDayModel> retrieveWordOfTheDay() async {
//     try {
//       DocumentSnapshot doc = (await metaDataDB
//               .doc('Basic')
//               .collection('wordsOfTheDay')
//               .where('isActive', isEqualTo: true)
//               .get())
//           .docs
//           .first;
//       // return TextOfTheDayModel.extractDocument(doc);
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Stream<QuerySnapshot> streamWordsOfTheDay() {
//     Query query = metaDataDB.doc('Basic').collection('wordsOfTheDay');
//
//     return query.snapshots();
//   }
//
//   @override
//   Future<void> createWOTD({String text}) async {
//     try {
//       //Create word of the day object.
//       TextOfTheDayModel wotd =
//           TextOfTheDayModel(id: '', isActive: false, text: text);
//       //Save word of the day to database.
//       DocumentReference docRef = await metaDataDB
//           .doc('Basic')
//           .collection('wordsOfTheDay')
//           .add(wotd.toMap());
//       //Update id.
//       await metaDataDB
//           .doc('Basic')
//           .collection('wordsOfTheDay')
//           .doc(docRef.id)
//           .update(
//         {'id': docRef.id},
//       );
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> deleteWOTD({String wotdID}) async {
//     try {
//       await metaDataDB
//           .doc('Basic')
//           .collection('wordsOfTheDay')
//           .doc(wotdID)
//           .delete();
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<List<TextOfTheDayModel>> retrieveWordsOfTheDay() async {
//     try {
//       List<TextOfTheDayModel> quotes = [];
//       List<DocumentSnapshot> docs =
//           (await metaDataDB.doc('Basic').collection('wordsOfTheDay').get())
//               .docs;
//       // for (var i = 0; i < docs.length; i++) {
//       //   quotes.add(
//       //     TextOfTheDayModel.extractDocument(docs[i]),
//       //   );
//       // }
//       return quotes;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> updateActiveWOTD({String wotdID}) async {
//     try {
//       //Set old word of the day to inactive.
//       TextOfTheDayModel word = await retrieveWordOfTheDay();
//       await metaDataDB
//           .doc('Basic')
//           .collection('wordsOfTheDay')
//           .doc(word.id)
//           .update({'isActive': false});
//       //Set new word of the day to active.
//       await metaDataDB
//           .doc('Basic')
//           .collection('wordsOfTheDay')
//           .doc(wotdID)
//           .update({'isActive': true});
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
// }
