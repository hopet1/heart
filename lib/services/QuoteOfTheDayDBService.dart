// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
//
// abstract class IQuoteOfTheDayDBService {
//   //Quote of The Day
//   Future<TextOfTheDayModel> retrieveQuoteOfTheDay();
//   Future<List<TextOfTheDayModel>> retrieveQuotesOfTheDay();
//   Stream<QuerySnapshot> streamQuotesOfTheDay();
//   Future<void> updateActiveQOTD({@required String qotdID});
//   Future<void> createQOTD({@required String text});
//   Future<void> deleteQOTD({@required String qotdID});
// }
//
// class QuoteOfTheDayDBService extends IQuoteOfTheDayDBService {
//   final CollectionReference _metaDataColRef =
//       FirebaseFirestore.instance.collection('Meta Data');
//   @override
//   Future<TextOfTheDayModel> retrieveQuoteOfTheDay() async {
//     try {
//       DocumentSnapshot doc = (await _metaDataColRef
//               .doc('Basic')
//               .collection('quotesOfTheDay')
//               .where('isActive', isEqualTo: true)
//               .get())
//           .docs
//           .first;
//       // return TextOfTheDayModel.extractDocument(doc);
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Stream<QuerySnapshot> streamQuotesOfTheDay() {
//     Query query = _metaDataColRef.doc('Basic').collection('quotesOfTheDay');
//     return query.snapshots();
//   }
//
//   @override
//   Future<List<TextOfTheDayModel>> retrieveQuotesOfTheDay() async {
//     try {
//       List<TextOfTheDayModel> quotes = [];
//       List<DocumentSnapshot> docs = (await _metaDataColRef
//               .doc('Basic')
//               .collection('quotesOfTheDay')
//               .get())
//           .docs;
//       for (var i = 0; i < docs.length; i++) {
//         // quotes.add(
//         //   TextOfTheDayModel.extractDocument(docs[i]),
//         // );
//       }
//       return quotes;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> updateActiveQOTD({String qotdID}) async {
//     try {
//       //Set old quote of the day to inactive.
//       TextOfTheDayModel quote = await retrieveQuoteOfTheDay();
//       await _metaDataColRef
//           .doc('Basic')
//           .collection('quotesOfTheDay')
//           .doc(quote.id)
//           .update({'isActive': false});
//       //Set new quote of the day to active.
//       await _metaDataColRef
//           .doc('Basic')
//           .collection('quotesOfTheDay')
//           .doc(qotdID)
//           .update({'isActive': true});
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> createQOTD({String text}) async {
//     try {
//       //Create quote of the day object.
//       TextOfTheDayModel qotd =
//           TextOfTheDayModel(id: '', isActive: false, text: text);
//       //Save quote of the day to database.
//       DocumentReference docRef = await _metaDataColRef
//           .doc('Basic')
//           .collection('quotesOfTheDay')
//           .add(qotd.toMap());
//       //Update id.
//       await _metaDataColRef
//           .doc('Basic')
//           .collection('quotesOfTheDay')
//           .doc(docRef.id)
//           .update(
//         {'id': docRef.id},
//       );
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<void> deleteQOTD({String qotdID}) async {
//     try {
//       await _metaDataColRef
//           .doc('Basic')
//           .collection('quotesOfTheDay')
//           .doc(qotdID)
//           .delete();
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
// }
