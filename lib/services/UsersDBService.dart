// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/UserModel.dart';
//
// abstract class IUsersDBService {
//   Future<void> createUser({@required UserModel user});
//   Future<UserModel> retrieveUser({@required String uid});
//   Future<List<UserModel>> retrieveUsers(
//       {bool isAdmin, int limit, String orderBy});
//   Stream<QuerySnapshot> streamUsers({
//     @required bool canGiveAdvice,
//     @required bool isAdmin,
//   });
//   Future<void> updateUser(
//       {@required String uid, @required Map<String, dynamic> data});
//   Future<List<DocumentSnapshot>> retrieveUsersPagination({
//     int limit,
//     String orderBy,
//     DocumentSnapshot startAfterDocument,
//   });
// }
//
// class UsersDBService extends IUsersDBService {
//   final CollectionReference usersDB =
//       FirebaseFirestore.instance.collection('Users');
//
//   @override
//   Future<void> createUser({@required UserModel user}) async {
//     try {
//       DocumentReference docRef = usersDB.doc();
//       docRef.set(user.toMap());
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<UserModel> retrieveUser({@required String uid}) async {
//     try {
//       DocumentSnapshot documentSnapshot =
//           (await usersDB.where('uid', isEqualTo: uid).get()).docs.first;
//       // return UserModel.fromDocumentSnapshot(ds: documentSnapshot);
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }
//
//   @override
//   Stream<QuerySnapshot> streamUsers({
//     @required bool canGiveAdvice,
//     @required bool isAdmin,
//   }) {
//     Query query = usersDB;
//
//     query = query.where('canGiveAdvice', isEqualTo: canGiveAdvice);
//     query = query.where('isAdmin', isEqualTo: isAdmin);
//
//     return query.snapshots();
//   }
//
//   @override
//   Future<void> updateUser(
//       {@required String uid, @required Map<String, dynamic> data}) async {
//     try {
//       DocumentReference documentReference =
//           (await usersDB.where('uid', isEqualTo: uid).get())
//               .docs
//               .first
//               .reference;
//
//       await documentReference.update(data);
//       return;
//     } catch (e) {
//       throw Exception(
//         e.toString(),
//       );
//     }
//   }
//
//   @override
//   Future<List<UserModel>> retrieveUsers(
//       {bool isAdmin, int limit, String orderBy}) async {
//     try {
//       Query query = usersDB;
//
//       if (isAdmin != null) {
//         query = query.where('isAdmin', isEqualTo: isAdmin);
//       }
//
//       if (limit != null) {
//         query = query.limit(limit);
//       }
//
//       if (orderBy != null) {
//         query = query.orderBy(orderBy);
//       }
//
//       List<DocumentSnapshot> docs = (await query.get()).docs;
//
//       List<UserModel> users = [];
//       // for (int i = 0; i < docs.length; i++) {
//       //   users.add(
//       //     UserModel.fromDocumentSnapshot(ds: docs[i]),
//       //   );
//       // }
//
//       return users;
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }
//
//   @override
//   Future<List<DocumentSnapshot>> retrieveUsersPagination({
//     int limit,
//     String orderBy,
//     DocumentSnapshot startAfterDocument,
//   }) async {
//     try {
//       Query query = usersDB;
//
//       if (limit != null) {
//         query = query.limit(limit);
//       }
//
//       if (orderBy != null) {
//         query = query.orderBy(orderBy);
//       }
//
//       if (startAfterDocument != null) {
//         query = query.startAfterDocument(startAfterDocument);
//       }
//
//       List<DocumentSnapshot> userDocs = (await query.get()).docs;
//
//       return userDocs;
//     } catch (e) {
//       throw Exception(e.toString());
//     }
//   }
// }
