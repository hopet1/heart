// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/Extensions/HexColor.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/data.dart';
// import 'package:heart/services/ModalService.dart';
// // import 'package:purchases_flutter/purchases_flutter.dart';
// import 'Bloc.dart';

// class SubscriptionPage extends StatefulWidget {
//   @override
//   State createState() => SubscriptionPageState();
// }

// class SubscriptionPageState extends State<SubscriptionPage>
//     implements SubscriptionBlocDelegate {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   SubscriptionBloc _subscriptionBloc;
//   double screenHeight, screenWidth;

//   final Color colorGold = HexColor('#D4AF37');
//   final Color colorSilver = HexColor('#C0C0C0');

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   void dispose() {
//     super.dispose();
//     _subscriptionBloc.close();
//   }

//   @override
//   void showMessage(String message) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }

//   @override
//   Widget build(BuildContext context) {
//     screenHeight = MediaQuery.of(context).size.height;
//     screenWidth = MediaQuery.of(context).size.width;

//     _subscriptionBloc = BlocProvider.of<SubscriptionBloc>(context);
//     _subscriptionBloc.setDelegate(delegate: this);

//     return Scaffold(
//       key: _scaffoldKey,
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Subscription',
//           style: TextStyle(fontWeight: FontWeight.bold),
//         ),
//         actions: <Widget>[
//           IconButton(
//             icon: Icon(Icons.refresh),
//             onPressed: () {
//               _subscriptionBloc.add(ResetSubscriptionEvent());
//             },
//           )
//         ],
//       ),
//       body: SafeArea(
//         child: Padding(
//           padding: EdgeInsets.all(20),
//           child: BlocBuilder<SubscriptionBloc, SubscriptionState>(
//             builder: (BuildContext context, SubscriptionState state) {
//               if (state is LoadingState) {
//                 return Spinner(text: state.text);
//               }

//               if (state is PremiumPlanSubscribedState) {
//                 return buildSubscribedView(plan: state.premiumPlan);
//               }

//               if (state is NoPlanSubscribedState) {
//                 return buildUnsubscribedView(
//                   premiumPlan: state.premiumPlan,
//                 );
//               }

//               if (state is ErrorState) {
//                 return Center(
//                   child: Text('Error: ${state.error.toString()}'),
//                 );
//               }

//               return Center(
//                 child: Text(
//                   'You should NEVER see this.',
//                 ),
//               );
//             },
//           ),
//         ),
//       ),
//     );
//   }

//   Widget buildUnsubscribedView({@required Product premiumPlan}) {
//     return Column(
//       children: [
//         RichText(
//           text: TextSpan(
//             style: TextStyle(fontSize: 21, fontWeight: FontWeight.bold),
//             children: [
//               TextSpan(
//                 text: '${premiumPlan.title}',
//                 style: TextStyle(color: colorGold),
//               ),
//             ],
//           ),
//         ),
//         Image.asset(
//           'assets/images/black_man_phone.png',
//           height: screenHeight * 0.4,
//         ),
//         Spacer(),
//         Row(
//           children: <Widget>[
//             Expanded(
//               child: Padding(
//                 padding: EdgeInsets.all(10),
//                 child: Column(
//                   children: <Widget>[
//                     RichText(
//                       text: TextSpan(
//                         children: [
//                           TextSpan(
//                             text: '${premiumPlan.priceString}',
//                             style: TextStyle(
//                                 color: Colors.black,
//                                 fontSize: 30,
//                                 fontWeight: FontWeight.bold),
//                           ),
//                           TextSpan(
//                             text: '/mo',
//                             style: TextStyle(
//                                 color: Colors.grey,
//                                 fontWeight: FontWeight.bold),
//                           )
//                         ],
//                       ),
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     Text(
//                       '${premiumPlan.description}',
//                       textAlign: TextAlign.center,
//                       style: TextStyle(
//                           fontWeight: FontWeight.bold, color: Colors.grey),
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     RaisedButton(
//                       color: colorGold,
//                       child: Text('Subscribe Now'),
//                       onPressed: () {
//                         _subscriptionBloc.add(
//                             BuyProductEvent(sku: revenueCatPremiumPlan));
//                       },
//                       textColor: Colors.white,
//                     )
//                   ],
//                 ),
//               ),
//             )
//           ],
//         ),
//       ],
//     );
//   }

//   Widget buildSubscribedView({@required Product plan}) {
//     return Column(
//       children: [
//         Text(
//           'You Are Currently On The ${plan.title}',
//           textAlign: TextAlign.center,
//           style: TextStyle(
//               color: Colors.black, fontSize: 21, fontWeight: FontWeight.bold),
//         ),
//         Image.asset(
//           'assets/images/black_men_office.jpg',
//           height: screenHeight * 0.4,
//         ),
//         RichText(
//           text: TextSpan(
//             children: [
//               TextSpan(
//                 text: plan.priceString,
//                 style: TextStyle(
//                     color: Colors.black,
//                     fontSize: 30,
//                     fontWeight: FontWeight.bold),
//               ),
//               TextSpan(
//                   text: '/mo',
//                   style: TextStyle(
//                       color: Colors.grey, fontWeight: FontWeight.bold))
//             ],
//           ),
//         ),
//         SizedBox(
//           height: 20,
//         ),
//         //buildSubscriptionStatus(status: subscription.status),
//         Spacer(),
//         Text(
//           plan.description,
//           textAlign: TextAlign.center,
//           style: TextStyle(fontWeight: FontWeight.bold),
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Text(
//           'Plan will automatically renew. To cancel subscription, go to your settings.',
//           textAlign: TextAlign.center,
//         )
//       ],
//     );
//   }
// }
