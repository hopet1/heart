// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/data.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/RevenueCatService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'SubscriptionEvent.dart';
// import 'SubscriptionState.dart';
// // import 'package:purchases_flutter/purchases_flutter.dart';

// abstract class SubscriptionBlocDelegate {
//   void showMessage(String message);
// }

// class SubscriptionBloc extends Bloc<SubscriptionEvent, SubscriptionState> {
//   SubscriptionBloc() : super(null);

//   UserModel _currentUser;
//   SubscriptionBlocDelegate _delegate;
//   // Product _premiumPlan;

//   void setDelegate({@required SubscriptionBlocDelegate delegate}) {
//     this._delegate = delegate;
//   }

//   @override
//   Stream<SubscriptionState> mapEventToState(SubscriptionEvent event) async* {
//     if (event is LoadPageEvent) {
//       yield LoadingState(text: 'Loading...');

//       try {
//       //   final List<Product> products =
//       //       await locator<RevenueCatService>().getProducts();

//       //   _premiumPlan = products.firstWhere(
//       //       (product) => product.identifier == revenueCatPremiumPlan);

//       //   _currentUser = await locator<AuthService>().getCurrentUser();

//       //   if (_currentUser.activeSubscription == revenueCatPremiumPlan) {
//       //     yield PremiumPlanSubscribedState(premiumPlan: _premiumPlan);
//       //   } else {
//       //     yield NoPlanSubscribedState(
//       //         premiumPlan: _premiumPlan);
//       //   }
//       } catch (error) {
//         yield ErrorState(error: error);
//       }
//     }

//     if (event is BuyProductEvent) {
//       try {
//         // PurchaserInfo purchaserInfo =
//         //     await locator<RevenueCatService>().purchase(sku: event.sku);
//         // final String activeSubscription =
//         //     purchaserInfo.activeSubscriptions.first;

//         // if (activeSubscription == revenueCatPremiumPlan) {
//         //   locator<UsersDBService>().updateUser(
//         //     uid: _currentUser.uid,
//         //     data: {
//         //       'activeSubscription': revenueCatPremiumPlan,
//         //     },
//         //   );
//         //   yield PremiumPlanSubscribedState(premiumPlan: _premiumPlan);
//         // }
//       } catch (e) {
//         //Hacky way of doing this, but prevent this error message from displaying on cancel.
//         if (e.message != null &&
//             e['message'] ==
//                 "PlatformException(1, Purchase was cancelled., {code: 1, message: Purchase was cancelled., readable_error_code: PURCHASE_CANCELLE…") {
//           return;
//         }

//         _delegate.showMessage(e.toString());
//       }
//     }

//     if (event is ResetSubscriptionEvent) {
//       // locator<RevenueCatService>().reset();
//       // _delegate.showMessage(
//       //     'Close and re-open app to see refreshed subscription status.');
//     }
//   }
// }
