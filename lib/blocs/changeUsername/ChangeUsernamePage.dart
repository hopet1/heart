// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/blocs/changeUsername/Bloc.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import '../../ServiceLocator.dart';
//
// class ChangeUsernamePage extends StatefulWidget {
//   @override
//   State createState() => ChangeUsernamePageState();
// }
//
// class ChangeUsernamePageState extends State<ChangeUsernamePage>
//     implements ChangeUsernameBlocDelegate {
//   TextEditingController _usernameController = TextEditingController();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   ChangeUsernameBloc _changeUsernameBloc;
//
//   @override
//   void initState() {
//     _changeUsernameBloc = BlocProvider.of<ChangeUsernameBloc>(context);
//     _changeUsernameBloc.setDelegate(delegate: this);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Change Username',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: _scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: BlocBuilder<ChangeUsernameBloc, ChangeUsernameState>(
//           builder: (BuildContext context, ChangeUsernameState state) {
//             if (state is LoadingState) {
//               return Spinner();
//             }
//
//             if (state is LoadedState) {
//               _usernameController.text = state.currentUser.username;
//
//               return Form(
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 key: state.formKey,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Padding(
//                       padding: EdgeInsets.all(30),
//                       child: TextFormField(
//                         controller: _usernameController,
//                         keyboardType: TextInputType.text,
//                         textInputAction: TextInputAction.done,
//                         onFieldSubmitted: (term) {},
//                         validator: locator<ValidatorService>().isEmpty,
//                         onSaved: (value) {},
//                         decoration: InputDecoration(
//                           hintText: 'Username',
//                         ),
//                       ),
//                     ),
//                     Spacer(),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         RaisedButton(
//                           color: Colors.red,
//                           textColor: Colors.white,
//                           child: Text('Save'),
//                           onPressed: () {
//                             _changeUsernameBloc.add(
//                               SubmitEvent(
//                                 formKey: state.formKey,
//                                 username: _usernameController.text,
//                               ),
//                             );
//                           },
//                         )
//                         // SilentButton(
//                         //   title: 'SAVE',
//                         //   onTap: () => {
//                         //     _changeUsernameBloc.add(
//                         //       SubmitEvent(
//                         //         formKey: state.formKey,
//                         //         username: _usernameController.text,
//                         //       ),
//                         //     )
//                         //   },
//                         // )
//                       ],
//                     ),
//                   ],
//                 ),
//               );
//             }
//
//             return Container();
//           },
//         ),
//       ),
//     );
//   }
//
//   @override
//   void showMessage({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
