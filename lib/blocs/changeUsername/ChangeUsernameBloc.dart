// import 'package:flutter/widgets.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'Bloc.dart';
//
// abstract class ChangeUsernameBlocDelegate {
//   void showMessage({@required String message});
// }
//
// class ChangeUsernameBloc
//     extends Bloc<ChangeUsernameEvent, ChangeUsernameState> {
//   ChangeUsernameBloc() : super(null);
//
//   ChangeUsernameBlocDelegate _changeUsernameBlocDelegate;
//   UserModel _currentUser;
//
//   void setDelegate({@required ChangeUsernameBlocDelegate delegate}) {
//     this._changeUsernameBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<ChangeUsernameState> mapEventToState(
//       ChangeUsernameEvent event) async* {
//     if (event is LoadPageEvent) {
//       try {
//         _currentUser = await locator<AuthService>().getCurrentUser();
//         yield LoadedState(
//           autoValidate: false,
//           formKey: GlobalKey<FormState>(),
//           currentUser: _currentUser,
//         );
//       } catch (error) {
//         //todo...
//       }
//     }
//
//     if (event is SubmitEvent) {
//       final GlobalKey<FormState> formKey = event.formKey;
//       final String username = event.username;
//
//       if (formKey.currentState.validate()) {
//         formKey.currentState.save();
//
//         try {
//           yield LoadingState();
//
//           if (formKey.currentState.validate()) {
//             formKey.currentState.save();
//
//             await locator<UsersDBService>().updateUser(
//                 uid: _currentUser.uid, data: {'username': username});
//
//             _currentUser.username = username;
//           }
//           _changeUsernameBlocDelegate.showMessage(
//             message: 'Username updated.',
//           );
//           yield LoadedState(
//             autoValidate: true,
//             formKey: event.formKey,
//             currentUser: _currentUser,
//           );
//         } catch (error) {
//           _changeUsernameBlocDelegate.showMessage(
//             message: error.toString(),
//           );
//           yield LoadedState(
//             autoValidate: true,
//             formKey: event.formKey,
//             currentUser: _currentUser,
//           );
//         }
//       }
//     }
//   }
// }
