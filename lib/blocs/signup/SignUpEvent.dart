// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
//
// abstract class SignUpEvent extends Equatable {
//   @override
//   List<Object> get props => [];
// }
//
// class SignUp extends SignUpEvent {
//   final String email;
//   final String password;
//   final String username;
//   final String confirmPassword;
//   final GlobalKey<FormState> formKey;
//
//   SignUp({
//     @required this.email,
//     @required this.password,
//     @required this.username,
//     @required this.confirmPassword,
//     @required this.formKey,
//   });
//
//   List<Object> get props => [
//         email,
//         password,
//         username,
//         confirmPassword,
//         formKey,
//       ];
// }
//
// class TermsServiceCheckboxEvent extends SignUpEvent {
//   final bool checked;
//   final GlobalKey<FormState> formKey;
//
//   TermsServiceCheckboxEvent({
//     @required this.checked,
//     @required this.formKey,
//   });
//
//   List<Object> get props => [
//         checked,
//         formKey,
//       ];
// }
