// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/data.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// // import 'package:heart/services/AnalyticsService.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'Bloc.dart';
//
// abstract class SignUpBlocDelegate {
//   void navigateHome();
//   void showMessage({@required String message});
// }
//
// class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
//   SignUpBloc()
//       : super(
//           SignUpNotStarted(
//             autoValidate: false,
//             formKey: GlobalKey<FormState>(),
//             termsServicesChecked: false,
//           ),
//         );
//   SignUpBlocDelegate _signUpBlocDelegate;
//
//   bool _termsServicesChecked = false;
//
//   void setDelegate({
//     @required SignUpBlocDelegate delegate,
//   }) {
//     this._signUpBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
//     if (event is SignUp) {
//       yield SigningIn();
//
//       try {
//         final String email = event.email;
//         final String password = event.password;
//         final String confirmPassword = event.confirmPassword;
//         final String username = event.username;
//
//         if (password != confirmPassword) {
//           throw Exception('Passwords do not match.');
//         }
//
//         UserCredential userCredential =
//             await locator<AuthService>().createUserWithEmailAndPassword(
//           email: email,
//           password: password,
//         );
//
//         final User firebaseUser = userCredential.user;
//
//         UserModel user = UserModel(
//           imgUrl: DUMMY_PROFILE_PHOTO_URL,
//           isAdmin: false,
//           isOnline: false,
//           email: email,
//           fcmToken: '',
//           time: DateTime.now(),
//           uid: firebaseUser.uid,
//           username: username,
//           canGiveAdvice: false,
//           activeSubscription: null,
//           freeMessageCount: 3,
//         );
//
//         await locator<UsersDBService>().createUser(user: user);
//
//         // locator<AnalyticsService>()
//         //     .logEvent(eventType: AnalyticsEventType.SIGNUP);
//
//         _signUpBlocDelegate.navigateHome();
//       } catch (error) {
//         _signUpBlocDelegate.showMessage(message: '${error.toString()}');
//
//         yield SignUpNotStarted(
//           autoValidate: true,
//           formKey: event.formKey,
//           termsServicesChecked: _termsServicesChecked,
//         );
//       }
//     }
//
//     if (event is TermsServiceCheckboxEvent) {
//       _termsServicesChecked = event.checked;
//
//       yield SignUpNotStarted(
//           autoValidate: true,
//           formKey: event.formKey,
//           termsServicesChecked: _termsServicesChecked);
//     }
//   }
// }
