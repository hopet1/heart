// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/asset_images.dart';
// import 'package:heart/pages/TermsServicePage.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// class SignUpPage extends StatefulWidget {
//   @override
//   State createState() => SignUpPageState();
// }
//
// class SignUpPageState extends State<SignUpPage>
//     with SingleTickerProviderStateMixin
//     implements SignUpBlocDelegate {
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final TextEditingController _confirmPasswordController =
//       TextEditingController();
//   final TextEditingController _usernameController = TextEditingController();
//   SignUpBloc _signUpBloc;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // double screenWidth = MediaQuery.of(context).size.width;
//     // double screenHeight = MediaQuery.of(context).size.height;
//
//     _signUpBloc = BlocProvider.of<SignUpBloc>(context);
//     _signUpBloc.setDelegate(delegate: this);
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Sign Up',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: _scaffoldKey,
//       body: BlocBuilder<SignUpBloc, SignUpState>(
//         builder: (BuildContext context, SignUpState state) {
//           if (state is SigningIn) {
//             return Spinner();
//           }
//
//           if (state is SignUpNotStarted) {
//             return Stack(
//               children: [
//                 Container(
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                         image: signUpImage,
//                         fit: BoxFit.cover,
//                         alignment: Alignment.center),
//                   ),
//                 ),
//                 Container(
//                   decoration: BoxDecoration(
//                     gradient: LinearGradient(
//                         colors: [
//                           Colors.grey.withOpacity(0.9),
//                           Colors.grey.withOpacity(0.7)
//                         ],
//                         begin: Alignment.topCenter,
//                         end: Alignment.bottomCenter,
//                         stops: [0, 1]),
//                   ),
//                 ),
//                 SafeArea(
//                   child: Padding(
//                     padding: EdgeInsets.all(20),
//                     child: Form(
//                       key: state.formKey,
//                       // autovalidate: autoValidate,
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           TextFormField(
//                             style: TextStyle(
//                               color: Colors.white,
//                             ),
//                             cursorColor: Colors.white,
//                             autovalidateMode:
//                                 AutovalidateMode.onUserInteraction,
//                             controller: _usernameController,
//                             keyboardType: TextInputType.text,
//                             textInputAction: TextInputAction.done,
//                             onFieldSubmitted: (term) {},
//                             validator: locator<ValidatorService>().isEmpty,
//                             onSaved: (value) {},
//                             decoration: InputDecoration(
//                               focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(color: Colors.white),
//                               ),
//                               hintStyle: TextStyle(
//                                 color: Colors.white,
//                               ),
//                               hintText: 'Username',
//                               fillColor: Colors.white,
//                             ),
//                           ),
//                           SizedBox(
//                             height: 20,
//                           ),
//                           TextFormField(
//                             style: TextStyle(
//                               color: Colors.white,
//                             ),
//                             cursorColor: Colors.white,
//                             autovalidateMode:
//                                 AutovalidateMode.onUserInteraction,
//                             controller: _emailController,
//                             keyboardType: TextInputType.emailAddress,
//                             textInputAction: TextInputAction.done,
//                             onFieldSubmitted: (term) {},
//                             validator: locator<ValidatorService>().email,
//                             onSaved: (value) {},
//                             decoration: InputDecoration(
//                               focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(color: Colors.white),
//                               ),
//                               hintStyle: TextStyle(
//                                 color: Colors.white,
//                               ),
//                               hintText: 'Email',
//                               fillColor: Colors.white,
//                             ),
//                           ),
//                           SizedBox(
//                             height: 20,
//                           ),
//                           TextFormField(
//                             style: TextStyle(
//                               color: Colors.white,
//                             ),
//                             cursorColor: Colors.white,
//                             autovalidateMode:
//                                 AutovalidateMode.onUserInteraction,
//                             controller: _passwordController,
//                             keyboardType: TextInputType.visiblePassword,
//                             textInputAction: TextInputAction.done,
//                             onFieldSubmitted: (term) {},
//                             obscureText: true,
//                             validator: locator<ValidatorService>().isEmpty,
//                             onSaved: (value) {},
//                             decoration: InputDecoration(
//                               focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(color: Colors.white),
//                               ),
//                               hintStyle: TextStyle(
//                                 color: Colors.white,
//                               ),
//                               hintText: 'Password',
//                               // icon: Icon(Icons.email),
//                               fillColor: Colors.white,
//                             ),
//                           ),
//                           SizedBox(height: 20),
//                           TextFormField(
//                             style: TextStyle(
//                               color: Colors.white,
//                             ),
//                             cursorColor: Colors.white,
//                             autovalidateMode:
//                                 AutovalidateMode.onUserInteraction,
//                             controller: _confirmPasswordController,
//                             keyboardType: TextInputType.visiblePassword,
//                             textInputAction: TextInputAction.done,
//                             onFieldSubmitted: (term) {},
//                             obscureText: true,
//                             validator: locator<ValidatorService>().isEmpty,
//                             onSaved: (value) {},
//                             decoration: InputDecoration(
//                               focusedBorder: UnderlineInputBorder(
//                                 borderSide: BorderSide(color: Colors.white),
//                               ),
//                               hintStyle: TextStyle(
//                                 color: Colors.white,
//                               ),
//                               hintText: 'Confirm Password',
//                               // icon: Icon(Icons.email),
//                               fillColor: Colors.white,
//                             ),
//                           ),
//                           CheckboxListTile(
//                             title: InkWell(
//                               onTap: () {
//                                 Route route = MaterialPageRoute(
//                                   builder: (BuildContext context) =>
//                                       TermsServicePage(),
//                                 );
//                                 Navigator.of(context).push(route);
//                               },
//                               child: RichText(
//                                 text: TextSpan(
//                                   style: TextStyle(
//                                       color: Colors.white, fontSize: 14),
//                                   children: [
//                                     TextSpan(
//                                         text: 'I accept and agree to the '),
//                                     TextSpan(
//                                       text: 'Terms & Services',
//                                       style: TextStyle(
//                                           fontWeight: FontWeight.bold),
//                                     ),
//                                     TextSpan(text: ' of HEART.')
//                                   ],
//                                 ),
//                               ),
//                             ),
//                             // value: state.termsServicesChecked,
//                             value: state.termsServicesChecked,
//                             onChanged: (newValue) {
//                               _signUpBloc.add(
//                                 TermsServiceCheckboxEvent(
//                                   formKey: state.formKey,
//                                   checked: newValue,
//                                 ),
//                               );
//                             },
//                           ),
//                           Spacer(),
//                           Row(
//                             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                             children: <Widget>[
//                               RaisedButton(
//                                 color: Colors.white,
//                                 textColor: Colors.red,
//                                 child: Text('Sign Up'),
//                                 onPressed: () {
//                                   if (!state.termsServicesChecked) {
//                                     locator<ModalService>().showInSnackBar(
//                                         context: context,
//                                         message:
//                                             'Error: You must check the Terms & Service first.');
//                                     return;
//                                   }
//                                   _signUpBloc.add(
//                                     SignUp(
//                                       username: _usernameController.text,
//                                       password: _passwordController.text,
//                                       confirmPassword:
//                                           _confirmPasswordController.text,
//                                       email: _emailController.text,
//                                       formKey: state.formKey,
//                                     ),
//                                   );
//                                 },
//                               )
//                             ],
//                           )
//                         ],
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             );
//           }
//
//           return Center(
//             child: Text('You Should NEVER see this.'),
//           );
//         },
//       ),
//     );
//   }
//
//   @override
//   void navigateHome() {
//     Navigator.of(context).pop();
//   }
//
//   @override
//   void showMessage({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
