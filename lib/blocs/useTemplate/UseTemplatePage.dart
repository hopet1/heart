// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/blocs/subscription/Bloc.dart' as SUBSCRIPTION_BP;
// import 'package:heart/widgets/UserTemplateListTile.dart';
// import 'package:pagination/pagination.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// class UseTemplatePage extends StatefulWidget {
//   @override
//   State createState() => UseTemplatePageState();
// }
//
// class UseTemplatePageState extends State<UseTemplatePage>
//     implements UseTemplateBlocDelegate {
//   final double categoryImageHeight = 250;
//   UseTemplateBloc _useTemplateBloc;
//
//   @override
//   void initState() {
//     _useTemplateBloc = BlocProvider.of<UseTemplateBloc>(context);
//     _useTemplateBloc.setDelegate(delegate: this);
//     super.initState();
//   }
//
//   Future<List<TemplateModel>> pageFetch(int offset) async {
//     //Fetch template documents.
//     List<DocumentSnapshot> documentSnapshots = await locator<TemplatesService>()
//         .retrieveTemplatesPaginated(
//             category: _useTemplateBloc.category.title,
//             safe: true,
//             limit: 20,
//             startAfterDocument: _useTemplateBloc.startAfterDocument,
//             orderBy: _useTemplateBloc.orderBy);
//
//     //Return an empty list if there are no new documents.
//     if (documentSnapshots.isEmpty) {
//       return [];
//     }
//
//     //Save reference to last fetched document.
//     _useTemplateBloc.add(
//       UpdateStartAfterDocumentEvent(
//           documentSnapshot: documentSnapshots[documentSnapshots.length - 1]),
//     );
//     // _useTemplateBloc.startAfterDocument =
//     //     documentSnapshots[documentSnapshots.length - 1];
//
//     //Reinitialize the template models.
//     List<TemplateModel> templates = [];
//
//     //Convert documents to template models.
//     // documentSnapshots.forEach((documentSnapshot) {
//     //   TemplateModel templateModel =
//     //       TemplateModel.extractDocument(documentSnapshot);
//     //   templates.add(templateModel);
//     // });
//
//     return templates;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<UseTemplateBloc, UseTemplateState>(
//       builder: (BuildContext context, UseTemplateState state) {
//         final double screenWidth = MediaQuery.of(context).size.width;
//
//         if (state is LoadingState) {
//           return Scaffold(
//             backgroundColor: Colors.white,
//             body: Spinner(),
//           );
//         }
//
//         if (state is SubscribedState) {
//           return Scaffold(
//             backgroundColor: Colors.white,
//             body: NestedScrollView(
//               headerSliverBuilder:
//                   (BuildContext context, bool innerBoxIsScrolled) {
//                 return <Widget>[
//                   SliverAppBar(
//                     backgroundColor: Colors.black,
//                     expandedHeight: 200.0,
//                     floating: false,
//                     pinned: true,
//                     flexibleSpace: FlexibleSpaceBar(
//                       centerTitle: true,
//                       title: Text(
//                         _useTemplateBloc.category.title,
//                         style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 16.0,
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       background: Container(
//                         height: categoryImageHeight,
//                         width: screenWidth,
//                         decoration: BoxDecoration(
//                           image: DecorationImage(
//                             colorFilter: ColorFilter.mode(
//                                 Colors.black.withOpacity(0.3),
//                                 BlendMode.darken),
//                             image: _useTemplateBloc.category.image.image,
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ];
//               },
//               body: PaginationList<TemplateModel>(
//                 onLoading: Spinner(
//                   text: 'Loading...',
//                 ),
//                 onPageLoading: Spinner(
//                   text: 'Loading...',
//                 ),
//                 separatorWidget: Divider(),
//                 itemBuilder: (BuildContext context, TemplateModel template) {
//                   return UserTemplateListTile(
//                     key: UniqueKey(),
//                     forAdmin: false,
//                     template: template,
//                     category: _useTemplateBloc.category,
//                   );
//                 },
//                 pageFetch: pageFetch,
//                 onError: (dynamic error) => Center(
//                   child: Text('Something Went Wrong'),
//                 ),
//                 onEmpty: Center(
//                   child: Text('Empty List'),
//                 ),
//               ),
//             ),
//           );
//         }
//
//         if (state is UnsubscribedState) {
//           final double screenWidth = MediaQuery.of(context).size.width;
//
//           return Scaffold(
//             backgroundColor: Colors.white,
//             body: NestedScrollView(
//               headerSliverBuilder:
//                   (BuildContext context, bool innerBoxIsScrolled) {
//                 return <Widget>[
//                   SliverAppBar(
//                     backgroundColor: Colors.black,
//                     expandedHeight: 200.0,
//                     floating: false,
//                     pinned: true,
//                     flexibleSpace: FlexibleSpaceBar(
//                       centerTitle: true,
//                       title: Text(
//                         _useTemplateBloc.category.title,
//                         style: TextStyle(
//                           color: Colors.white,
//                           fontSize: 16.0,
//                           fontWeight: FontWeight.bold,
//                         ),
//                       ),
//                       background: Container(
//                         height: categoryImageHeight,
//                         width: screenWidth,
//                         decoration: BoxDecoration(
//                           image: DecorationImage(
//                             colorFilter: ColorFilter.mode(
//                                 Colors.black.withOpacity(0.3),
//                                 BlendMode.darken),
//                             image: _useTemplateBloc.category.image.image,
//                             fit: BoxFit.cover,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ];
//               },
//               body: SafeArea(
//                 child: Column(
//                   children: [
//                     Expanded(
//                       child: ListView.builder(
//                         itemCount: state.templates.length,
//                         itemBuilder: (BuildContext context, int index) {
//                           final TemplateModel template = state.templates[index];
//                           return UserTemplateListTile(
//                             key: UniqueKey(),
//                             forAdmin: false,
//                             template: template,
//                             category: _useTemplateBloc.category,
//                           );
//                         },
//                       ),
//                     ),
//                     InkWell(
//                       child: Text(
//                         'Click here to subscribe and see more messages...',
//                         style: TextStyle(
//                           color: Colors.grey,
//                         ),
//                       ),
//                       onTap: () {
//                         // Route route = MaterialPageRoute(
//                         //   builder: (BuildContext context) => BlocProvider(
//                         //     create: (BuildContext context) =>
//                         //         SUBSCRIPTION_BP.SubscriptionBloc()
//                         //           ..add(SUBSCRIPTION_BP.LoadPageEvent()),
//                         //     child: SUBSCRIPTION_BP.SubscriptionPage(),
//                         //   ),
//                         // );
//
//                         // Navigator.push(context, route);
//                       },
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           );
//         }
//
//         return Container();
//       },
//     );
//   }
//
//   @override
//   void showMessage({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
