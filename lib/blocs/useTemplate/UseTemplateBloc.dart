// import 'dart:math';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter/widgets.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// abstract class UseTemplateBlocDelegate {
//   void showMessage({@required String message});
// }
//
// class UseTemplateBloc extends Bloc<UseTemplateEvent, UseTemplateState> {
//   UseTemplateBloc({
//     @required this.category,
//   }) : super(null);
//
//   final CategoryModel category;
//   final List<String> orderByOptions = [
//     'time',
//     'id',
//     'message',
//     'userID',
//   ];
//
//   UseTemplateBlocDelegate _useTemplateBlocDelegate;
//   DocumentSnapshot startAfterDocument;
//   String orderBy;
//   UserModel _currentUser;
//
//   void setDelegate({@required UseTemplateBlocDelegate delegate}) {
//     this._useTemplateBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<UseTemplateState> mapEventToState(UseTemplateEvent event) async* {
//     if (event is LoadPageEvent) {
//       try {
//         yield LoadingState();
//
//         _currentUser = await locator<AuthService>().getCurrentUser();
//
//         int randomNumber = Random().nextInt(orderByOptions.length);
//         orderBy = orderByOptions[randomNumber];
//
//         if (_currentUser.activeSubscription == null) {
//           List<TemplateModel> templates =
//               await locator<TemplatesService>().retrieveTemplates(
//             limit: 5,
//             orderBy: 'time',
//             category: category.title,
//             safe: true,
//           );
//
//           yield UnsubscribedState(templates: templates);
//         } else {
//           yield SubscribedState();
//         }
//       } catch (error) {
//         _useTemplateBlocDelegate.showMessage(
//           message: error.toString(),
//         );
//       }
//     }
//
//     if (event is UpdateStartAfterDocumentEvent) {
//       startAfterDocument = event.documentSnapshot;
//     }
//   }
// }
