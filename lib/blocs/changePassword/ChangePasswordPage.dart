// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// class ChangePasswordPage extends StatefulWidget {
//   @override
//   State createState() => ChangePasswordPageState();
// }
//
// class ChangePasswordPageState extends State<ChangePasswordPage>
//     implements ChangePasswordBlocDelegate {
//   TextEditingController _passwordController = TextEditingController();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   ChangePasswordBloc _changePasswordBloc;
//
//   @override
//   void initState() {
//     _changePasswordBloc = BlocProvider.of<ChangePasswordBloc>(context);
//     _changePasswordBloc.setDelegate(delegate: this);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Change Password',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: _scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: BlocBuilder<ChangePasswordBloc, ChangePasswordState>(
//           builder: (BuildContext context, ChangePasswordState state) {
//             if (state is LoadingState) {
//               return Spinner();
//             }
//
//             if (state is LoadedState) {
//               return Form(
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 key: state.formKey,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Padding(
//                       padding: EdgeInsets.all(30),
//                       child: TextFormField(
//                         controller: _passwordController,
//                         keyboardType: TextInputType.text,
//                         textInputAction: TextInputAction.done,
//                         onFieldSubmitted: (term) {},
//                         validator: locator<ValidatorService>().password,
//                         onSaved: (value) {},
//                         decoration: InputDecoration(
//                           hintText: 'Password',
//                         ),
//                       ),
//                     ),
//                     Spacer(),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         RaisedButton(
//                           textColor: Colors.white,
//                           color: Colors.red,
//                           child: Text('Save'),
//                           onPressed: () {
//                             _changePasswordBloc.add(
//                               SubmitEvent(
//                                 formKey: state.formKey,
//                                 password: _passwordController.text,
//                               ),
//                             );
//                           },
//                         )
//                         // SilentButton(
//                         //   title: 'SAVE',
//                         //   onTap: () => {
//                         //     _changePasswordBloc.add(
//                         //       SubmitEvent(
//                         //         formKey: state.formKey,
//                         //         password: _passwordController.text,
//                         //       ),
//                         //     )
//                         //   },
//                         // )
//                       ],
//                     ),
//                   ],
//                 ),
//               );
//             }
//
//             return Container();
//           },
//         ),
//       ),
//     );
//   }
//
//   @override
//   void showMessage({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
