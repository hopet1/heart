// import 'package:flutter/widgets.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'Bloc.dart';
//
// abstract class ChangePasswordBlocDelegate {
//   void showMessage({@required String message});
// }
//
// class ChangePasswordBloc
//     extends Bloc<ChangePasswordEvent, ChangePasswordState> {
//   ChangePasswordBloc() : super(null);
//
//   ChangePasswordBlocDelegate _changePasswordBlocDelegate;
//   UserModel _currentUser;
//
//   void setDelegate({@required ChangePasswordBlocDelegate delegate}) {
//     this._changePasswordBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<ChangePasswordState> mapEventToState(
//       ChangePasswordEvent event) async* {
//     if (event is LoadPageEvent) {
//       try {
//         _currentUser = await locator<AuthService>().getCurrentUser();
//         yield LoadedState(
//           autoValidate: false,
//           formKey: GlobalKey<FormState>(),
//           currentUser: _currentUser,
//         );
//       } catch (error) {
//         _changePasswordBlocDelegate.showMessage(
//           message: error.toString(),
//         );
//       }
//     }
//
//     if (event is SubmitEvent) {
//       final GlobalKey<FormState> formKey = event.formKey;
//       final String password = event.password;
//
//       if (formKey.currentState.validate()) {
//         formKey.currentState.save();
//
//         try {
//           yield LoadingState();
//
//           if (formKey.currentState.validate()) {
//             formKey.currentState.save();
//
//             locator<AuthService>().updatePassword(password: password);
//           }
//
//           _changePasswordBlocDelegate.showMessage(
//             message: 'Password updated.',
//           );
//
//           yield LoadedState(
//             autoValidate: true,
//             formKey: event.formKey,
//             currentUser: _currentUser,
//           );
//         } catch (error) {
//           _changePasswordBlocDelegate.showMessage(
//             message: error.toString(),
//           );
//           yield LoadedState(
//             autoValidate: true,
//             formKey: event.formKey,
//             currentUser: _currentUser,
//           );
//         }
//       }
//     }
//   }
// }
