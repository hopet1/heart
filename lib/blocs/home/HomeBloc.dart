// import 'dart:io';
// import 'dart:ui';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// // import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/models/firebase/ConversationModel.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/ConversationsDBService.dart';
// import 'package:heart/services/MetaDataDBService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/QuoteOfTheDayDBService.dart';
// import 'package:heart/services/RevenueCatService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/services/WordOfTheDayDBService.dart';
// import '../../data.dart';
// import 'Bloc.dart' as HOME_BP;
// import 'package:heart/blocs/message/Bloc.dart' as MESSAGE_BP;
//
// abstract class HomeBlocDelegate {
//   void navigateToSettings({@required bool isAdmin});
//   void showMessageInSnackbar({@required String message});
// }
//
// class HomeBloc extends Bloc<HOME_BP.HomeEvent, HOME_BP.HomeState> {
//   final BuildContext context;
//   // final FirebaseMessaging _fcm = FirebaseMessaging();
//
//   UserModel _currentUser;
//   TextOfTheDayModel _quoteOfTheDay;
//   TextOfTheDayModel _wordOfTheDay;
//   int _autoPlayIntervalFuture;
//
//   HomeBlocDelegate _delegate;
//
//   HomeBloc({
//     @required this.context,
//   }) : super(null);
//
//   //Assigns a delegate to this class.
//   void setDelegate({@required HomeBlocDelegate delegate}) {
//     this._delegate = delegate;
//   }
//
//   // Future<ConversationModel> buildConversationModel(
//   //     {@required String oppositeUserID}) async {
//   //   //Fetch oposite user.
//   //   final UserModel oppositeUser =
//   //       await locator<UsersDBService>().retrieveUser(uid: oppositeUserID);
//
//   //   final DocumentSnapshot conversationDoc =
//   //       await locator<ConversationsDBService>().getConversation(
//   //           thisUserID: _currentUser.uid, thatUserID: oppositeUser.uid);
//
//   //   ConversationModel conversationModel = ConversationModel(
//   //     title: oppositeUser.username,
//   //     // lastMessage: conversationDoc.data()['lastMessage'],
//   //     imageUrl: oppositeUser.imgUrl,
//   //     sender: ChatUser(
//   //         uid: _currentUser.uid,
//   //         name: _currentUser.username,
//   //         avatar: _currentUser.imgUrl,
//   //         fcmToken: _currentUser.fcmToken),
//   //     sendee: ChatUser(
//   //         uid: oppositeUser.uid,
//   //         name: oppositeUser.username,
//   //         avatar: oppositeUser.imgUrl,
//   //         fcmToken: oppositeUser.fcmToken),
//   //     reference: conversationDoc.reference,
//   //     time: conversationDoc.data()['time'].toDate(),
//   //     read: conversationDoc.data()['${_currentUser.uid}_read'],
//   //   );
//
//   //   return conversationModel;
//   // }
//
//   @override
//   Stream<HOME_BP.HomeState> mapEventToState(HOME_BP.HomeEvent event) async* {
//     if (event is HOME_BP.LoadPageEvent) {
//       try {
//         yield HOME_BP.LoadingState();
//
//         _quoteOfTheDay =
//             await locator<QuoteOfTheDayDBService>().retrieveQuoteOfTheDay();
//         _wordOfTheDay =
//             await locator<WordOfTheDayDBService>().retrieveWordOfTheDay();
//
//         _autoPlayIntervalFuture =
//             await locator<MetaDataDBService>().getCarouselSliderDuration();
//
//         _currentUser = await locator<AuthService>().getCurrentUser();
//
//         locator<UsersDBService>()
//             .updateUser(uid: _currentUser.uid, data: {'isOnline': true});
//
//         if (Platform.isIOS) {
//           // _fcm.requestNotificationPermissions(
//           //   IosNotificationSettings(),
//           // );
//         }
//
//         // final String fcmToken = await _fcm.getToken();
//
//         // if (fcmToken != null) {
//         //   locator<UsersDBService>()
//         //       .updateUser(uid: _currentUser.uid, data: {'fcmToken': fcmToken});
//         // }
//
//         // SubscriptionStatus subscriptionStatus =
//         //     await locator<RevenueCatService>().getActiveSubscriptionStatus();
//
//         // switch (subscriptionStatus) {
//         //   case SubscriptionStatus.PREMIUM_PLAN_SUBSCRIBED:
//         //     locator<UsersDBService>().updateUser(
//         //       uid: _currentUser.uid,
//         //       data: {
//         //         'activeSubscription': revenueCatPremiumPlan,
//         //       },
//         //     );
//         //     break;
//         //   case SubscriptionStatus.NO_PLAN_SUBSCRIBED:
//         //     locator<UsersDBService>().updateUser(
//         //       uid: _currentUser.uid,
//         //       data: {
//         //         'activeSubscription': null,
//         //       },
//         //     );
//         //     break;
//         // }
//
//         // _fcm.configure(
//         //   onResume: (Map<String, dynamic> message) async {
//         //     print('onResume:');
//
//         //     //Get message type.
//         //     final String type = message['type'];
//
//         //     //Detect what type of notification this is.
//         //     switch (type) {
//         //       case FCM_NEW_MESSAGE:
//
//         //         //Extract data from message.
//         //         final String title = message['message'];
//         //         final String subtitle = 'Open message now?';
//         //         final String userID = message['userID'];
//
//         //         //Call to open pop up confirmation window.
//         //         final bool confirm =
//         //             await locator<ModalService>().showConfirmation(
//         //           context: context,
//         //           title: title,
//         //           message: subtitle,
//         //         );
//
//         //         if (!confirm) return;
//
//         //         //Build conversation from current user and opposite user info.
//         //         ConversationModel conversationModel =
//         //             await buildConversationModel(oppositeUserID: userID);
//
//         //         //Prepare route to Message Bloc.
//         //         Route route = MaterialPageRoute(
//         //           builder: (BuildContext context) => BlocProvider(
//         //             create: (BuildContext context) => MESSAGE_BP.MessageBloc(
//         //               sendee: conversationModel.sendee,
//         //               sender: conversationModel.sender,
//         //               convoDocRef: conversationModel.reference,
//         //             )..add(MESSAGE_BP.LoadPageEvent()),
//         //             child: MESSAGE_BP.MessagePage(),
//         //           ),
//         //         );
//
//         //         Navigator.push(
//         //           context,
//         //           route,
//         //         );
//         //         break;
//         //       default:
//         //         break;
//         //     }
//         //   },
//         //   onLaunch: (Map<String, dynamic> message) async {
//         //     print('onLaunch:');
//
//         //     //Get message type.
//         //     final String type = message['type'];
//
//         //     //Detect what type of notification this is.
//         //     switch (type) {
//         //       case FCM_NEW_MESSAGE:
//
//         //         //Extract data from message.
//         //         final String title = message['message'];
//         //         final String subtitle = 'Open message now?';
//         //         final String userID = message['userID'];
//
//         //         //Call to open pop up confirmation window.
//         //         final bool confirm =
//         //             await locator<ModalService>().showConfirmation(
//         //           context: context,
//         //           title: title,
//         //           message: subtitle,
//         //         );
//
//         //         if (!confirm) return;
//
//         //         //Build conversation from current user and opposite user info.
//         //         ConversationModel conversationModel =
//         //             await buildConversationModel(oppositeUserID: userID);
//
//         //         //Prepare route to Message Bloc.
//         //         Route route = MaterialPageRoute(
//         //           builder: (BuildContext context) => BlocProvider(
//         //             create: (BuildContext context) => MESSAGE_BP.MessageBloc(
//         //               sendee: conversationModel.sendee,
//         //               sender: conversationModel.sender,
//         //               convoDocRef: conversationModel.reference,
//         //             )..add(MESSAGE_BP.LoadPageEvent()),
//         //             child: MESSAGE_BP.MessagePage(),
//         //           ),
//         //         );
//
//         //         Navigator.push(
//         //           context,
//         //           route,
//         //         );
//         //         break;
//         //       default:
//         //         break;
//         //     }
//         //   },
//         //   onMessage: (Map<String, dynamic> message) async {
//         //     print('onMessage:');
//         //     //Get message type.
//         //     final String type = message['type'];
//
//         //     //Detect what type of notification this is.
//         //     switch (type) {
//         //       case FCM_NEW_MESSAGE:
//
//         //         //Extract data from message.
//         //         final String title = message['message'];
//         //         final String subtitle = 'Open message now?';
//         //         final String userID = message['userID'];
//
//         //         //Call to open pop up confirmation window.
//         //         final bool confirm =
//         //             await locator<ModalService>().showConfirmation(
//         //           context: context,
//         //           title: title,
//         //           message: subtitle,
//         //         );
//
//         //         if (!confirm) return;
//
//         //         //Build conversation from current user and opposite user info.
//         //         ConversationModel conversationModel =
//         //             await buildConversationModel(oppositeUserID: userID);
//
//         //         //Prepare route to Message Bloc.
//         //         Route route = MaterialPageRoute(
//         //           builder: (BuildContext context) => BlocProvider(
//         //             create: (BuildContext context) => MESSAGE_BP.MessageBloc(
//         //               sendee: conversationModel.sendee,
//         //               sender: conversationModel.sender,
//         //               convoDocRef: conversationModel.reference,
//         //             )..add(MESSAGE_BP.LoadPageEvent()),
//         //             child: MESSAGE_BP.MessagePage(),
//         //           ),
//         //         );
//
//         //         Navigator.push(
//         //           context,
//         //           route,
//         //         );
//
//         //         break;
//         //       default:
//         //         break;
//         //     }
//         //   },
//         // );
//
//         //Proceed to the Loaded State.
//         yield HOME_BP.LoadedState(
//           wordOfTheDay: _wordOfTheDay,
//           quoteOfTheDay: _quoteOfTheDay,
//           autoPlayIntervalFuture: _autoPlayIntervalFuture,
//         );
//       } catch (error) {
//         //Proceed to the Error State.
//         yield HOME_BP.ErrorState(error: error);
//       }
//     }
//
//     if (event is HOME_BP.DidChangeAppLifecycleStateEvent) {
//       //Validate the user is logged in.
//       if (_currentUser != null) {
//         //Update a user's 'isOnline' property when the app is in the resume App Lifecycle State.
//         switch (event.appLifecycleState) {
//           case AppLifecycleState.inactive:
//             locator<UsersDBService>()
//                 .updateUser(uid: _currentUser.uid, data: {'isOnline': false});
//             break;
//           case AppLifecycleState.paused:
//             locator<UsersDBService>()
//                 .updateUser(uid: _currentUser.uid, data: {'isOnline': false});
//             break;
//           case AppLifecycleState.detached:
//             locator<UsersDBService>()
//                 .updateUser(uid: _currentUser.uid, data: {'isOnline': false});
//             break;
//           case AppLifecycleState.resumed:
//             locator<UsersDBService>()
//                 .updateUser(uid: _currentUser.uid, data: {'isOnline': true});
//             break;
//         }
//       }
//     }
//
//     if (event is HOME_BP.NavigateToSettingsEvent) {
//       _delegate.navigateToSettings(
//         isAdmin: _currentUser.isAdmin,
//       );
//     }
//   }
// }
