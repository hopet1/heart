// import 'package:flutter/material.dart';
// import 'package:flutter/rendering.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/asset_images.dart';
// import 'package:heart/blocs/home/Bloc.dart' as HOME_BP;
// import 'package:heart/blocs/profile/Bloc.dart' as PROFILE_BP;
// import 'package:heart/blocs/createMessage/Bloc.dart' as CREATE_MESSAGE_BP;
// import 'package:heart/blocs/useTemplate/Bloc.dart' as USE_TEMPLATE_BP;
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/data.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/pages/SettingsPage.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:carousel_slider/carousel_slider.dart';
//
// class HomePage extends StatefulWidget {
//   const HomePage({Key key});
//
//   @override
//   State createState() => HomePageState();
// }
//
// class HomePageState extends State<HomePage>
//     with
//         WidgetsBindingObserver,
//         TickerProviderStateMixin,
//         HOME_BP.HomeBlocDelegate {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   HOME_BP.HomeBloc _homeBlock;
//   final TextStyle _fabText = TextStyle(
//     color: Colors.white,
//     fontSize: 18,
//     fontWeight: FontWeight.bold,
//   );
//
//   @override
//   void initState() {
//     super.initState();
//     WidgetsBinding.instance.addObserver(this);
//
//     _homeBlock = BlocProvider.of<HOME_BP.HomeBloc>(context);
//     _homeBlock.setDelegate(delegate: this);
//   }
//
//   @override
//   void dispose() {
//     //WidgetsBinding.instance.removeObserver(this);
//     super.dispose();
//   }
//
//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) {
//     _homeBlock
//         .add(HOME_BP.DidChangeAppLifecycleStateEvent(appLifecycleState: state));
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//
//     return Scaffold(
//       key: _scaffoldKey,
//       body: BlocBuilder<HOME_BP.HomeBloc, HOME_BP.HomeState>(
//         builder: (BuildContext context, HOME_BP.HomeState state) {
//           if (state is HOME_BP.LoadingState) {
//             return Spinner();
//           }
//
//           if (state is HOME_BP.LoadedState) {
//             return Stack(
//               children: <Widget>[
//                 // Container(color: Colors.red),
//                 Container(
//                   decoration: BoxDecoration(
//                     image: DecorationImage(
//                         image: homeImage,
//                         fit: BoxFit.cover,
//                         alignment: Alignment.center),
//                   ),
//                 ),
//                 Container(
//                   decoration: BoxDecoration(
//                     gradient: LinearGradient(
//                         colors: [
//                           Colors.grey.withOpacity(0.9),
//                           Colors.grey.withOpacity(0.7)
//                         ],
//                         begin: Alignment.topCenter,
//                         end: Alignment.bottomCenter,
//                         stops: [0, 1]),
//                   ),
//                 ),
//                 Column(
//                   crossAxisAlignment: CrossAxisAlignment.center,
//                   children: <Widget>[
//                     SizedBox(
//                       height: 40,
//                     ),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.end,
//                       children: <Widget>[
//                         IconButton(
//                           icon: Icon(
//                             Icons.settings,
//                             color: Colors.white,
//                           ),
//                           onPressed: () {
//                             _homeBlock.add(
//                               HOME_BP.NavigateToSettingsEvent(),
//                             );
//                           },
//                         )
//                       ],
//                     ),
//                     Expanded(
//                       child: ListView(
//                         children: <Widget>[
//                           CarouselSlider(
//                             options: CarouselOptions(
//                               viewportFraction: 1.0,
//                               height: 250.0,
//                               autoPlay: true,
//                               autoPlayInterval: Duration(
//                                   seconds: state.autoPlayIntervalFuture),
//                               autoPlayAnimationDuration:
//                                   Duration(milliseconds: 800),
//                               autoPlayCurve: Curves.fastOutSlowIn,
//                             ),
//                             items: [
//                               Column(
//                                 children: <Widget>[
//                                   Text(
//                                     'Quote of The Day',
//                                     style: TextStyle(
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.white),
//                                     textAlign: TextAlign.center,
//                                   ),
//                                   Padding(
//                                     padding: EdgeInsets.all(40),
//                                     child: Text(
//                                       state.quoteOfTheDay.text,
//                                       style: TextStyle(
//                                           fontSize: 25,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.white),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                               Column(
//                                 children: <Widget>[
//                                   Text(
//                                     'Word of The Day',
//                                     style: TextStyle(
//                                         fontWeight: FontWeight.bold,
//                                         color: Colors.white),
//                                     textAlign: TextAlign.center,
//                                   ),
//                                   Padding(
//                                     padding: EdgeInsets.all(40),
//                                     child: Text(
//                                       state.wordOfTheDay.text,
//                                       style: TextStyle(
//                                           fontSize: 25,
//                                           fontWeight: FontWeight.bold,
//                                           color: Colors.white),
//                                       textAlign: TextAlign.center,
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                           Padding(
//                             padding: EdgeInsets.all(8),
//                             child: Text(
//                               'Select. Edit. Send.',
//                               style: TextStyle(
//                                   color: Colors.white,
//                                   fontWeight: FontWeight.bold,
//                                   fontSize: 18),
//                               textAlign: TextAlign.center,
//                             ),
//                           ),
//                           Divider(),
//                           _buildCategoryView(
//                               category: categories[0],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[1],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[2],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[3],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[4],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[5],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[6],
//                               screenWidth: screenWidth),
//                           _buildCategoryView(
//                               category: categories[7],
//                               screenWidth: screenWidth),
//                         ],
//                       ),
//                     ),
//                     Padding(
//                       padding: EdgeInsets.symmetric(vertical: 20),
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                         children: <Widget>[
//                           // messageToUseIconButton(),
//                           getAdviceIconButton(),
//                           messageToCreateIconButton(),
//                           //settingsIconButton()
//                         ],
//                       ),
//                     ),
//                   ],
//                 ),
//               ],
//             );
//           }
//
//           if (state is HOME_BP.ErrorState) {
//             return Center(
//               child: Text('Error:${state.error.toString()}'),
//             );
//           }
//
//           return Container();
//         },
//       ),
//     );
//   }
//
//   Widget _buildCategoryView({
//     @required CategoryModel category,
//     @required double screenWidth,
//   }) {
//     return InkWell(
//       onTap: () {
//         Route route = MaterialPageRoute(
//           builder: (BuildContext context) => BlocProvider(
//             create: (BuildContext context) => USE_TEMPLATE_BP.UseTemplateBloc(
//               category: category,
//             )..add(
//                 USE_TEMPLATE_BP.LoadPageEvent(),
//               ),
//             child: USE_TEMPLATE_BP.UseTemplatePage(),
//           ),
//         );
//
//         Navigator.push(
//           context,
//           route,
//         );
//       },
//       child: Container(
//         height: 250,
//         width: screenWidth * 0.9,
//         child: Padding(
//           padding: EdgeInsets.all(20),
//           child: Stack(
//             children: <Widget>[
//               Container(
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(10.0),
//                   image: DecorationImage(
//                     image: category.image.image,
//                     fit: BoxFit.cover,
//                   ),
//                 ),
//               ),
//               Container(
//                 alignment: Alignment.center,
//                 decoration: BoxDecoration(
//                   borderRadius: BorderRadius.circular(10.0),
//                   color: Colors.black.withOpacity(0.3),
//                 ),
//                 child: Text(
//                   category.title,
//                   style: TextStyle(
//                     color: Colors.white,
//                     fontWeight: FontWeight.bold,
//                     fontSize: 24.0,
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   Widget getAdviceIconButton() {
//     final double buttonRadius = 100;
//
//     return Column(
//       children: <Widget>[
//         FloatingActionButton(
//           tooltip: 'Get Advice / Profile',
//           heroTag: UniqueKey().toString(),
//           child: Container(
//             width: buttonRadius,
//             height: buttonRadius,
//             decoration: BoxDecoration(
//               shape: BoxShape.circle,
//               image: DecorationImage(
//                 fit: BoxFit.fill,
//                 image: AssetImage('assets/images/app_icon.png'),
//               ),
//             ),
//           ),
//           onPressed: () {
//             Route route = MaterialPageRoute(
//               builder: (BuildContext context) => BlocProvider(
//                 create: (BuildContext context) => PROFILE_BP.ProfileBloc()
//                   ..add(
//                     PROFILE_BP.LoadPageEvent(),
//                   ),
//                 child: PROFILE_BP.ProfilePage(),
//               ),
//             );
//
//             Navigator.push(
//               context,
//               route,
//             );
//           },
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Text(
//           'Get Advice',
//           style: _fabText,
//         )
//       ],
//     );
//   }
//
//   Widget messageToCreateIconButton() {
//     return Column(
//       children: <Widget>[
//         FloatingActionButton(
//           heroTag: UniqueKey().toString(),
//           tooltip: 'Create Message',
//           child: Icon(Icons.add),
//           onPressed: () async {
//             Categories category = await locator<ModalService>()
//                 .showCategoryOptionsDialog(
//                     context: context, title: 'Select Message To Create');
//
//             CategoryModel categoryModel;
//
//             switch (category) {
//               case Categories.GoodMorning:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Good Morning');
//                 break;
//               case Categories.GoodAfternoon:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Good Afternoon');
//                 break;
//               case Categories.GoodNight:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Good Night');
//                 break;
//               case Categories.HappyBirthday:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Happy Birthday');
//                 break;
//               case Categories.HappyAnniversary:
//                 categoryModel = categories
//                     .firstWhere((c) => c.title == 'Happy Anniversary');
//                 break;
//               case Categories.Congrats:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Congrats');
//                 break;
//               case Categories.Apology:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Apology');
//                 break;
//               case Categories.Condolence:
//                 categoryModel =
//                     categories.firstWhere((c) => c.title == 'Condolence');
//                 break;
//               default:
//                 return;
//             }
//
//             Route route = MaterialPageRoute(
//               builder: (BuildContext context) => BlocProvider(
//                 create: (BuildContext context) =>
//                     CREATE_MESSAGE_BP.CreateMessageBloc(category: categoryModel)
//                       ..add(
//                         CREATE_MESSAGE_BP.LoadPageEvent(),
//                       ),
//                 child: CREATE_MESSAGE_BP.CreateMessagePage(),
//               ),
//             );
//
//             Navigator.push(
//               context,
//               route,
//             );
//           },
//         ),
//         SizedBox(
//           height: 10,
//         ),
//         Text(
//           'Create Message',
//           style: _fabText,
//         )
//       ],
//     );
//   }
//
//   @override
//   void navigateToSettings({@required bool isAdmin}) {
//     Route route = MaterialPageRoute(
//       builder: (BuildContext context) => SettingsPage(
//         isAdmin: isAdmin,
//       ),
//     );
//     Navigator.push(context, route);
//   }
//
//   @override
//   void showMessageInSnackbar({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
