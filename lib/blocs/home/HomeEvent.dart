// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
//
// abstract class HomeEvent extends Equatable {
//   @override
//   List<Object> get props => [];
// }
//
// //Event: Load the page data.
// class LoadPageEvent extends HomeEvent {
//   LoadPageEvent();
//
//   List<Object> get props => [];
// }
//
// //Event: The current App Lifecycle State has changed.
// class DidChangeAppLifecycleStateEvent extends HomeEvent {
//   final AppLifecycleState appLifecycleState;
//
//   DidChangeAppLifecycleStateEvent({@required this.appLifecycleState});
//
//   List<Object> get props => [appLifecycleState];
// }
//
// //Event: User selected settings icon button.
// class NavigateToSettingsEvent extends HomeEvent {
//   NavigateToSettingsEvent();
//
//   List<Object> get props => [];
// }
