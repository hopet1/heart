// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/TextOfTheDayModel.dart';
//
// class HomeState extends Equatable {
//   @override
//   List<Object> get props => [];
// }
//
// //State: Initial view, nothing has been changed.
// class LoadingState extends HomeState {}
//
// class LoadedState extends HomeState {
//   final TextOfTheDayModel quoteOfTheDay;
//   final TextOfTheDayModel wordOfTheDay;
//   final int autoPlayIntervalFuture;
//
//   LoadedState({
//     @required this.quoteOfTheDay,
//     @required this.wordOfTheDay,
//     @required this.autoPlayIntervalFuture,
//   });
//
//   @override
//   List<Object> get props => [
//         quoteOfTheDay,
//         wordOfTheDay,
//         autoPlayIntervalFuture,
//       ];
// }
//
// class ErrorState extends HomeState {
//   final dynamic error;
//
//   ErrorState({
//     @required this.error,
//   });
//
//   @override
//   List<Object> get props => [
//         error,
//       ];
// }
