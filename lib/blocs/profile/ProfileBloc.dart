// import 'dart:async';
// import 'dart:io';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/StorageService.dart';
// import 'package:heart/services/UsersDBService.dart';
// import 'package:image_cropper/image_cropper.dart';
// import 'package:image_picker/image_picker.dart';
//
// import 'ProfileEvent.dart';
// import 'ProfileState.dart';
//
// class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
//   ProfileBloc() : super(null);
//   StreamSubscription subscription;
//   UserModel currentUser;
//
//   @override
//   Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
//     //Load page data for initial view.
//     if (event is LoadPageEvent) {
//       //Display spinner.
//       yield LoadingState(text: 'Loading...');
//
//       //Fetch current user.
//       currentUser = await locator<AuthService>().getCurrentUser();
//
//       //Query conversations table.
//       Query query = FirebaseFirestore.instance.collection('Conversations');
//
//       //Filter on user ID.
//       query = query.where(currentUser.uid, isEqualTo: true);
//
//       //Order conversations by time.
//       // query = query.orderBy('time', descending: true);
//
//       //Check to see if conversations are empty.
//       if ((await query.get()).docs.isEmpty) {
//         yield NoConversationsState(currentUser: currentUser);
//       } else {
//         Stream<QuerySnapshot> snapshots = query.snapshots();
//
//         subscription = snapshots.listen((querySnapshot) {
//           add(ConversationAddedEvent(querySnapshot: querySnapshot));
//         });
//       }
//     }
//
//     if (event is ConversationAddedEvent) {
//       yield LoadedState(
//           querySnapshot: event.querySnapshot, currentUser: currentUser);
//     }
//
//     if (event is UpdatePhotoEvent) {
//       //Pick an image.
//       PickedFile file = await ImagePicker().getImage(source: event.imageSource);
//
//       //Check that user picked an image.
//       if (file == null) return;
//
//       //Crop an image.
//       File image = await ImageCropper.cropImage(sourcePath: file.path);
//
//       //Check that user cropped the image.
//       if (image == null) return;
//
//       // //Display spinner.
//       // yield LoadingState(text: 'Loading...');
//
//       //Get image upload url.
//       final String newImgUrl = await locator<StorageService>().uploadImage(
//           file: image, path: 'Images/Users/${currentUser.uid}/Profile');
//
//       //Save image upload url.
//       await locator<UsersDBService>()
//           .updateUser(uid: currentUser.uid, data: {'imgUrl': newImgUrl});
//
//       //Update image url on user.
//       currentUser.imgUrl = newImgUrl;
//
//       add(LoadPageEvent()); //Relaod page.
//     }
//   }
//
//   @override
//   Future<void> close() {
//     subscription?.cancel();
//     return super.close();
//   }
// }
