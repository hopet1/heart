// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/firebase/UserModel.dart';
//
// class ProfileState extends Equatable {
//   @override
//   List<Object> get props => [];
// }
//
// class LoadedState extends ProfileState {
//   final QuerySnapshot querySnapshot;
//   final UserModel currentUser;
//
//   LoadedState({@required this.querySnapshot, @required this.currentUser});
//
//   @override
//   List<Object> get props => [querySnapshot, currentUser];
// }
//
// class NoConversationsState extends ProfileState {
//   final UserModel currentUser;
//
//   NoConversationsState({@required this.currentUser});
//
//   @override
//   List<Object> get props => [currentUser];
// }
//
// class LoadingState extends ProfileState {
//   final String text;
//
//   LoadingState({@required this.text});
//
//   @override
//   List<Object> get props => [text];
// }
//
// class ErrorState extends ProfileState {
//   final dynamic error;
//
//   ErrorState({
//     @required this.error,
//   });
//
//   @override
//   List<Object> get props => [
//         error,
//       ];
// }
