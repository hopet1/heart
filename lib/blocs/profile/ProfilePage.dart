// import 'dart:io';
// import 'dart:math';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/blocs/profile/ProfileBloc.dart';
// import 'package:heart/blocs/profile/ProfileEvent.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/ConversationsDBService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/blocs/message/Bloc.dart' as MESSAGE_BP;
// import 'package:heart/services/UsersDBService.dart';
// import 'package:heart/widgets/ConversationListTile.dart';
// import 'package:image_picker/image_picker.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// class ProfilePage extends StatefulWidget {
//   @override
//   State createState() => ProfilePageState();
// }
//
// class ProfilePageState extends State<ProfilePage> {
//   double imageSize = 120;
//   final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
//   ProfileBloc _profileBloc;
//
//   @override
//   void initState() {
//     _profileBloc = BlocProvider.of<ProfileBloc>(context);
//
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     locator<ConversationsDBService>().cancelConversationSubscription();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     double screenWidth = MediaQuery.of(context).size.width;
//     double screenHeight = MediaQuery.of(context).size.height;
//
//     return Scaffold(
//       key: scaffoldKey,
//       backgroundColor: Colors.white,
//       body: BlocBuilder<ProfileBloc, ProfileState>(
//         builder: (BuildContext context, ProfileState state) {
//           if (state is LoadingState) {
//             return Spinner(text: 'Loading...');
//           }
//           if (state is LoadedState) {
//             List<DocumentSnapshot> docs = state.querySnapshot.docs;
//
//             //Sort the conversations by time client side, server side requires an index.
//             // docs.sort(
//             //   (a, b) {
//             //     DateTime aDate = a.data()['time'].toDate();
//             //     DateTime bDate = b.data()['time'].toDate();
//             //     return bDate.compareTo(aDate);
//             //   },
//             // );
//
//             // return buildView(
//             //   screenWidth: screenWidth,
//             //   screenHeight: screenHeight,
//             //   currentUser: state.currentUser,
//             //   mainWidget: Expanded(
//             //     flex: 6,
//             //     child: ListView.builder(
//             //       itemCount: docs.length,
//             //       itemBuilder: (BuildContext context, int index) {
//             //         DocumentSnapshot conversationDoc = docs[index];
//             //         return ConversationListTile(
//             //           key: UniqueKey(),
//             //           conversationDoc: conversationDoc,
//             //           currentUser: state.currentUser,
//             //         );
//             //       },
//             //     ),
//             //   ),
//             // );
//           }
//           if (state is NoConversationsState) {
//             return buildView(
//               screenWidth: screenWidth,
//               screenHeight: screenHeight,
//               currentUser: state.currentUser,
//               mainWidget: Center(
//                 child: Text('No messages...'),
//               ),
//             );
//           }
//           if (state is ErrorState) {
//             return Center(
//               child: Text('Error: ${state.error.toString()}'),
//             );
//           }
//
//           return Container();
//         },
//       ),
//     );
//   }
//
//   Widget buildView(
//       {@required double screenWidth,
//       @required double screenHeight,
//       @required UserModel currentUser,
//       @required Widget mainWidget}) {
//     return Stack(
//       children: <Widget>[
//         Container(
//           color: Colors.white,
//           width: screenWidth,
//           height: screenHeight,
//         ),
//         Stack(
//           children: <Widget>[
//             Container(
//               height: imageSize + 100,
//               width: screenWidth,
//               color: Colors.black,
//             ),
//             Padding(
//               padding: EdgeInsets.fromLTRB(20, 50, 20, 0),
//               child: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   IconButton(
//                     icon: Icon(
//                       Icons.chevron_left,
//                       color: Colors.white,
//                     ),
//                     onPressed: () {
//                       Navigator.of(context).pop();
//                     },
//                   ),
//                   Text(
//                     currentUser.username,
//                     style: TextStyle(
//                         fontSize: 18,
//                         color: Colors.white,
//                         fontWeight: FontWeight.bold),
//                   ),
//                   Row(
//                     children: [
//                       IconButton(
//                         icon: Icon(
//                           Icons.refresh,
//                           color: Colors.white,
//                         ),
//                         onPressed: () {
//                           _profileBloc.add(
//                             LoadPageEvent(),
//                           );
//                         },
//                       ),
//                       SizedBox(
//                         width: 10,
//                       ),
//                       InkWell(
//                         onTap: showSelectImageDialog,
//                         child: CircleAvatar(
//                           backgroundImage: NetworkImage(currentUser.imgUrl),
//                         ),
//                       )
//                     ],
//                   )
//                 ],
//               ),
//             ),
//           ],
//         ),
//         Positioned(
//           top: imageSize,
//           child: Container(
//             height: screenHeight - imageSize,
//             width: screenWidth,
//             decoration: BoxDecoration(
//               color: Colors.white,
//               borderRadius: BorderRadius.all(
//                 Radius.circular(20),
//               ),
//             ),
//             child: Container(
//               height: screenHeight - imageSize,
//               width: screenWidth,
//               child: Column(
//                 children: <Widget>[
//                   Container(
//                     width: screenWidth,
//                     padding: EdgeInsets.all(20),
//                     child: Text(
//                       'Messages',
//                       style:
//                           TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
//                       textAlign: TextAlign.center,
//                     ),
//                   ),
//                   mainWidget,
//                   Expanded(
//                     flex: 1,
//                     child: Column(
//                       children: [
//                         RaisedButton(
//                           child: Text(
//                             'Get Advice From HEART Team Member',
//                             style: TextStyle(
//                               fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                           onPressed: () async {
//                             List<UserModel> heartTeamMembers =
//                                 await locator<UsersDBService>()
//                                     .retrieveUsers(isAdmin: true);
//
//                             final int randomIndex =
//                                 Random().nextInt(heartTeamMembers.length);
//
//                             UserModel chosenHeartTeamMember =
//                                 heartTeamMembers[randomIndex];
//
//                             openMessage(adviceUser: chosenHeartTeamMember);
//                           },
//                           color: Colors.red,
//                           textColor: Colors.white,
//                         ),
//                         Text(
//                           'Free messages left - ${currentUser.freeMessageCount}\n(Refresh to show accurate message count.)',
//                           textAlign: TextAlign.center,
//                           style: TextStyle(
//                               color: Colors.grey, fontWeight: FontWeight.bold),
//                         )
//                       ],
//                     ),
//                   )
//                 ],
//               ),
//             ),
//           ),
//         )
//       ],
//     );
//   }
//
//   showSelectImageDialog() {
//     return Platform.isIOS ? iOSBottomSheet() : androidDialog();
//   }
//
//   iOSBottomSheet() {
//     showCupertinoModalPopup(
//         context: context,
//         builder: (BuildContext context) {
//           return CupertinoActionSheet(
//             title: Text('Add Photo'),
//             actions: <Widget>[
//               CupertinoActionSheetAction(
//                 child: Text('Take Photo'),
//                 onPressed: () {
//                   Navigator.of(context).pop();
//
//                   _profileBloc
//                       .add(UpdatePhotoEvent(imageSource: ImageSource.camera));
//                 },
//               ),
//               CupertinoActionSheetAction(
//                 child: Text('Choose From Gallery'),
//                 onPressed: () {
//                   Navigator.of(context).pop();
//                   _profileBloc
//                       .add(UpdatePhotoEvent(imageSource: ImageSource.gallery));
//                 },
//               )
//             ],
//             cancelButton: CupertinoActionSheetAction(
//               child: Text(
//                 'Cancel',
//                 style: TextStyle(color: Colors.redAccent),
//               ),
//               onPressed: () => Navigator.pop(context),
//             ),
//           );
//         });
//   }
//
//   androidDialog() {
//     showDialog(
//       context: context,
//       builder: (BuildContext context) {
//         return SimpleDialog(
//           title: Text('Add Photo'),
//           children: <Widget>[
//             SimpleDialogOption(
//               child: Text('Take Photo'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//
//                 _profileBloc
//                     .add(UpdatePhotoEvent(imageSource: ImageSource.camera));
//               },
//             ),
//             SimpleDialogOption(
//               child: Text('Choose From Gallery'),
//               onPressed: () {
//                 Navigator.of(context).pop();
//
//                 _profileBloc
//                     .add(UpdatePhotoEvent(imageSource: ImageSource.gallery));
//               },
//             ),
//             SimpleDialogOption(
//               child: Text(
//                 'Cancel',
//                 style: TextStyle(color: Colors.redAccent),
//               ),
//               onPressed: () => Navigator.pop(context),
//             )
//           ],
//         );
//       },
//     );
//   }
//
//   void openMessage({@required UserModel adviceUser}) async {
//     try {
//       UserModel currentUser = await locator<AuthService>().getCurrentUser();
//
//       if (adviceUser.uid == currentUser.uid) {
//         locator<ModalService>().showAlert(
//             context: context,
//             title: 'Error',
//             message: 'Cannot message yourself...');
//         return;
//       }
//
//       // final ChatUser sendee = ChatUser(
//       //     name: adviceUser.username,
//       //     uid: adviceUser.uid,
//       //     avatar: adviceUser.imgUrl,
//       //     fcmToken: adviceUser.fcmToken);
//
//       // final ChatUser sender = ChatUser(
//       //     name: currentUser.username,
//       //     uid: currentUser.uid,
//       //     avatar: currentUser.imgUrl,
//       //     fcmToken: currentUser.fcmToken);
//
//       Query query = FirebaseFirestore.instance.collection('Conversations');
//
//       //Find conversation by filtering on both user IDs being true.
//       query = query.where(currentUser.uid, isEqualTo: true);
//       query = query.where(adviceUser.uid, isEqualTo: true);
//
//       //Extract document snapshots from query object.
//       List<DocumentSnapshot> docs = (await query.get()).docs;
//
//       DocumentReference convoDocRef;
//       //Creat a new conversation document if one does not already exist.
//       // if (docs.isEmpty) {
//       //   convoDocRef =
//       //       FirebaseFirestore.instance.collection('Conversations').doc();
//       //   convoDocRef.set({
//       //     'id': convoDocRef.id,
//       //     sender.uid: true,
//       //     sendee.uid: true,
//       //     'users': [sender.uid, sendee.uid],
//       //     'time': DateTime.now(),
//       //     'lastMessage': ''
//       //   });
//       // } else {
//       //   convoDocRef = docs.first.reference;
//       // }
//
//       //Proceed to message thread with the user who you just recieved a message from.
//       // Route route = MaterialPageRoute(
//       //   builder: (BuildContext context) => BlocProvider(
//       //     create: (BuildContext context) => MESSAGE_BP.MessageBloc(
//       //       sendee: sendee,
//       //       sender: sender,
//       //       convoDocRef: convoDocRef,
//       //     )..add(
//       //         MESSAGE_BP.LoadPageEvent(),
//       //       ),
//       //     child: MESSAGE_BP.MessagePage(),
//       //   ),
//       // );
//
//       // Navigator.push(
//       //   context,
//       //   route,
//       // );
//     } catch (e) {
//       locator<ModalService>().showAlert(
//         context: context,
//         title: 'Error',
//         message: e.toString(),
//       );
//     }
//   }
// }
