// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/pages/authentication/ForgotPasswordPage.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/ValidatorService.dart';
// import '../../ServiceLocator.dart';
// import 'Bloc.dart';
//
// class LoginPage extends StatefulWidget {
//   @override
//   State createState() => LoginPageState();
// }
//
// class LoginPageState extends State<LoginPage>
//     with SingleTickerProviderStateMixin
//     implements LoginBlocDelegate {
//   final TextEditingController _emailController = TextEditingController();
//   final TextEditingController _passwordController = TextEditingController();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   LoginBloc loginBloc;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     // double screenWidth = MediaQuery.of(context).size.width;
//     // double screenHeight = MediaQuery.of(context).size.height;
//
//     //Assign LoginBloc
//     loginBloc = BlocProvider.of<LoginBloc>(context);
//     loginBloc.setDelegate(delegate: this);
//
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Login',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: _scaffoldKey,
//       body: BlocBuilder<LoginBloc, LoginState>(
//         builder: (BuildContext context, LoginState state) {
//           if (state is LoggingIn) {
//             return Spinner();
//           }
//
//           if (state is LoginNotStarted) {
//             return SafeArea(
//                 child: Padding(
//               padding: EdgeInsets.all(20),
//               child: Form(
//                 key: state.formKey,
//                 // autovalidate: autoValidate,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     TextFormField(
//                       autovalidateMode: AutovalidateMode.onUserInteraction,
//                       controller: _emailController,
//                       keyboardType: TextInputType.emailAddress,
//                       textInputAction: TextInputAction.done,
//                       onFieldSubmitted: (term) {},
//                       validator: locator<ValidatorService>().email,
//                       onSaved: (value) {},
//                       decoration: InputDecoration(
//                         focusedBorder: UnderlineInputBorder(
//                           borderSide: BorderSide(color: Colors.red),
//                         ),
//                         hintText: 'Email',
//                         // icon: Icon(Icons.email),
//                         fillColor: Colors.white,
//                       ),
//                     ),
//                     SizedBox(
//                       height: 20,
//                     ),
//                     TextFormField(
//                       autovalidateMode: AutovalidateMode.onUserInteraction,
//                       controller: _passwordController,
//                       keyboardType: TextInputType.emailAddress,
//                       textInputAction: TextInputAction.done,
//                       onFieldSubmitted: (term) {},
//                       obscureText: true,
//                       validator: locator<ValidatorService>().password,
//                       onSaved: (value) {},
//                       decoration: InputDecoration(
//                         focusedBorder: UnderlineInputBorder(
//                           borderSide: BorderSide(color: Colors.red),
//                         ),
//                         hintText: 'Password',
//                         // icon: Icon(Icons.email),
//                         fillColor: Colors.white,
//                       ),
//                     ),
//                     Spacer(),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         RaisedButton(
//                           color: Colors.red,
//                           textColor: Colors.white,
//                           child: Text('Continue'),
//                           onPressed: () {
//                             loginBloc.add(
//                               Login(
//                                   formKey: state.formKey,
//                                   email: _emailController.text,
//                                   password: _passwordController.text),
//                             );
//                           },
//                         )
//
//                         // SilentButton(
//                         //   title: 'CONTINUE',
//                         //   onTap: () {
//                         //     loginBloc.add(
//                         //       Login(
//                         //           formKey: state.formKey,
//                         //           email: _emailController.text,
//                         //           password: _passwordController.text),
//                         //     );
//                         //   },
//                         // )
//                       ],
//                     ),
//                     Align(
//                       alignment: Alignment.topCenter,
//                       child: Container(
//                         margin: EdgeInsets.only(top: 30),
//                         child: InkWell(
//                           child: Text(
//                             "Forgot Password?",
//                             textAlign: TextAlign.right,
//                             style: TextStyle(
//                               color: Color.fromARGB(255, 232, 63, 63),
//                               fontFamily: "Avenir",
//                               fontWeight: FontWeight.w800,
//                               fontSize: 16,
//                             ),
//                           ),
//                           onTap: () => {
//                             Navigator.push(
//                               context,
//                               MaterialPageRoute(
//                                 builder: (context) => ForgotPasswordPage(),
//                               ),
//                             )
//                           },
//                         ),
//                       ),
//                     ),
//                   ],
//                 ),
//               ),
//             ));
//           }
//
//           return Center(
//             child: Text('You Should NEVER see this.'),
//           );
//         },
//       ),
//     );
//   }
//
//   @override
//   void navigateHome() {
//     Navigator.of(context).pop();
//   }
//
//   @override
//   void showMessage({String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
