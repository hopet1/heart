// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// // import 'package:heart/services/AnalyticsService.dart';
// import 'package:heart/services/AuthService.dart';
// import 'Bloc.dart';
//
// abstract class LoginBlocDelegate {
//   void navigateHome();
//   void showMessage({@required String message});
// }
//
// class LoginBloc extends Bloc<LoginEvent, LoginState> {
//   LoginBloc() : super(null);
//   LoginBlocDelegate _loginBlocDelegate;
//
//   void setDelegate({
//     @required LoginBlocDelegate delegate,
//   }) {
//     this._loginBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<LoginState> mapEventToState(LoginEvent event) async* {
//     if (event is LoadPageEvent) {
//       yield LoginNotStarted(
//         autoValidate: false,
//         formKey: GlobalKey<FormState>(),
//       );
//     }
//
//     if (event is Login) {
//       yield LoggingIn();
//       try {
//         final UserCredential userCredential = await locator<AuthService>()
//             .signInWithEmailAndPassword(
//                 email: event.email, password: event.password);
//
//         print('User UID : ${userCredential.user.uid}');
//
//         // locator<AnalyticsService>()
//         //     .logEvent(eventType: AnalyticsEventType.LOGIN);
//
//         _loginBlocDelegate.navigateHome();
//       } catch (error) {
//         _loginBlocDelegate.showMessage(message: 'Error: ${error.toString()}');
//
//         yield LoginNotStarted(
//           autoValidate: true,
//           formKey: event.formKey,
//         );
//       }
//     }
//   }
// }
