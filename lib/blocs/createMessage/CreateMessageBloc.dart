// import 'package:flutter/widgets.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/blocs/createMessage/CreateMessageState.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/TemplateModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/TemplatesService.dart';
// import 'Bloc.dart';
//
// abstract class CreateMessageBlocDelegate {
//   void showMessage({@required String message});
// }
//
// class CreateMessageBloc extends Bloc<CreateMessageEvent, CreateMessageState> {
//   CreateMessageBloc({@required this.category}) : super(null);
//
//   CategoryModel category;
//
//   CreateMessageBlocDelegate _createMessageBlocDelegate;
//   UserModel _currentUser;
//
//   void setDelegate({@required CreateMessageBlocDelegate delegate}) {
//     this._createMessageBlocDelegate = delegate;
//   }
//
//   @override
//   Stream<CreateMessageState> mapEventToState(CreateMessageEvent event) async* {
//     if (event is LoadPageEvent) {
//       try {
//         _currentUser = await locator<AuthService>().getCurrentUser();
//         yield LoadedState(
//             autoValidate: false,
//             formKey: GlobalKey<FormState>(),
//             currentUser: _currentUser,
//             category: category);
//       } catch (error) {
//         //todo...
//       }
//     }
//
//     if (event is SubmitEvent) {
//       final GlobalKey<FormState> formKey = event.formKey;
//       final String message = event.message;
//
//       if (formKey.currentState.validate()) {
//         formKey.currentState.save();
//
//         try {
//           yield LoadingState();
//
//           if (formKey.currentState.validate()) {
//             formKey.currentState.save();
//
//             TemplateModel template = TemplateModel(
//               id: '',
//               category: category.title,
//               copies: 0,
//               count: 0,
//               message: message,
//               time: DateTime.now(),
//               userID: _currentUser.uid,
//               safe: false,
//             );
//
//             await locator<TemplatesService>()
//                 .createTemplate(template: template);
//           }
//           _createMessageBlocDelegate.showMessage(
//             message: 'Message submitted..',
//           );
//           yield LoadedState(
//               autoValidate: true,
//               formKey: event.formKey,
//               currentUser: _currentUser,
//               category: category);
//         } catch (error) {
//           _createMessageBlocDelegate.showMessage(
//             message: error.toString(),
//           );
//           yield LoadedState(
//               autoValidate: true,
//               formKey: event.formKey,
//               currentUser: _currentUser,
//               category: category);
//         }
//       }
//     }
//   }
// }
