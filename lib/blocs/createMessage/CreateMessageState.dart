// import 'package:equatable/equatable.dart';
// import 'package:flutter/material.dart';
// import 'package:heart/models/basic/CategoryModel.dart';
// import 'package:heart/models/firebase/UserModel.dart';
//
// class CreateMessageState extends Equatable {
//   const CreateMessageState();
//   @override
//   List<Object> get props => [];
// }
//
// class LoadingState extends CreateMessageState {}
//
// class LoadedState extends CreateMessageState {
//   final bool autoValidate;
//   final GlobalKey<FormState> formKey;
//   final UserModel currentUser;
//   final CategoryModel category;
//
//   LoadedState({
//     @required this.autoValidate,
//     @required this.formKey,
//     @required this.currentUser,
//     @required this.category,
//   });
//
//   @override
//   List<Object> get props => [
//         autoValidate,
//         formKey,
//         currentUser,
//         category,
//       ];
// }
