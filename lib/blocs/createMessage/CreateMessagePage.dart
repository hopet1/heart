// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/blocs/createMessage/Bloc.dart';
// import 'package:heart/services/InAppReviewService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/ValidatorService.dart';
// import '../../ServiceLocator.dart';
//
// class CreateMessagePage extends StatefulWidget {
//   @override
//   State createState() => CreateMessagePageState();
// }
//
// class CreateMessagePageState extends State<CreateMessagePage>
//     implements CreateMessageBlocDelegate {
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//   final TextEditingController _messageController = TextEditingController();
//   CreateMessageBloc _createMessageBloc;
//
//   @override
//   void initState() {
//     _createMessageBloc = BlocProvider.of<CreateMessageBloc>(context);
//     _createMessageBloc.setDelegate(delegate: this);
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.black,
//         title: Text(
//           'Create Message',
//           style: TextStyle(
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ),
//       key: _scaffoldKey,
//       backgroundColor: Colors.white,
//       body: SafeArea(
//         child: BlocBuilder<CreateMessageBloc, CreateMessageState>(
//           builder: (BuildContext context, CreateMessageState state) {
//             if (state is LoadingState) {
//               return Spinner();
//             }
//
//             if (state is LoadedState) {
//               return Form(
//                 autovalidateMode: AutovalidateMode.onUserInteraction,
//                 key: state.formKey,
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: [
//                     Padding(
//                       padding: EdgeInsets.all(30),
//                       child: TextFormField(
//                         keyboardType: TextInputType.multiline,
//                         maxLines: null,
//                         textInputAction: TextInputAction.done,
//                         textCapitalization: TextCapitalization.sentences,
//                         controller: _messageController,
//                         onFieldSubmitted: (term) {},
//                         validator: locator<ValidatorService>().isEmpty,
//                         onSaved: (value) {},
//                         decoration: InputDecoration(
//                           hintText:
//                               'New message for \"${state.category.title}\"',
//                         ),
//                       ),
//                     ),
//                     Spacer(),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                       children: <Widget>[
//                         RaisedButton(
//                           color: Colors.red,
//                           textColor: Colors.white,
//                           child: Text('Save'),
//                           onPressed: () {
//                             _createMessageBloc.add(
//                               SubmitEvent(
//                                 formKey: state.formKey,
//                                 message: _messageController.text,
//                               ),
//                             );
//
//                             locator<InAppReviewService>().requestReview();
//                           },
//                         )
//                       ],
//                     ),
//                   ],
//                 ),
//               );
//             }
//
//             return Container();
//           },
//         ),
//       ),
//     );
//   }
//
//   @override
//   void showMessage({@required String message}) {
//     locator<ModalService>().showInSnackBar(context: context, message: message);
//   }
// }
