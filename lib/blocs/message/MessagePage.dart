// import 'dart:async';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter/widgets.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/blocs/subscription/SubscriptionPage.dart';
// import 'package:heart/widgets/Spinner.dart';
// import 'package:heart/services/FormatterService.dart';
// import 'package:heart/services/ModalService.dart';
// import '../../ServiceLocator.dart';
// import '../../data.dart';
// import 'package:heart/blocs/message/Bloc.dart' as MESSAGE_Bloc;
// import 'package:heart/blocs/subscription/Bloc.dart' as SUBSCRIPTION_BP;
//
// class MessagePage extends StatefulWidget {
//   @override
//   MessagePageState createState() => MessagePageState();
// }
//
// class MessagePageState extends State<MessagePage>
//     implements MESSAGE_Bloc.MessageBlocDelegate {
//   final GlobalKey<DashChatState> _chatViewKey = GlobalKey<DashChatState>();
//   final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
//
//   List<ChatMessage> _messages = [];
//   List<ChatMessage> _m = [];
//
//   int _i = 0;
//   MESSAGE_Bloc.MessageBloc _messageBloc;
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   void dispose() {
//     _messageBloc.close();
//     super.dispose();
//   }
//
//   @override
//   void showAlert({@required String message}) async {
//     locator<ModalService>()
//         .showAlert(context: context, title: '', message: message);
//   }
//
//   @override
//   void showOptions(
//       {@required String message, @required ChatMessage chatMessage}) async {
//     int choice = await locator<ModalService>()
//         .showOptions(context: context, title: 'No More Messages', options: [
//       'Update Subscription',
//       'Send message for ${locator<FormatterService>().money(amount: 50)}.'
//     ]);
//
//     switch (choice) {
//       case 0:
//         // Route route = MaterialPageRoute(
//         //   builder: (BuildContext context) => BlocProvider(
//         //     create: (BuildContext context) => SUBSCRIPTION_BP.SubscriptionBloc()
//         //       ..add(SUBSCRIPTION_BP.LoadPageEvent()),
//         //     child: SubscriptionPage(),
//         //   ),
//         // );
//         // Navigator.push(context, route);
//         break;
//       case 1:
//         bool confirm = await locator<ModalService>().showConfirmation(
//             context: context,
//             title:
//                 'Send message for ${locator<FormatterService>().money(amount: 50)}.',
//             message: 'Are you sure?');
//
//         if (confirm) {
//           _messageBloc.add(
//             MESSAGE_Bloc.ChargeSendMessageEvent(message: chatMessage),
//           );
//         }
//         break;
//       default:
//         break;
//     }
//   }
//
//   void systemMessage() {
//     Timer(Duration(milliseconds: 300), () {
//       if (_i < 6) {
//         setState(() {
//           _messages = [..._messages, _m[_i]];
//         });
//         _i++;
//       }
//       Timer(Duration(milliseconds: 300), () {
//         _chatViewKey.currentState.scrollController
//           ..animateTo(
//             _chatViewKey.currentState.scrollController.position.maxScrollExtent,
//             curve: Curves.easeOut,
//             duration: const Duration(milliseconds: 300),
//           );
//       });
//     });
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     _messageBloc = BlocProvider.of<MESSAGE_Bloc.MessageBloc>(context);
//     _messageBloc.setDelegate(delegate: this);
//
//     return BlocBuilder<MESSAGE_Bloc.MessageBloc, MESSAGE_Bloc.MessageState>(
//         builder: (BuildContext context, MESSAGE_Bloc.MessageState state) {
//       if (state is MESSAGE_Bloc.LoadingState) {
//         return Scaffold(
//           key: _scaffoldKey,
//           appBar: AppBar(
//             backgroundColor: Colors.black,
//             title: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//               children: <Widget>[
//                 Text(''),
//                 CircleAvatar(
//                   backgroundImage: NetworkImage(DUMMY_PROFILE_PHOTO_URL),
//                 )
//               ],
//             ),
//           ),
//           body: Center(
//             child: Spinner(text: 'Loading...'),
//           ),
//         );
//       }
//
//       if (state is MESSAGE_Bloc.MessagesState) {
//         final String sendeeName = state.sendee.name.length > 12
//             ? '${state.sendee.name.substring(0, 13)}...'
//             : state.sendee.name;
//
//         List<ChatMessage> messages;
//         if (state.messagesQuerySnapshot == null) {
//           messages = [];
//         } else {
//           List<DocumentSnapshot> items = state.messagesQuerySnapshot.docs;
//           messages = items.map((i) => ChatMessage.fromJson(i.data())).toList();
//         }
//
//         return WillPopScope(
//           onWillPop: () async {
//             _messageBloc.add(MESSAGE_Bloc.GoBackToPreviousScreenEvent());
//             return true;
//           },
//           child: Scaffold(
//             key: _scaffoldKey,
//             appBar: AppBar(
//               backgroundColor: Colors.black,
//               title: Row(
//                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                 children: <Widget>[
//                   Text(sendeeName),
//                   CircleAvatar(
//                     backgroundImage: NetworkImage(state.sendee.avatar),
//                   )
//                 ],
//               ),
//             ),
//             body: SafeArea(
//               child: DashChat(
//                 textCapitalization: TextCapitalization.sentences,
//                 key: _chatViewKey,
//                 inverted: false,
//                 onSend: (ChatMessage message) {
//                   _messageBloc.add(
//                       MESSAGE_Bloc.ValidateSendMessageEvent(message: message));
//                 },
//                 user: state.sender,
//                 inputDecoration:
//                     InputDecoration.collapsed(hintText: "Add message here..."),
//                 dateFormat: DateFormat('yyyy-MMM-dd'),
//                 timeFormat: DateFormat('h:mm a'),
//                 messages: messages,
//                 showUserAvatar: true,
//                 showAvatarForEveryMessage: true,
//                 scrollToBottom: false,
//                 onLongPressMessage: (ChatMessage chatMessage) {
//                   //Open/download image.
//                   if (chatMessage.image != null) {
//                     showModalBottomSheet(
//                       context: context,
//                       builder: (context) => Container(
//                         child: Column(
//                           mainAxisSize: MainAxisSize.min,
//                           children: <Widget>[
//                             ListTile(
//                               leading: Icon(Icons.open_in_browser),
//                               title: Text("View image"),
//                               onTap: () {
//                                 Navigator.of(context).push(
//                                   MaterialPageRoute(
//                                     builder: (ctx) => Scaffold(
//                                       body: Container(
//                                         color: Colors.black,
//                                         child: GestureDetector(
//                                           onTap: () {
//                                             Navigator.pop(context);
//                                           },
//                                           child: Center(
//                                             child: Hero(
//                                               tag: chatMessage.image,
//                                               child: Image.network(
//                                                   chatMessage.image),
//                                             ),
//                                           ),
//                                         ),
//                                       ),
//                                     ),
//                                   ),
//                                 );
//                               },
//                             ),
//                             ListTile(
//                                 leading: Icon(Icons.file_download),
//                                 title: Text("Download image"),
//                                 onTap: () => _messageBloc.add(
//                                       MESSAGE_Bloc.DownloadImageEvent(
//                                           imgUrl: chatMessage.image),
//                                     )
//                                 //downloadImage(imgUrl: chatMessage.image),
//                                 ),
//                             ListTile(
//                               leading: Icon(Icons.cancel),
//                               title: Text("Cancel"),
//                               onTap: () {
//                                 Navigator.pop(context);
//                               },
//                             )
//                           ],
//                         ),
//                       ),
//                     );
//                   } //Copy message.
//                   else {
//                     showModalBottomSheet(
//                       context: context,
//                       builder: (context) => Container(
//                         child: Column(
//                           mainAxisSize: MainAxisSize.min,
//                           children: <Widget>[
//                             ListTile(
//                               leading: Icon(Icons.content_copy),
//                               title: Text("Copy to clipboard"),
//                               onTap: () {
//                                 Clipboard.setData(
//                                     ClipboardData(text: chatMessage.text));
//                                 Navigator.pop(context);
//                                 locator<ModalService>().showInSnackBar(
//                                     context: context,
//                                     message: 'Message copied.');
//                               },
//                             ),
//                             ListTile(
//                               leading: Icon(Icons.cancel),
//                               title: Text("Cancel"),
//                               onTap: () {
//                                 Navigator.pop(context);
//                               },
//                             )
//                           ],
//                         ),
//                       ),
//                     );
//                   }
//                 },
//                 onPressAvatar: (ChatUser user) {
//                   print("OnPressAvatar: ${user.name}");
//                 },
//                 onLongPressAvatar: (ChatUser user) {
//                   print("OnLongPressAvatar: ${user.name}");
//                 },
//                 inputMaxLines: 5,
//                 messageContainerPadding: EdgeInsets.only(left: 5.0, right: 5.0),
//                 alwaysShowSend: true,
//                 inputTextStyle: TextStyle(fontSize: 16.0),
//                 inputContainerStyle: BoxDecoration(
//                   border: Border.all(width: 0.0),
//                   color: Colors.white,
//                 ),
//                 onQuickReply: (Reply reply) {
//                   setState(
//                     () {
//                       messages.add(
//                         ChatMessage(
//                             text: reply.value,
//                             createdAt: DateTime.now(),
//                             user: state.sendee),
//                       );
//
//                       messages = [...messages];
//                     },
//                   );
//
//                   Timer(Duration(milliseconds: 300), () {
//                     _chatViewKey.currentState.scrollController
//                       ..animateTo(
//                         _chatViewKey.currentState.scrollController.position
//                             .maxScrollExtent,
//                         curve: Curves.easeOut,
//                         duration: const Duration(milliseconds: 300),
//                       );
//
//                     if (_i == 0) {
//                       systemMessage();
//                       Timer(Duration(milliseconds: 600), () {
//                         systemMessage();
//                       });
//                     } else {
//                       systemMessage();
//                     }
//                   });
//                 },
//                 onLoadEarlier: () {
//                   print("loading...");
//                 },
//                 shouldShowLoadEarlier: false,
//                 showTraillingBeforeSend: true,
//                 trailing: <Widget>[
//                   IconButton(
//                     icon: Icon(Icons.photo),
//                     onPressed: () {
//                       _messageBloc
//                           .add(MESSAGE_Bloc.PickImageEvent(context: context));
//                       //pickImage()
//                     },
//                   )
//                 ],
//               ),
//             ),
//           ),
//         );
//       }
//
//       if (state is MESSAGE_Bloc.ErrorState) {
//         return Center(
//           child: Text('Error: ${state.error.toString()}'),
//         );
//       }
//
//       return Container();
//     });
//   }
// }
