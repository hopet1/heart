// import 'dart:async';
// import 'dart:io';
// import 'dart:math';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:dash_chat/dash_chat.dart';
// import 'package:firebase_storage/firebase_storage.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:heart/ServiceLocator.dart';
// import 'package:heart/blocs/message/MessageEvent.dart';
// import 'package:heart/blocs/message/MessageState.dart';
// import 'package:heart/models/firebase/UserModel.dart';
// import 'package:heart/services/AuthService.dart';
// import 'package:heart/services/FCMNotificationService.dart';
// import 'package:heart/services/ModalService.dart';
// import 'package:heart/services/RevenueCatService.dart';
// import 'package:image_downloader/image_downloader.dart';
// import 'package:image_picker/image_picker.dart';
// import '../../data.dart';
// import 'Bloc.dart';
// import 'package:uuid/uuid.dart';
//
// abstract class MessageBlocDelegate {
//   void showOptions(
//       {@required String message, @required ChatMessage chatMessage});
//   void showAlert({@required String message});
// }
//
// class MessageBloc extends Bloc<MessageEvent, MessageState> {
//   MessageBloc(
//       {@required this.sendee,
//       @required this.sender,
//       @required this.convoDocRef})
//       : super(null);
//
//   final ChatUser sendee;
//   final ChatUser sender;
//   final DocumentReference convoDocRef;
//
//   UserModel currentUser;
//   StreamSubscription subscription;
//   MessageBlocDelegate delegate;
//
//   void setDelegate({@required MessageBlocDelegate delegate}) {
//     this.delegate = delegate;
//   }
//
//   @override
//   Stream<MessageState> mapEventToState(MessageEvent event) async* {
//     if (event is LoadPageEvent) {
//       yield LoadingState(text: 'Loading...');
//
//       currentUser = await locator<AuthService>().getCurrentUser();
//
//       convoDocRef
//           .update({'${sender.uid}_read': true, '${sendee.uid}_read': false});
//
//       Query messagesQuery = convoDocRef.collection('Messages');
//
//       //nnabaa@gmail.com && benton.ej.08@gmail.com
//       final bool sendeeIsAdmin =
//           sendee.uid.contains('CLzXnuhV2fbALUJUd5ul4PGv5t82') ||
//               sendee.uid.contains('qGYqKO4PsYUXtSSwdWZFJv0tnNS2');
//
//       //If talking to a HEART Team member and thread is empty, add template message.
//       if (sendeeIsAdmin && (await messagesQuery.get()).docs.isEmpty) {
//         DocumentReference newMessageDocRef =
//             convoDocRef.collection('Messages').doc(
//                   DateTime.now().millisecondsSinceEpoch.toString(),
//                 );
//
//         final int randomIndex =
//             Random().nextInt(HEART_TEMPLATE_MESSAGES.length);
//
//         final String templateMessage = HEART_TEMPLATE_MESSAGES[randomIndex];
//
//         convoDocRef.update({
//           '${sender.uid}_read': true,
//           '${sendee.uid}_read': false,
//           'lastMessage': templateMessage,
//           'time': DateTime.now()
//         });
//
//         newMessageDocRef.set({
//           'createdAt': DateTime.now().millisecondsSinceEpoch,
//           'id': Uuid().v4(),
//           'image': null,
//           'quickReplies': null,
//           'text': templateMessage,
//           'user': {
//             'avatar': sendee.avatar,
//             'color': null,
//             'containerColor': null,
//             // 'fcmToken': sendee.fcmToken,
//             'name': sendee.name,
//             'uid': sendee.uid
//           },
//           'video': null,
//         });
//       }
//
//       if ((await messagesQuery.get()).docs.isEmpty) {
//         yield MessagesState(
//           sendee: sendee,
//           sender: sender,
//           convoDocRef: convoDocRef,
//           messagesQuerySnapshot: null,
//         );
//       } else {
//         Stream<QuerySnapshot> snapshots = messagesQuery.snapshots();
//         subscription = snapshots.listen(
//           (querySnapshot) {
//             add(
//               MessageAddedEvent(querySnapshot: querySnapshot),
//             );
//           },
//         );
//       }
//     }
//
//     if (event is MessageAddedEvent) {
//       yield MessagesState(
//         sendee: sendee,
//         sender: sender,
//         convoDocRef: convoDocRef,
//         messagesQuerySnapshot: event.querySnapshot,
//       );
//     }
//
//     if (event is DownloadImageEvent) {
//       // void downloadImage({@required String imgUrl}) async {
//       try {
//         // Saved with this method.
//         var imageId = await ImageDownloader.downloadImage(event.imgUrl);
//         if (imageId == null) {
//           delegate.showAlert(
//               message: 'Could not download message at this time.');
//         } else {
//           // Below is a method of obtaining saved image information.
//           // var fileName = await ImageDownloader.findName(imageId);
//           // var path = await ImageDownloader.findPath(imageId);
//           // var size = await ImageDownloader.findByteSize(imageId);
//           // var mimeType = await ImageDownloader.findMimeType(imageId);
//
//           delegate.showAlert(message: 'Image downloaded.');
//         }
//       } on PlatformException catch (error) {
//         print(error);
//       }
//       // }
//     }
//
//     //Validate user has messages left.
//     if (event is ValidateSendMessageEvent) {
//       if (event.message.text == '') {
//         return;
//       }
//
//       if (!currentUser.isAdmin) {
//         if (currentUser.freeMessageCount == 3) {
//           delegate.showAlert(message: 'You have 2 more free messages.');
//         }
//
//         if (currentUser.freeMessageCount == 2) {
//           delegate.showAlert(message: 'You have 1 more free message.');
//         }
//
//         if (currentUser.freeMessageCount == 1) {
//           delegate.showAlert(message: 'You have 0 more free message.');
//         }
//
//         if (currentUser.freeMessageCount == 0) {
//           delegate.showOptions(
//               message:
//                   'You have no more free messages. Upgrade plan now to get more messages.',
//               chatMessage: event.message);
//           return;
//         }
//       }
//       add(
//         SendMessageEvent(message: event.message),
//       );
//     }
//
//     if (event is ChargeSendMessageEvent) {
//       try {
//         // await locator<RevenueCatService>().purchase(sku: singleMessageProduct);
//
//         add(
//           SendMessageEvent(message: event.message),
//         );
//       } catch (error) {
//         yield ErrorState(error: error);
//       }
//     }
//
//     if (event is SendMessageEvent) {
//       try {
//         ChatMessage message = event.message;
//         FirebaseFirestore db = FirebaseFirestore.instance;
//         WriteBatch batch = db.batch();
//
//         if (!currentUser.isAdmin) {
//           DocumentReference userDocRef = (await db
//                   .collection('Users')
//                   .where('uid', isEqualTo: currentUser.uid)
//                   .get())
//               .docs
//               .first
//               .reference;
//
//           batch.update(
//             userDocRef,
//             {
//               'freeMessageCount': currentUser.freeMessageCount == 0
//                   ? 0
//                   : FieldValue.increment(-1),
//             },
//           );
//         }
//
//         batch.update(convoDocRef, {
//           '${sender.uid}_read': true,
//           '${sendee.uid}_read': false,
//           'lastMessage': message.image == null ? message.text : 'Image',
//           'time': DateTime.now()
//         });
//
//         DocumentReference newMessageDocRef =
//             convoDocRef.collection('Messages').doc(
//                   DateTime.now().millisecondsSinceEpoch.toString(),
//                 );
//
//         batch.set(
//           newMessageDocRef,
//           message.toJson(),
//         );
//
//         await batch.commit();
//
//         await locator<FCMNotificationService>().sendNotificationToUser(
//           // fcmToken: sendee.fcmToken,
//           title: 'New Message From ${sender.name}',
//           body: message.text,
//           notificationData: NotificationData(
//             userID: sender.uid,
//             message: 'New Message From ${sender.name}',
//             type: FCM_NEW_MESSAGE,
//           ),
//         );
//
//         currentUser = await locator<AuthService>().getCurrentUser();
//       } catch (error) {
//         yield ErrorState(error: error);
//       }
//     }
//
//     if (event is PickImageEvent) {
//       PickedFile result = await ImagePicker().getImage(
//         source: ImageSource.gallery,
//         // imageQuality: 80,
//         // maxHeight: 400,
//         // maxWidth: 400,
//       );
//
//       if (result != null) {
//         try {
//           bool confirm = await locator<ModalService>()
//               .showConfirmationWithImage(
//                   context: event.context,
//                   title: 'Send This Image?',
//                   message: 'Are you sure?',
//                   file: result);
//           if (confirm) {
//             final Reference storageRef =
//                 FirebaseStorage.instance.ref().child(convoDocRef.path).child(
//                       DateTime.now().toString(),
//                     );
//
//             UploadTask uploadTask = storageRef.putFile(
//               File(result.path),
//               SettableMetadata(
//                 contentType: 'image/jpg',
//               ),
//             );
//             Reference download = (await uploadTask).ref;
//
//             String url = await download.getDownloadURL();
//
//             ChatMessage message =
//                 ChatMessage(text: '', user: sender, image: url);
//
//             var documentReference = convoDocRef
//                 .collection('Messages')
//                 .doc(DateTime.now().millisecondsSinceEpoch.toString());
//
//             FirebaseFirestore.instance.runTransaction((transaction) async {
//               transaction.set(
//                 documentReference,
//                 message.toJson(),
//               );
//             });
//           }
//         } catch (e) {
//           locator<ModalService>().showAlert(
//             context: event.context,
//             title: 'Error',
//             message: e.toString(),
//           );
//         }
//       }
//     }
//
//     if (event is GoBackToPreviousScreenEvent) {
//       final DocumentSnapshot convoDoc = await convoDocRef.get();
//       // final String lastMessage = convoDoc.data()['lastMessage'];
//       // if (lastMessage == '') {
//       //   convoDocRef.delete();
//       // }
//     }
//   }
//
//   @override
//   Future<void> close() {
//     subscription?.cancel();
//     return super.close();
//   }
// }
