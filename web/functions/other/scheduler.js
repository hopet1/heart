const functions = require('firebase-functions');
const admin = require('firebase-admin');

//“At 00:00 on day-of-month 1.”
exports.resetFreeMessagesCount = functions.pubsub.schedule('0 0 1 * *').timeZone('America/New_York').onRun(async (context) => {
    //https://crontab.guru/

    const querySnapshot = await admin.firestore().collection('Users').get();

    await Promise.all(querySnapshot.docs.map(
        (userDoc) => {
            //12 free advice messages monthly.
            if (userDoc.data['activeSubscription'] === 'PREMIUM_PLAN') {
                userDoc.ref.update({ 'freeMessageCount': 12 });

            }
            //3 free advice messages monthly.
            else if (userDoc.data['activeSubscription'] === 'PLUS_PLAN') {
                userDoc.ref.update({ 'freeMessageCount': 3 });

            }
            //1 free advice message monthly.
            else {
                userDoc.ref.update({ 'freeMessageCount': 1 });
            }
        }
    ))
});